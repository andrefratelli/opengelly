#include <evts/KeyboardAnimationListener.h>

using namespace eng;

KeyboardAnimationEvent::KeyboardAnimationEvent(Transformable& target, KeyboardEvent trigger) :
    AnimationEvent<KeyboardEvent>(target, trigger)
{}

KeyboardAnimationListener::KeyboardAnimationListener(Transformable& target) :
    AnimationListener<KeyboardAnimationEvent, KeyboardListener, KeyboardEvent>(target)
{}
