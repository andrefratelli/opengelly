#ifndef KEYBOARDLISTENER_H
#define	KEYBOARDLISTENER_H

#include <evts/Listener.h>
#include <evts/Event.h>

#define DEFAULT_KEYBOARD_IDLE_INTERVAL

/*
 * == KeyboardEvent == KeyboardListener ==
 * 
 * These are the listener and event for keyboard events. The arguments and methods
 * might seem strange. See the implementation of GameEngine::notify(IdleEvent)
 * for details.
 */
namespace eng
{
    typedef enum
    {
        KEY_PRESS,
        KEY_RELEASE
    } KeyEvent;
    
    class KeyboardEvent : public Event
    {
    public:
        typedef unsigned char key_type;
        
        KeyboardEvent(bool normalMap[256], bool specialMap[256]);
        KeyboardEvent(const KeyboardEvent& copy);
        
        bool down(key_type key) const;  // Check if a standard key is down
        bool sdown(key_type key) const; // Check if a special key is down
        
    private:
        // The buffers for these come from the window manager, which is static,
        // so there's no danger of becoming invalid
        bool* _map;
        bool* _special;
    };
    
    class KeyboardListener : public Listener<KeyboardEvent>
    {};
}

#endif	/* KEYBOARDLISTENER_H */

