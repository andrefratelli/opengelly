#include <evts/IdleAnimationListener.h>

using namespace eng;

IdleAnimationEvent::IdleAnimationEvent(Transformable& target, IdleEvent trigger) :
    AnimationEvent<IdleEvent>(target, trigger)
{}

IdleAnimationListener::IdleAnimationListener(Transformable& target) :
    AnimationListener<IdleAnimationEvent, IdleListener, IdleEvent>(target)
{}

