#ifndef KEYBOARDANIMATIONLISTENER_H
#define	KEYBOARDANIMATIONLISTENER_H

#include <evts/AnimationListener.h>
#include <evts/KeyboardListener.h>

/*
 * == KeyboardAnimationEvent == KeyboardAnimationListener ==
 * 
 * Listener and event for keyboard animation
 */
namespace eng
{
    class KeyboardAnimationEvent :
        public AnimationEvent<KeyboardEvent>
    {
    public:
        KeyboardAnimationEvent(Transformable& target, KeyboardEvent trigger);
    };
    
    class KeyboardAnimationListener :
        public AnimationListener<KeyboardAnimationEvent, KeyboardListener, KeyboardEvent>
    {
    public:
        KeyboardAnimationListener(Transformable& target);
    };
}

#endif	/* KEYBOARDANIMATIONLISTENER_H */
