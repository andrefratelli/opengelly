#ifndef EVENT_H
#define	EVENT_H

/*
 * == Event ==
 * 
 * Base class for all events in the engine
 */
namespace eng
{
    class Event
    {
    protected:
        
        // Prevent direct instantiation
        Event();
    };
}

#endif	/* EVENT_H */

