#ifndef LISTENER_H
#define	LISTENER_H

/*
 * == Listener ==
 * 
 * This is the base class for all listeners in the engine. It defines a virtual
 * method which is called when an event occurs.
 */
namespace eng
{
    template <class E> class Listener
    {
    public:
        typedef E event_type;
        
        virtual void notify(event_type evt) = 0;
        
    protected:
        
        // Prevents direct instantiation
        Listener();
    };
    
    template <class E> Listener<E>::Listener() {}
}

#endif	/* LISTENER_H */
