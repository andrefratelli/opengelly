#ifndef RESHAPEANIMATIONLISTENER_H
#define	RESHAPEANIMATIONLISTENER_H

#include <evts/AnimationListener.h>
#include <evts/ReshapeListener.h>

/*
 * == ReshapeAnimationEvent == ReshapeAnimationListener ==
 * 
 * For animating transformables when the window is resized.
 */
namespace eng
{
    class ReshapeAnimationEvent :
        public AnimationEvent<ReshapeEvent>
    {
    public:
        ReshapeAnimationEvent(Transformable& target, ReshapeEvent trigger);
    };
    
    class ReshapeAnimationListener :
        public AnimationListener<ReshapeAnimationEvent, ReshapeListener, ReshapeEvent>
    {
    public:
        ReshapeAnimationListener(Transformable& target);
    };
}

#endif	/* RESHAPEANIMATIONLISTENER_H */

