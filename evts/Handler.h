#ifndef HANDLER_H
#define	HANDLER_H

#include <list>
#include <iterator>
#include <vector>
#include <utils/SafePointer.h>

#include <iostream>
using namespace std;

/*
 * == Handler ==
 * 
 * This is the base class for all handlers of events in the engine. It receives
 * listeners of specific events, registers them in a list and returns a
 * registration container (which can be used to remove the event).
 * 
 * As soon as an event is fired, every listener in the list is notified.
 */
namespace eng
{
    template <class L, class E> class HandlerRegistration;
    
    template <class L, class E> class Handler
    {
        friend class HandlerRegistration<L,E>;
        
    public:
        typedef L listener_type;
        typedef E event_type;
        typedef HandlerRegistration<listener_type, event_type> registration_type;
        
        Handler();
        ~Handler();
        
        registration_type addEventListener(listener_type* listener);
        void fire(event_type evt);

    private:
        typedef std::list<listener_type*> bundle_type;  // Lists don't erase pointers
        typedef std::list<SafePointer<bool> > registration_bundle_type;
        
        bundle_type _bundle;
        registration_bundle_type _registrationBundle;
    };
    
    template <class L, class E> Handler<L,E>::Handler() :
        _bundle(Handler<L,E>::bundle_type()),
        _registrationBundle(Handler<L,E>::registration_bundle_type())
    {}
    
    template <class L, class E> Handler<L,E>::~Handler()
    {
        /*
         * Uppon destruction, remove notify all listeners that they have been
         * removed.
         */
        registration_bundle_type::iterator it = _registrationBundle.begin();
        
        while (it != _registrationBundle.end())
        {
            **it = true;
        }
    }
    
    template <class L, class E> typename Handler<L,E>::registration_type Handler<L,E>::addEventListener(Handler<L,E>::listener_type* listener) 
    {
        _bundle.push_back(listener);
        
        // If the handler dies all registrations need to be notified so that
        // they do not attempt to remove the listener after the handler is dead.
        // We solve this by being the handler to create the pointer to the bool
        // flag that tells if the register has been removed (see
        // HandlerRegistration.h)
        SafePointer<bool> erased = new bool(false);
        
        _registrationBundle.push_back(erased);

        return registration_type(&_bundle, _bundle.end(), erased);
    }
    
    template <class L, class E> void Handler<L,E>::fire(Handler<L,E>::event_type evt)
    {
        typename bundle_type::iterator it = _bundle.begin();
        
        while (it != _bundle.end())
        {
            (*it)->notify(evt);
            it++;
        }
    }
}

#endif	/* HANDLER_H */
