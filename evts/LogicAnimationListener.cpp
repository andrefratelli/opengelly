#include <evts/LogicAnimationListener.h>

using namespace eng;

LogicAnimationEvent::LogicAnimationEvent(Transformable& target, LogicEvent trigger) :
    AnimationEvent<LogicEvent>(target, trigger)
{}

LogicAnimationListener::LogicAnimationListener(Transformable& target) :
    AnimationListener<LogicAnimationEvent, LogicListener, LogicEvent>(target)
{}

