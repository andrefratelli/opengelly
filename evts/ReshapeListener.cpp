#include <evts/ReshapeListener.h>

using namespace eng;

ReshapeEvent::ReshapeEvent(int width, int height) :
    _width(width), _height(height)
{}

int ReshapeEvent::width() const { return _width; }
int ReshapeEvent::height() const { return _height; }
