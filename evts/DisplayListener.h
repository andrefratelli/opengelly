#ifndef DISPLAYLISTENER_H
#define	DISPLAYLISTENER_H

#include <evts/Listener.h>
#include <evts/Event.h>

/*
 * == DisplayEvent == DisplayListener ==
 * 
 * Utility class for display events
 */
namespace eng
{
    class DisplayEvent : public Event
    {};
    
    class DisplayListener : public Listener<DisplayEvent>
    {};
}

#endif	/* DISPLAYLISTENER_H */

