#ifndef HANDLERREGISTRATION_H
#define	HANDLERREGISTRATION_H

#include <evts/Handler.h>
#include <utils/SafePointer.h>

/*
 * == HandlerRegistration ==
 * 
 * Encapsulates the registration of a listener in a respective handler. It's sole
 * purpose is to remove the listener from the handler as soon as notifications
 * are no longer required.
 * 
 * It uses safe pointers to know if some copy has already removed the listener
 * from the handler.
 */
namespace eng
{
    template <class L, class E> class HandlerRegistration
    {
    private:
        typename Handler<L,E>::bundle_type* _bundle;
        typename Handler<L,E>::bundle_type::iterator _item;
        SafePointer<bool> _erased;
        
    public:
        HandlerRegistration(typename Handler<L,E>::bundle_type* bundle, typename Handler<L,E>::bundle_type::iterator item, SafePointer<bool> erased);
        HandlerRegistration(const HandlerRegistration<L,E>& copy);
        
        // Don't! There's no guarantee the user will keep the registration. For
        // instance, when the user wants a permanent listener, he will probably
        // throw the registration away.
        //
        //~HandlerRegistration();
        
        void remove();
    };
    
    template <class L, class E> HandlerRegistration<L,E>::HandlerRegistration(const HandlerRegistration<L,E>& copy) :
        _bundle(copy._bundle),
        _item(copy._item),
        _erased(copy._erased)
    {}
    
    template <class L, class E> HandlerRegistration<L,E>::HandlerRegistration(typename Handler<L,E>::bundle_type* bundle, typename Handler<L,E>::bundle_type::iterator item, SafePointer<bool> erased) :
        _bundle(bundle),
        _item(item),
        _erased(erased)
    {}

    template <class L, class E> void HandlerRegistration<L,E>::remove()
    {
        if (!*_erased) {
            _bundle->erase(_item);
            *_erased = true;    // Updates in all copies
        }
    }
}

#endif	/* HANDLERREGISTRATION_H */

