#include <evts/ReshapeAnimationListener.h>

using namespace eng;

ReshapeAnimationEvent::ReshapeAnimationEvent(Transformable& target, ReshapeEvent trigger) :
    AnimationEvent<ReshapeEvent>(target, trigger)
{}

ReshapeAnimationListener::ReshapeAnimationListener(Transformable& target) :
    AnimationListener<ReshapeAnimationEvent, ReshapeListener, ReshapeEvent>(target)
{}
