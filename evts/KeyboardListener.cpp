#include <evts/KeyboardListener.h>

using namespace eng;

KeyboardEvent::KeyboardEvent(bool normalMap[256], bool specialMap[256]) :
    _map(normalMap), _special(specialMap)
{}

KeyboardEvent::KeyboardEvent(const KeyboardEvent& copy) :
    _map(copy._map),
    _special(copy._special)
{}

bool KeyboardEvent::down(key_type key) const
{
    return _map[key];
}

bool KeyboardEvent::sdown(key_type key) const
{
    return _special[key];
}
