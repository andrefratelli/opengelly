#ifndef ANIMATIONLISTENER_H
#define	ANIMATIONLISTENER_H

#include <evts/Event.h>
#include <evts/Listener.h>
#include <gfx/Transformable.h>
#include <mng/WindowManager.h>
#include <evts/HandlerRegistration.h>

/*
 * == AnimationListener == AnimationEvent ==
 * 
 * Animation listeners associate a given event (keyboard, mouse, idle, etc) with
 * a transformable (visual) object. This is supposed to be a class for such
 * events, nothing more.
 * 
 * These listeners register themselfs in the window manager, and remove themselfs
 * uppon destruction.
 * 
 * The transformable is refered to as "target" and the event as "trigger".
 */
namespace eng
{
    template <class E>
    class AnimationEvent :
        public Event
    {
    public:
        typedef E event_type;
        
        AnimationEvent(Transformable& target, event_type trigger);
        
        Transformable& getTarget();
        event_type getTrigger() const;
        
    private:
        Transformable& _target;
        event_type _trigger;
    };
    
    template <class AE, class L, class E>
    class AnimationListener :
        public Listener<AE>,
        public L
    {
    public:
        AnimationListener(Transformable& target);
        ~AnimationListener();
        
        Transformable& getTarget();
        
        virtual void notify(AE evt) = 0;
        void notify(E evt);
        
    private:
        /*
         * Because we have a destructor that automatically removes the listener
         * from the handler, we cannot allow copies. Copying the listener would
         * mean that as soon as the copy died (which would probably be fast
         * enough) the original would also be removed from the handler.
         */
        AnimationListener(const AnimationListener<AE,L,E>& copy);
        
        Transformable& _target;
        HandlerRegistration<L,E> _registration;
    };
    
    template <class E>
    AnimationEvent<E>::AnimationEvent(Transformable& target, event_type trigger) :
        _target(target), _trigger(trigger)
    {}

    template <class E>
    Transformable& AnimationEvent<E>::getTarget() { return _target; }
    
    template <class E>
    typename AnimationEvent<E>::event_type AnimationEvent<E>::getTrigger() const { return _trigger; }
    
    template <class AE, class L, class E>
    AnimationListener<AE,L,E>::AnimationListener(Transformable& target) :
        _target(target),
        _registration(WindowManager::instance()->Handler<L,E>::addEventListener(this))
    {}
    
    template <class AE, class L, class E>
    AnimationListener<AE,L,E>::~AnimationListener()
    {
        // Remove the listener before dying. This is OK unless there's a copy,
        // which we do not allow
        _registration.remove();
    }
    
    template <class AE, class L, class E>
    Transformable& AnimationListener<AE,L,E>::getTarget() { return _target; }
        
    template <class AE, class L, class E>
    void AnimationListener<AE,L,E>::notify(E trigger)
    {
        notify(AE(getTarget(), trigger));
    }
    
    template <class AE, class L, class E>
    AnimationListener<AE,L,E>::AnimationListener(const AnimationListener<AE,L,E>& copy) :
        _target(copy._target),
        _registration(copy._registration)
    {}
}

#endif	/* ANIMATIONLISTENER_H */
