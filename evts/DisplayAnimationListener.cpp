#include <evts/DisplayAnimationListener.h>

using namespace eng;

DisplayAnimationEvent::DisplayAnimationEvent(Transformable& target, DisplayEvent trigger) :
    AnimationEvent<DisplayEvent>(target, trigger)
{}

DisplayAnimationListener::DisplayAnimationListener(Transformable& target) :
    AnimationListener<DisplayAnimationEvent, DisplayListener, DisplayEvent>(target)
{}

