#ifndef MOUSELISTENER_H
#define	MOUSELISTENER_H

#include <evts/Listener.h>

/*
 * == MouseEvent == MouseListener ==
 * 
 * Base class for mouse events.
 * 
 * WARNING: never warp the cursor during such events unless you have some control
 * over the frequency it happens. See MouseAnimationListener.h for details.
 */
namespace eng
{
    class MouseEvent
    {
    public:
        MouseEvent(int button, int state, int x, int y);
        
        int button() const;
        int state() const;
        int x() const;
        int y() const;
        
    private:
        int _button;
        int _state;
        int _x;
        int _y;
    };
    
    class MouseListener : public Listener<MouseEvent>
    {};
}

#endif	/* MOUSELISTENER_H */

