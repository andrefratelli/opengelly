#ifndef MOUSEANIMATIONLISTENER_H
#define	MOUSEANIMATIONLISTENER_H

#include <evts/AnimationListener.h>
#include <evts/MouseListener.h>
#include <shared.h>

/*
 * == MouseAnimationEvent == MouseAnimationListener ==
 * 
 * Base (utility) class for mouse animations.
 * 
 * There's an issue with this class, though. If the user tries to wrap the cursor
 * during a mouse event (glutWrapCursor) an infinite loop is created. The mouse
 * event originates another event. Therefore, this class implements a stop watch
 * which limits the interval in which mouse events are fired.
 * 
 * Mouse listeners do not have this feature! Only animations.
 */
#define DEFAULT_MOUSE_IDLE_INTERVAL 0.01f

namespace eng
{
    class MouseAnimationEvent :
        public AnimationEvent<MouseEvent>
    {
    public:
        MouseAnimationEvent(Transformable& target, MouseEvent trigger);
    };
    
    class MouseAnimationListener :
        public AnimationListener<MouseAnimationEvent, MouseListener, MouseEvent>
    {
    public:
        MouseAnimationListener(Transformable& target);
        MouseAnimationListener(Transformable& target, GLfloat idleInterval);
        
        // Animations cannot be copied. See AnimationListener.h
        //MouseAnimationListener(const MouseAnimationListener& copy);
        
        /*
         * Usually, mouse displacements create an infinite loop. This prevents it
         */
        void notify(MouseEvent evt);
        
        void setIdleInterval(GLfloat idleInterval);
        GLfloat getIdleInterval() const;
        
    private:
        CStopWatch _watch;
        GLfloat _idleInterval;
    };
}

#endif	/* MOUSEANIMATIONLISTENER_H */

