#ifndef LOGICLISTENER_H
#define	LOGICLISTENER_H

#include <evts/Event.h>
#include <evts/Listener.h>

/*
 * == LogicEvent == LogicListener ==
 * 
 * Used to dispatch events at logic time. This is the only kind of event that
 * the GameManager fires, instead of the WindowManager.
 */
namespace eng
{
    class LogicEvent : public Event
    {};
    
    class LogicListener : public Listener<LogicEvent>
    {};
}

#endif	/* LOGICLISTENER_H */
