#ifndef LOGICANIMATIONLISTENER_H
#define	LOGICANIMATIONLISTENER_H

#include <evts/AnimationListener.h>
#include <evts/LogicListener.h>

/*
 * == LogicAnimationEvent == LogicAnimationListener ==
 * 
 * Defines the base (utility) class for animations that occur during idle time.
 */
namespace eng
{
    class LogicAnimationEvent :
        public AnimationEvent<LogicEvent>
    {
    public:
        LogicAnimationEvent(Transformable& target, LogicEvent trigger);
    };
    
    class LogicAnimationListener :
        public AnimationListener<LogicAnimationEvent, LogicListener, LogicEvent>
    {
    public:
        LogicAnimationListener(Transformable& target);
    };
}

#endif	/* LOGICANIMATIONLISTENER_H */

