#ifndef KEYBOARDNOTIFICATIONEVENT_H
#define	KEYBOARDNOTIFICATIONEVENT_H

#include <evts/Event.h>
#include <evts/Listener.h>
#include <evts/KeyboardListener.h>

/*
 * == KeyboardNotificationEvent == KeyboardNotificationListener ==
 * 
 * Keyboard events are not immediately dispatched. See the implementation of
 * GameEngine::notify(IdleEvent).
 * 
 * This class is dispatches every keyboard event and should not be used directly.
 * Instead, use KeyboardListener and KeyboardEvent, which are dispatched every
 * time there is a keyboard event.
 */
namespace eng
{
    class KeyboardNotificationEvent : public Event
    {
    public:
        KeyboardNotificationEvent(KeyboardEvent::key_type key, bool special, KeyEvent type);
        
        KeyboardEvent::key_type key() const;
        bool special() const;
        KeyEvent event() const;
        
    private:
        KeyboardEvent::key_type _key;
        bool _special;
        KeyEvent _event;
    };
    
    class KeyboardNotificationListener : public Listener<KeyboardNotificationEvent>
    {};
}

#endif	/* KEYBOARDNOTIFICATIONEVENT_H */

