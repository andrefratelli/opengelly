#ifndef DISPLAYANIMATIONLISTENER_H
#define	DISPLAYANIMATIONLISTENER_H

#include <evts/AnimationListener.h>

/*
 * == DisplayAnimationEvent == DisplayAnimationListener ==
 * 
 * An utility class to facilitate the creation of display animation events.
 */
namespace eng
{
    class DisplayAnimationEvent :
        public AnimationEvent<DisplayEvent>
    {
    public:
        DisplayAnimationEvent(Transformable& target, DisplayEvent trigger);
    };
    
    class DisplayAnimationListener :
        public AnimationListener<DisplayAnimationEvent, DisplayListener, DisplayEvent>
    {
    public:
        DisplayAnimationListener(Transformable& target);
    };
}

#endif	/* DISPLAYANIMATIONLISTENER_H */
