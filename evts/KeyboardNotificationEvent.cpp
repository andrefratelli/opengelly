#include <evts/KeyboardNotificationEvent.h>

using namespace eng;

KeyboardNotificationEvent::KeyboardNotificationEvent(KeyboardEvent::key_type key, bool special, KeyEvent event) :
    _key(key), _special(special), _event(event)
{}

KeyboardEvent::key_type KeyboardNotificationEvent::key() const
{
    return _key;
}

bool KeyboardNotificationEvent::special() const
{
    return _special;
}

KeyEvent KeyboardNotificationEvent::event() const
{
    return _event;
}
