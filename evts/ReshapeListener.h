#ifndef RESHAPELISTENER_H
#define	RESHAPELISTENER_H

#include <evts/Listener.h>
#include <evts/Event.h>

/*
 * == ReshapeEvent == ReshapeListener ==
 * 
 * These events are fired by the window manager when the window is resized.
 */
namespace eng
{
    class ReshapeEvent : public Event
    {
    public:
        ReshapeEvent(int width, int height);
        
        int width() const;
        int height() const;
        
    private:
        int _width;
        int _height;
    };
    
    class ReshapeListener : public Listener<ReshapeEvent>
    {};
}

#endif	/* RESHAPELISTENER_H */

