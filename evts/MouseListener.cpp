#include <evts/MouseListener.h>

using namespace eng;

MouseEvent::MouseEvent(int button, int state, int x, int y) :
    _button(button), _state(state), _x(x), _y(y)
{}

int MouseEvent::button() const { return _button; }
int MouseEvent::state() const { return _state; }
int MouseEvent::x() const { return _x; }
int MouseEvent::y() const { return _y; }
