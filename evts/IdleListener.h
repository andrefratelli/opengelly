#ifndef IDLELISTENER_H
#define	IDLELISTENER_H

#include <evts/Event.h>
#include <evts/Listener.h>

/*
 * == IdleListener == IdleEvent ==
 * 
 * Base class for idle events and listeners. It's just an utility class
 */
namespace eng
{
    class IdleEvent : public Event
    {};
    
    class IdleListener : public Listener<IdleEvent>
    {};
}

#endif	/* IDLELISTENER_H */

