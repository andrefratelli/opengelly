#ifndef IDLEANIMATIONLISTENER_H
#define	IDLEANIMATIONLISTENER_H

#include <evts/AnimationListener.h>
#include <evts/IdleListener.h>

/*
 * == IdleAnimationEvent == IdleAnimationListener ==
 * 
 * Base class for all idle animation events. It's merely an utility class
 */
namespace eng
{
    class IdleAnimationEvent :
        public AnimationEvent<IdleEvent>
    {
    public:
        IdleAnimationEvent(Transformable& target, IdleEvent trigger);
    };
    
    class IdleAnimationListener :
        public AnimationListener<IdleAnimationEvent, IdleListener, IdleEvent>
    {
    public:
        IdleAnimationListener(Transformable& target);
    };
}
#endif	/* IDLEANIMATIONLISTENER_H */

