#include <evts/MouseAnimationListener.h>

using namespace eng;

MouseAnimationEvent::MouseAnimationEvent(Transformable& target, MouseEvent trigger) :
    AnimationEvent<MouseEvent>(target, trigger)
{}

MouseAnimationListener::MouseAnimationListener(Transformable& target) :
    AnimationListener<MouseAnimationEvent, MouseListener, MouseEvent>(target),
    _idleInterval(DEFAULT_MOUSE_IDLE_INTERVAL)
{}

MouseAnimationListener::MouseAnimationListener(Transformable& target, GLfloat idleInterval) :
    AnimationListener<MouseAnimationEvent, MouseListener, MouseEvent>(target),
    _idleInterval(idleInterval)
{}

void MouseAnimationListener::notify(MouseEvent evt)
{
    // Wrapping the cursor creates an endless loop, as the event keeps being
    // triggered. We need to avoid that loop, so we will be restraining the
    // mouse interval here.
    if (_watch.GetElapsedSeconds() >= _idleInterval)
    {
        AnimationListener<MouseAnimationEvent, MouseListener, MouseEvent>::notify(evt);
        
        _watch.Reset();
    }
}

void MouseAnimationListener::setIdleInterval(GLfloat idleInterval) { _idleInterval = idleInterval; }

GLfloat MouseAnimationListener::getIdleInterval() const { return _idleInterval; }
