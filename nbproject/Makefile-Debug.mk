#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=g++
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/shared/src/GLTriangleBatch.o \
	${OBJECTDIR}/model/Object.o \
	${OBJECTDIR}/utils/hash_map.o \
	${OBJECTDIR}/mng/WindowManager.o \
	${OBJECTDIR}/evts/HandlerRegistration.o \
	${OBJECTDIR}/evts/Event.o \
	${OBJECTDIR}/evts/DisplayListener.o \
	${OBJECTDIR}/gfx/WorldTransformation.o \
	${OBJECTDIR}/anim/FollowKeyboard.o \
	${OBJECTDIR}/shared/src/GLShaderManager.o \
	${OBJECTDIR}/phy/Collision.o \
	${OBJECTDIR}/evts/IdleAnimationListener.o \
	${OBJECTDIR}/anim/FollowFPS.o \
	${OBJECTDIR}/mng/lex.yy.o \
	${OBJECTDIR}/gfx/ParticleSystem.o \
	${OBJECTDIR}/evts/KeyboardAnimationListener.o \
	${OBJECTDIR}/mng/obj.tab.o \
	${OBJECTDIR}/model/Vertex.o \
	${OBJECTDIR}/mng/GameEngine.o \
	${OBJECTDIR}/mng/VisualResource.o \
	${OBJECTDIR}/model/Buffers.o \
	${OBJECTDIR}/evts/ReshapeListener.o \
	${OBJECTDIR}/evts/AnimationListener.o \
	${OBJECTDIR}/gfx/Transformable.o \
	${OBJECTDIR}/evts/LogicListener.o \
	${OBJECTDIR}/evts/MouseAnimationListener.o \
	${OBJECTDIR}/anim/Melt.o \
	${OBJECTDIR}/utils/SafePointer.o \
	${OBJECTDIR}/evts/KeyboardNotificationEvent.o \
	${OBJECTDIR}/mng/ShaderManager.o \
	${OBJECTDIR}/gfx/Camera.o \
	${OBJECTDIR}/mng/Resource.o \
	${OBJECTDIR}/shared/src/GLTools.o \
	${OBJECTDIR}/anim/FollowArrows.o \
	${OBJECTDIR}/evts/MouseListener.o \
	${OBJECTDIR}/gfx/Transformation.o \
	${OBJECTDIR}/phy/moller.o \
	${OBJECTDIR}/gfx/VisualObject.o \
	${OBJECTDIR}/model/FacePoint.o \
	${OBJECTDIR}/gfx/Frustum.o \
	${OBJECTDIR}/phy/CollisionDetection.o \
	${OBJECTDIR}/utils/Color.o \
	${OBJECTDIR}/phy/CollisionResult.o \
	${OBJECTDIR}/evts/LogicAnimationListener.o \
	${OBJECTDIR}/utils/hash_fun.o \
	${OBJECTDIR}/anim/Fx.o \
	${OBJECTDIR}/model/BoundBox.o \
	${OBJECTDIR}/shared/src/math3d.o \
	${OBJECTDIR}/anim/FollowASDW.o \
	${OBJECTDIR}/model/Group.o \
	${OBJECTDIR}/evts/Handler.o \
	${OBJECTDIR}/evts/IdleListener.o \
	${OBJECTDIR}/evts/ReshapeAnimationListener.o \
	${OBJECTDIR}/evts/KeyboardListener.o \
	${OBJECTDIR}/shared/src/GLBatch.o \
	${OBJECTDIR}/mng/ResourceManager.o \
	${OBJECTDIR}/gfx/LocalTransformation.o \
	${OBJECTDIR}/anim/FollowMouseRot.o \
	${OBJECTDIR}/model/Normal.o \
	${OBJECTDIR}/evts/DisplayAnimationListener.o \
	${OBJECTDIR}/mng/Renderer.o \
	${OBJECTDIR}/shared/src/glew.o \
	${OBJECTDIR}/model/Octree.o \
	${OBJECTDIR}/mng/TextureResource.o \
	${OBJECTDIR}/evts/Listener.o \
	${OBJECTDIR}/model/Face.o \
	${OBJECTDIR}/anim/FollowMouse.o \
	${OBJECTDIR}/model/Texture.o


# C Compiler Flags
CFLAGS=-L/usr/X11R6/lib -L/usr/X11R6/lib64 -L/usr/local/lib -I/usr/include -I/usr/local/include -I/usr/include/GL -lX11 -lglut -lGL -lGLU -lm

# CC Compiler Flags
CCFLAGS=-L/usr/X11R6/lib -L/usr/X11R6/lib64 -L/usr/local/lib -I/usr/include -I/usr/local/include -I/usr/include/GL -lX11 -lglut -lGL -lGLU -lm
CXXFLAGS=-L/usr/X11R6/lib -L/usr/X11R6/lib64 -L/usr/local/lib -I/usr/include -I/usr/local/include -I/usr/include/GL -lX11 -lglut -lGL -lGLU -lm

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libopengelly.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libopengelly.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libopengelly.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libopengelly.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libopengelly.a

${OBJECTDIR}/shared/src/GLTriangleBatch.o: shared/src/GLTriangleBatch.cpp 
	${MKDIR} -p ${OBJECTDIR}/shared/src
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/shared/src/GLTriangleBatch.o shared/src/GLTriangleBatch.cpp

${OBJECTDIR}/model/Object.o: model/Object.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/Object.o model/Object.cpp

${OBJECTDIR}/utils/hash_map.o: utils/hash_map.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/utils/hash_map.o utils/hash_map.cpp

${OBJECTDIR}/mng/WindowManager.o: mng/WindowManager.cpp 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/WindowManager.o mng/WindowManager.cpp

${OBJECTDIR}/evts/HandlerRegistration.o: evts/HandlerRegistration.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/HandlerRegistration.o evts/HandlerRegistration.cpp

${OBJECTDIR}/evts/Event.o: evts/Event.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/Event.o evts/Event.cpp

${OBJECTDIR}/evts/DisplayListener.o: evts/DisplayListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/DisplayListener.o evts/DisplayListener.cpp

${OBJECTDIR}/gfx/WorldTransformation.o: gfx/WorldTransformation.cpp 
	${MKDIR} -p ${OBJECTDIR}/gfx
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/gfx/WorldTransformation.o gfx/WorldTransformation.cpp

${OBJECTDIR}/anim/FollowKeyboard.o: anim/FollowKeyboard.cpp 
	${MKDIR} -p ${OBJECTDIR}/anim
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/anim/FollowKeyboard.o anim/FollowKeyboard.cpp

${OBJECTDIR}/shared/src/GLShaderManager.o: shared/src/GLShaderManager.cpp 
	${MKDIR} -p ${OBJECTDIR}/shared/src
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/shared/src/GLShaderManager.o shared/src/GLShaderManager.cpp

${OBJECTDIR}/phy/Collision.o: phy/Collision.cpp 
	${MKDIR} -p ${OBJECTDIR}/phy
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/phy/Collision.o phy/Collision.cpp

${OBJECTDIR}/evts/IdleAnimationListener.o: evts/IdleAnimationListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/IdleAnimationListener.o evts/IdleAnimationListener.cpp

${OBJECTDIR}/anim/FollowFPS.o: anim/FollowFPS.cpp 
	${MKDIR} -p ${OBJECTDIR}/anim
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/anim/FollowFPS.o anim/FollowFPS.cpp

${OBJECTDIR}/mng/lex.yy.o: mng/lex.yy.c 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.c) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/lex.yy.o mng/lex.yy.c

${OBJECTDIR}/gfx/ParticleSystem.o: gfx/ParticleSystem.cpp 
	${MKDIR} -p ${OBJECTDIR}/gfx
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/gfx/ParticleSystem.o gfx/ParticleSystem.cpp

${OBJECTDIR}/evts/KeyboardAnimationListener.o: evts/KeyboardAnimationListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/KeyboardAnimationListener.o evts/KeyboardAnimationListener.cpp

${OBJECTDIR}/mng/obj.tab.o: mng/obj.tab.c 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.c) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/obj.tab.o mng/obj.tab.c

${OBJECTDIR}/model/Vertex.o: model/Vertex.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/Vertex.o model/Vertex.cpp

${OBJECTDIR}/mng/GameEngine.o: mng/GameEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/GameEngine.o mng/GameEngine.cpp

${OBJECTDIR}/mng/VisualResource.o: mng/VisualResource.cpp 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/VisualResource.o mng/VisualResource.cpp

${OBJECTDIR}/model/Buffers.o: model/Buffers.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/Buffers.o model/Buffers.cpp

${OBJECTDIR}/evts/ReshapeListener.o: evts/ReshapeListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/ReshapeListener.o evts/ReshapeListener.cpp

${OBJECTDIR}/evts/AnimationListener.o: evts/AnimationListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/AnimationListener.o evts/AnimationListener.cpp

${OBJECTDIR}/gfx/Transformable.o: gfx/Transformable.cpp 
	${MKDIR} -p ${OBJECTDIR}/gfx
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/gfx/Transformable.o gfx/Transformable.cpp

${OBJECTDIR}/evts/LogicListener.o: evts/LogicListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/LogicListener.o evts/LogicListener.cpp

${OBJECTDIR}/evts/MouseAnimationListener.o: evts/MouseAnimationListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/MouseAnimationListener.o evts/MouseAnimationListener.cpp

${OBJECTDIR}/anim/Melt.o: anim/Melt.cpp 
	${MKDIR} -p ${OBJECTDIR}/anim
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/anim/Melt.o anim/Melt.cpp

${OBJECTDIR}/utils/SafePointer.o: utils/SafePointer.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/utils/SafePointer.o utils/SafePointer.cpp

${OBJECTDIR}/evts/KeyboardNotificationEvent.o: evts/KeyboardNotificationEvent.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/KeyboardNotificationEvent.o evts/KeyboardNotificationEvent.cpp

${OBJECTDIR}/mng/ShaderManager.o: mng/ShaderManager.cpp 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/ShaderManager.o mng/ShaderManager.cpp

${OBJECTDIR}/gfx/Camera.o: gfx/Camera.cpp 
	${MKDIR} -p ${OBJECTDIR}/gfx
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/gfx/Camera.o gfx/Camera.cpp

${OBJECTDIR}/mng/Resource.o: mng/Resource.cpp 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/Resource.o mng/Resource.cpp

${OBJECTDIR}/shared/src/GLTools.o: shared/src/GLTools.cpp 
	${MKDIR} -p ${OBJECTDIR}/shared/src
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/shared/src/GLTools.o shared/src/GLTools.cpp

${OBJECTDIR}/anim/FollowArrows.o: anim/FollowArrows.cpp 
	${MKDIR} -p ${OBJECTDIR}/anim
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/anim/FollowArrows.o anim/FollowArrows.cpp

${OBJECTDIR}/evts/MouseListener.o: evts/MouseListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/MouseListener.o evts/MouseListener.cpp

${OBJECTDIR}/gfx/Transformation.o: gfx/Transformation.cpp 
	${MKDIR} -p ${OBJECTDIR}/gfx
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/gfx/Transformation.o gfx/Transformation.cpp

${OBJECTDIR}/phy/moller.o: phy/moller.cpp 
	${MKDIR} -p ${OBJECTDIR}/phy
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/phy/moller.o phy/moller.cpp

${OBJECTDIR}/gfx/VisualObject.o: gfx/VisualObject.cpp 
	${MKDIR} -p ${OBJECTDIR}/gfx
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/gfx/VisualObject.o gfx/VisualObject.cpp

${OBJECTDIR}/model/FacePoint.o: model/FacePoint.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/FacePoint.o model/FacePoint.cpp

${OBJECTDIR}/gfx/Frustum.o: gfx/Frustum.cpp 
	${MKDIR} -p ${OBJECTDIR}/gfx
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/gfx/Frustum.o gfx/Frustum.cpp

${OBJECTDIR}/phy/CollisionDetection.o: phy/CollisionDetection.cpp 
	${MKDIR} -p ${OBJECTDIR}/phy
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/phy/CollisionDetection.o phy/CollisionDetection.cpp

${OBJECTDIR}/utils/Color.o: utils/Color.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/utils/Color.o utils/Color.cpp

${OBJECTDIR}/phy/CollisionResult.o: phy/CollisionResult.cpp 
	${MKDIR} -p ${OBJECTDIR}/phy
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/phy/CollisionResult.o phy/CollisionResult.cpp

${OBJECTDIR}/evts/LogicAnimationListener.o: evts/LogicAnimationListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/LogicAnimationListener.o evts/LogicAnimationListener.cpp

${OBJECTDIR}/utils/hash_fun.o: utils/hash_fun.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/utils/hash_fun.o utils/hash_fun.cpp

${OBJECTDIR}/anim/Fx.o: anim/Fx.cpp 
	${MKDIR} -p ${OBJECTDIR}/anim
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/anim/Fx.o anim/Fx.cpp

${OBJECTDIR}/model/BoundBox.o: model/BoundBox.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/BoundBox.o model/BoundBox.cpp

${OBJECTDIR}/shared/src/math3d.o: shared/src/math3d.cpp 
	${MKDIR} -p ${OBJECTDIR}/shared/src
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/shared/src/math3d.o shared/src/math3d.cpp

${OBJECTDIR}/anim/FollowASDW.o: anim/FollowASDW.cpp 
	${MKDIR} -p ${OBJECTDIR}/anim
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/anim/FollowASDW.o anim/FollowASDW.cpp

${OBJECTDIR}/model/Group.o: model/Group.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/Group.o model/Group.cpp

${OBJECTDIR}/evts/Handler.o: evts/Handler.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/Handler.o evts/Handler.cpp

${OBJECTDIR}/evts/IdleListener.o: evts/IdleListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/IdleListener.o evts/IdleListener.cpp

${OBJECTDIR}/evts/ReshapeAnimationListener.o: evts/ReshapeAnimationListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/ReshapeAnimationListener.o evts/ReshapeAnimationListener.cpp

${OBJECTDIR}/evts/KeyboardListener.o: evts/KeyboardListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/KeyboardListener.o evts/KeyboardListener.cpp

${OBJECTDIR}/shared/src/GLBatch.o: shared/src/GLBatch.cpp 
	${MKDIR} -p ${OBJECTDIR}/shared/src
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/shared/src/GLBatch.o shared/src/GLBatch.cpp

${OBJECTDIR}/mng/ResourceManager.o: mng/ResourceManager.cpp 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/ResourceManager.o mng/ResourceManager.cpp

${OBJECTDIR}/gfx/LocalTransformation.o: gfx/LocalTransformation.cpp 
	${MKDIR} -p ${OBJECTDIR}/gfx
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/gfx/LocalTransformation.o gfx/LocalTransformation.cpp

${OBJECTDIR}/anim/FollowMouseRot.o: anim/FollowMouseRot.cpp 
	${MKDIR} -p ${OBJECTDIR}/anim
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/anim/FollowMouseRot.o anim/FollowMouseRot.cpp

${OBJECTDIR}/model/Normal.o: model/Normal.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/Normal.o model/Normal.cpp

${OBJECTDIR}/evts/DisplayAnimationListener.o: evts/DisplayAnimationListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/DisplayAnimationListener.o evts/DisplayAnimationListener.cpp

${OBJECTDIR}/mng/Renderer.o: mng/Renderer.cpp 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/Renderer.o mng/Renderer.cpp

${OBJECTDIR}/shared/src/glew.o: shared/src/glew.c 
	${MKDIR} -p ${OBJECTDIR}/shared/src
	${RM} $@.d
	$(COMPILE.c) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/shared/src/glew.o shared/src/glew.c

${OBJECTDIR}/model/Octree.o: model/Octree.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/Octree.o model/Octree.cpp

${OBJECTDIR}/mng/TextureResource.o: mng/TextureResource.cpp 
	${MKDIR} -p ${OBJECTDIR}/mng
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/mng/TextureResource.o mng/TextureResource.cpp

${OBJECTDIR}/evts/Listener.o: evts/Listener.cpp 
	${MKDIR} -p ${OBJECTDIR}/evts
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/evts/Listener.o evts/Listener.cpp

${OBJECTDIR}/model/Face.o: model/Face.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/Face.o model/Face.cpp

${OBJECTDIR}/anim/FollowMouse.o: anim/FollowMouse.cpp 
	${MKDIR} -p ${OBJECTDIR}/anim
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/anim/FollowMouse.o anim/FollowMouse.cpp

${OBJECTDIR}/model/Texture.o: model/Texture.cpp 
	${MKDIR} -p ${OBJECTDIR}/model
	${RM} $@.d
	$(COMPILE.cc) -g -I. -Ishared/include -Ishared/include/GL -MMD -MP -MF $@.d -o ${OBJECTDIR}/model/Texture.o model/Texture.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libopengelly.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
