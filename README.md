# README #

This rep contains a game engine developed from scratch on top of OpenGL in C++ for a graduation course. By the time the project was no longer maintained, I believe it was somewhat unstable and memory and CPU hungry. [Here's a video on youtube](https://www.youtube.com/watch?v=JqKf42id0dU) showing several demos for this project. All those demos where written in less than 100 (one hundred) lines of code, which was the goal stipulated for the engine.

## Features ##

* An OBJ parser which generates the 3D model hierarchy
* Minimal bounding box calculation (rectangular over the object, not parts)
* An event system which implements the subscriber pattern for listening to events such as frame rendered, idle CPU, logic, and keyboard, mouse, and window events
* Managers, which consist of application controllers in MVC. These include managers for the application, window, resources, rendering, and shaders.
* Several shaders, with different types of lighting (Gouraud, Lambert, Phong, and Toon) and texturing. Shaders can be further expanded.
* Rendering
* Collision detection, by means of bounding boxes and octal trees
* Simple physics
* Object deformation, including Free-Form deformations and Bézier curves
* Particle systems