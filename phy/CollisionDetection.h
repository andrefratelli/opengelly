#ifndef COLLISIONDETECTION_H
#define	COLLISIONDETECTION_H

#include <gfx/VisualObject.h>
#include <phy/PhyDecls.h>
#include <phy/CollisionResult.h>

namespace eng
{    
    class CollisionDetection
    {
    public:
        static const int NCORNERS = 8;
        static const int NFACES = 6;
        static const int NQUADRANTS = 8;
        
        static GLfloat fGravity;
        static GLfloat fAirResistance;
        static GLfloat fFriction;
        
        static void respond(VisualObject& fst, VisualObject& snd, CollisionResult& result);
        
        static bool collide(VisualObject& fst, VisualObject& snd, PhyCol shallowness, bool bJustCheck);
        static bool collide(
            M3DVector2f vFstX,
            M3DVector2f vFstY,
            M3DVector2f vFstZ,
            
            M3DVector2f vSndX,
            M3DVector2f vSndY,
            M3DVector2f vSndZ,
            bool bJustCheck
        );

        static void extractBBInfo(
            M3DVector3f vPoints[NCORNERS],
            const M3DVector2f vX,
            const M3DVector2f vY,
            const M3DVector2f vZ,
            VisualObject& obj
        );
        static void extractBBInfoNoTransform(
            M3DVector3f coords[NCORNERS],
            const M3DVector2f vX,
            const M3DVector2f vY,
            const M3DVector2f vZ
        );
        static void extractFaceInfo(M3DVector3f vFaces[NFACES][3], const M3DVector3f vPoints[NCORNERS]);

        static bool intersects(const M3DVector3f fstFaces[NFACES][3], const M3DVector3f sndPoints[NCORNERS]);
        static GLfloat normalProjection(const M3DVector3f vFace[3], const M3DVector3f vPoint);
    };
}

#endif	/* COLLISIONDETECTION_H */

