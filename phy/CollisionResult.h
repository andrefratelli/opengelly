#ifndef COLLISIONRESULT_H
#define	COLLISIONRESULT_H

#include <model/Face.h>
#include <shared.h>
#include <model/Octree.h>

namespace eng
{
    class CollisionResult
    {
    public:
        CollisionResult();
        
        void feed(Face* lt, Face* rt, Face::index_type index);
        void feed(Octree* lt, Octree* rt, Face::index_type index);
        
        bool getAverage(M3DVector3f vLeft, M3DVector3f vRight);
        bool getLeftAverage(M3DVector3f vAvg);
        bool getRightAverage(M3DVector3f vAvg);
        
        bool collides() const;
        
    private:
        M3DVector3f _vLeftSum;
        M3DVector3f _vRightSum;
        size_t _count;
        bool _bCollides;
        size_t _index;
    };
}

#endif	/* COLLISIONRESULT_H */

