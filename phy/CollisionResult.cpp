#include <phy/CollisionResult.h>

using namespace eng;

CollisionResult::CollisionResult() :
    _count(0),
    _bCollides(false),
    _index(0)
{
    m3dLoadVector3(_vLeftSum, 0.0f, 0.0f, 0.0f);
    m3dLoadVector3(_vRightSum, 0.0f, 0.0f, 0.0f);
}

void CollisionResult::feed(Face* lt, Face* rt, Face::index_type index)
{
    M3DVector3f vLt, vRt;
    
    if (lt->getAverage(vLt, index) && rt->getAverage(vRt, index))
    {
        m3dAddVectors3(_vLeftSum, _vLeftSum, vLt);
        m3dAddVectors3(_vRightSum, _vRightSum, vRt);

        _count++;
        _bCollides = true;
    }
}

void CollisionResult::feed(Octree* lt, Octree* rt, Face::index_type index)
{
    M3DVector3f vLt, vRt;
    
    if (lt->getAverage(vLt, index) && rt->getAverage(vRt, index))
    {
        m3dAddVectors3(_vLeftSum, _vLeftSum, vLt);
        m3dAddVectors3(_vRightSum, _vRightSum, vRt);
        
        _count++;
        _bCollides = true;
    }
}

bool CollisionResult::getAverage(M3DVector3f vLeft, M3DVector3f vRight)
{
    getLeftAverage(vLeft);
    getRightAverage(vRight);
    
    return _count != 0;
}

bool CollisionResult::getLeftAverage(M3DVector3f vAvg)
{
    if (_count != 0)
    {
        m3dLoadVector3(vAvg, _vLeftSum[0] / _count, _vLeftSum[1] / _count, _vLeftSum[2] / _count);
        return true;
    }
    else
    {
        m3dLoadVector3(vAvg, 0.0f, 0.0f, 0.0f);
        return false;
    }
}

bool CollisionResult::getRightAverage(M3DVector3f vAvg)
{
    if (_count != 0)
    {
        m3dLoadVector3(vAvg, _vRightSum[0] / _count, _vRightSum[1] / _count, _vRightSum[2] / _count);
        return true;
    }
    else
    {
        m3dLoadVector3(vAvg, 0.0f, 0.0f, 0.0f);
        return false;
    }
}

bool CollisionResult::collides() const
{
    return _bCollides;
}
