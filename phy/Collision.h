#ifndef COLLISION_H
#define	COLLISION_H

#include <shared.h>
#include <gfx/VisualObject.h>
#include <phy/PhyDecls.h>
#include <vector>
#include <phy/CollisionResult.h>

namespace eng
{
    class Collision
    {
    public:
        static bool collide(
                CollisionResult& res,
                VisualObject& fst,
                VisualObject& snd,
                PhyCol shallowness,
                bool bJustCheck
        );
        
        static bool drawBoxes;
        static bool drawFaces;
        
    private:
        static bool collide(
            CollisionResult& res,
            Octree* fst,
            Octree* snd,
            M3DMatrix44f mFst,
            M3DMatrix44f mSnd,
            Face::index_type index,
            bool bJustCheck
        );
        
        static bool deep_collide(
            CollisionResult& res,
            Octree* lt, Octree* rt,
            M3DMatrix44f mFst,
            M3DMatrix44f mSnd,
            bool bJustCheck,
            Face::index_type index
        );
        
        static bool shallow_collide(
            Octree* lt, Octree* rt,
            M3DMatrix44f mFst,
            M3DMatrix44f mSnd,
            bool bJustCheck,
            Face::index_type index
        );
        
        static GLfloat normalProjection(const M3DVector3f vFace[3], const M3DVector3f vPoint);
        static bool intersects(const M3DVector3f fstFaces[6][3], const M3DVector3f sndPoints[8]);
        
        static void draw(const M3DVector3f vLeft[8], Octree* tree, const M3DMatrix44f mMatrix);
    };
}

#endif	/* COLLISION_H */

