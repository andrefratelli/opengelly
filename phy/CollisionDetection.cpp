#include <phy/CollisionDetection.h>
#include <gfx/Camera.h>
#include <mng/Renderer.h>
#include <phy/Collision.h>
#include <phy/moller.h>
#include <phy/CollisionResult.h>
#include <iostream>

using namespace eng;
using namespace std;

#define MIN 0
#define MAX 1

#define X 0
#define Y 1
#define Z 2

GLfloat CollisionDetection::fGravity = 0.01f;   // 0.01
GLfloat CollisionDetection::fAirResistance = 0.001f;
GLfloat CollisionDetection::fFriction = 0.3f;

#define T(s,v,m)                        \
    m3dTransformVector3(s,v,m);         \
    s[0] /= m[15];                      \
    s[1] /= m[15];                      \
    s[2] /= m[15];
#define CPYM_NOT(d,m) m3dCopyMatrix44(d,m); d[12] = d[13] = d[14] = 0
#define CE(v,d)                                         \
        v[0] = v[0] > -d && v[0] < d ? 0.0f : v[0];     \
        v[1] = v[1] > -d && v[1] < d ? 0.0f : v[1];     \
        v[2] = v[2] > -d && v[2] < d ? 0.0f : v[2];

#define ABS(s,v)                                \
    s[0] = v[0] < 0.0f ? -v[0] : v[0];          \
    s[1] = v[1] < 0.0f ? -v[1] : v[1];          \
    s[2] = v[2] < 0.0f ? -v[2] : v[2];

void CollisionDetection::respond(VisualObject& fst, VisualObject& snd, CollisionResult& result)
{
    if (result.collides())
    {
        fst.collide(snd);
        snd.collide(fst);
    }
    
    M3DVector3f vLeftLocalSpeed, vRightLocalSpeed;
    M3DMatrix44f mFstNoT, mSndNoT;
    M3DVector3f vLeftTotal, vRightTotal;
    M3DVector3f vLeftAvg, vRightAvg;
    M3DVector3f vResult;
    M3DVector3f vZero = {0.0f, 0.0f, 0.0f};
    
    // Local speed in world coordinates
    CPYM_NOT(mFstNoT, fst.getMatrix());
    CPYM_NOT(mSndNoT, snd.getMatrix());
    
    T(vLeftLocalSpeed, fst.local().getSpeed(), mFstNoT);
    T(vRightLocalSpeed, snd.local().getSpeed(), mSndNoT);
    
    // Total speed
    m3dAddVectors3(vLeftTotal, vLeftLocalSpeed, fst.world().getSpeed());
    m3dAddVectors3(vRightTotal, vRightLocalSpeed, snd.world().getSpeed());
    
    // Load averages from collision results
    result.getAverage(vLeftAvg, vRightAvg);
    
    if (fst.getPhy() && snd.getPhy())
    {
        M3DVector3f vTotalBoth;
        GLfloat mag;
        
        m3dAddVectors3(vTotalBoth, vLeftTotal, vRightTotal);
        mag = m3dGetVectorLength3(vTotalBoth) / 2.0f;
        
        m3dNormalizeVector3(vLeftAvg);
        m3dNormalizeVector3(vRightAvg);
        
        vLeftAvg[0] *= mag;
        vLeftAvg[1] *= mag;
        vLeftAvg[2] *= mag;
        
        vRightAvg[0] *= mag;
        vRightAvg[1] *= mag;
        vRightAvg[2] *= mag;
        
        fst.world().setSpeed(vRightAvg);
        fst.local().setSpeed(vZero);
        fst.setStalling(false);
        
        snd.world().setSpeed(vLeftAvg);
        snd.local().setSpeed(vZero);
        snd.setStalling(false);
    }
    else if (fst.getPhy())
    {
        GLfloat mag = m3dGetVectorLength3(vLeftTotal);
        
        if (m3dCloseEnough(mag, 0.0f, 0.00001f))
        {
            snd.setStalling(true);
        }
        
        m3dNormalizeVector3(vRightAvg);
        m3dNormalizeVector3(vLeftTotal);
        
        GLfloat dot = m3dDotProduct3(vRightAvg, vLeftTotal);
        
        vResult[0] = -2.0f * vRightAvg[0] * dot;
        vResult[1] = -2.0f * vRightAvg[1] * dot;
        vResult[2] = -2.0f * vRightAvg[2] * dot;
        
        vResult[0] = (vLeftTotal[0] + vResult[0]);
        vResult[1] = (vLeftTotal[1] + vResult[1]);
        vResult[2] = (vLeftTotal[2] + vResult[2]);
        
        m3dNormalizeVector3(vResult);
        
        vResult[0] *= mag * fFriction;
        vResult[1] *= mag * fFriction;
        vResult[2] *= mag * fFriction;
        
        fst.world().setSpeed(vResult);
        fst.local().setSpeed(vZero);
    }
    else if (snd.getPhy())
    {
        GLfloat mag = m3dGetVectorLength3(vRightTotal);
        
        if (m3dCloseEnough(mag, 0.0f, 0.00001f))
        {
//            if (!(vRightTotal[0] > vRightTotal[1] || vRightTotal[2] > vRightTotal[1]))
            {
                snd.setStalling(true);
            }
        }
        
        m3dNormalizeVector3(vLeftAvg);
        m3dNormalizeVector3(vRightTotal);
        
        GLfloat dot = m3dDotProduct3(vLeftAvg, vRightTotal);
        
        vResult[0] = -2.0f * vLeftAvg[0] * dot;
        vResult[1] = -2.0f * vLeftAvg[1] * dot;
        vResult[2] = -2.0f * vLeftAvg[2] * dot;
        
        vResult[0] = (vRightTotal[0] + vResult[0]);
        vResult[1] = (vRightTotal[1] + vResult[1]);
        vResult[2] = (vRightTotal[2] + vResult[2]);
        
        m3dNormalizeVector3(vResult);
        
        vResult[0] *= mag * fFriction;
        vResult[1] *= mag * fFriction;
        vResult[2] *= mag * fFriction;
        
        snd.world().setSpeed(vResult);
        snd.local().setSpeed(vZero);
    }
}

#undef ABS
#undef CPYM_NOT
#undef T
#undef CE

bool CollisionDetection::collide(VisualObject& fst, VisualObject& snd, PhyCol shallowness, bool bJustCheck)
{
    if (!fst.getPhyEnabled() || !snd.getPhyEnabled()) { return false; }
    
    if (fst.isParticle() && snd.isParticle())
    {
        return false;
    }
    
    if (fst.getPhy() || snd.getPhy())
    {
        if (fst.getPhy() && snd.getPhy())
        {
            if (!(fst.getStalling() && snd.getStalling()))
            {
                CollisionResult collision;

                if (Collision::collide(collision, fst, snd, shallowness, bJustCheck))
                {
                    fst.world().setAcceleration(0.0f, 0.0f, 0.0f);
                    snd.world().setAcceleration(0.0f, 0.0f, 0.0f);

                    fst.setStalling(false);
                    snd.setStalling(false);

                    respond(fst, snd, collision);
                }
                //else do nothing
            }
        }
        else
        {
            if (!fst.getPhy() && snd.getStalling())
            {
                snd.world().setSpeed(0.0f, 0.0f, 0.0f);
                return true;
            }
            
            if (!snd.getPhy() && fst.getStalling())
            {
                fst.world().setSpeed(0.0f, 0.0f, 0.0f);
                return true;
            }
            
            CollisionResult collision;

            if (Collision::collide(collision, fst, snd, shallowness, bJustCheck))
            {
                fst.world().setAcceleration(0.0f, 0.0f, 0.0f);
                snd.world().setAcceleration(0.0f, 0.0f, 0.0f);

                respond(fst, snd, collision);

                return true;
            }
            else
            {
                if (fst.getPhy()) fst.world().setAcceleration(0.0f, -CollisionDetection::fGravity, 0.0f);
                if (snd.getPhy()) snd.world().setAcceleration(0.0f, -CollisionDetection::fGravity, 0.0f);
            }
        }
    }
    return false;
}

bool CollisionDetection::collide(
    M3DVector2f vFstX,
    M3DVector2f vFstY,
    M3DVector2f vFstZ,

    M3DVector2f vSndX,
    M3DVector2f vSndY,
    M3DVector2f vSndZ,
    bool bJustCheck
)
{
    M3DVector3f fstPoints[NCORNERS], sndPoints[NCORNERS];
    M3DVector3f vFstFaces[NFACES][3], vSndFaces[NFACES][3];
    
    extractBBInfoNoTransform(fstPoints, vFstX, vFstY, vFstZ);
    extractBBInfoNoTransform(sndPoints, vSndX, vSndY, vSndZ);
    
    extractFaceInfo(vFstFaces, fstPoints);
    extractFaceInfo(vSndFaces, sndPoints);
    
    return intersects(vFstFaces, sndPoints) && intersects(vSndFaces, fstPoints);
}

void CollisionDetection::extractBBInfo(
    M3DVector3f vPoints[NCORNERS],
    const M3DVector2f vX,
    const M3DVector2f vY,
    const M3DVector2f vZ,
    VisualObject& obj
)
{
    M3DVector3f coords[NCORNERS];
    
    extractBBInfoNoTransform(coords, vX, vY, vZ);
    
    for (size_t point=0 ; point<NCORNERS ; point++)
    {
        obj.transform(vPoints[point], coords[point], true);
    }
}

void CollisionDetection::extractBBInfoNoTransform(
    M3DVector3f coords[NCORNERS],
    const M3DVector2f vX,
    const M3DVector2f vY,
    const M3DVector2f vZ
)
{
    m3dLoadVector3(coords[0], vX[MIN], vY[MIN], vZ[MIN]);
    m3dLoadVector3(coords[1], vX[MAX], vY[MIN], vZ[MIN]);
    m3dLoadVector3(coords[2], vX[MAX], vY[MAX], vZ[MIN]);
    m3dLoadVector3(coords[3], vX[MIN], vY[MAX], vZ[MIN]);
    
    m3dLoadVector3(coords[4], vX[MIN], vY[MAX], vZ[MAX]);
    m3dLoadVector3(coords[5], vX[MIN], vY[MIN], vZ[MAX]);
    m3dLoadVector3(coords[6], vX[MAX], vY[MIN], vZ[MAX]);
    m3dLoadVector3(coords[7], vX[MAX], vY[MAX], vZ[MAX]);
}

/*
 * M3DVector3f vFaces[NFACES][3]
 * 
 *  A box is defined by NFACES faces, where each face is defined by tree points,
 *  and each point is a vector.
 */
void CollisionDetection::extractFaceInfo(M3DVector3f vFaces[NFACES][3], const M3DVector3f vPoints[NCORNERS])
{
    m3dCopyVector3(vFaces[0][0], vPoints[0]);
    m3dCopyVector3(vFaces[0][1], vPoints[3]);
    m3dCopyVector3(vFaces[0][2], vPoints[2]);
    
    m3dCopyVector3(vFaces[1][0], vPoints[7]);
    m3dCopyVector3(vFaces[1][1], vPoints[4]);
    m3dCopyVector3(vFaces[1][2], vPoints[5]);

    m3dCopyVector3(vFaces[2][0], vPoints[3]);
    m3dCopyVector3(vFaces[2][1], vPoints[4]);
    m3dCopyVector3(vFaces[2][2], vPoints[7]);
    
    m3dCopyVector3(vFaces[3][0], vPoints[0]);
    m3dCopyVector3(vFaces[3][1], vPoints[1]);
    m3dCopyVector3(vFaces[3][2], vPoints[6]);
    
    m3dCopyVector3(vFaces[4][0], vPoints[1]);
    m3dCopyVector3(vFaces[4][1], vPoints[2]);
    m3dCopyVector3(vFaces[4][2], vPoints[7]);
    
    m3dCopyVector3(vFaces[5][0], vPoints[4]);
    m3dCopyVector3(vFaces[5][1], vPoints[3]);
    m3dCopyVector3(vFaces[5][2], vPoints[0]);
}

bool CollisionDetection::intersects(const M3DVector3f fstFaces[NFACES][3], const M3DVector3f sndPoints[NCORNERS])
{
    for (unsigned face=0 ; face<NFACES ; face++)
    {
        bool positive = true;
        
        for (unsigned corner=0 ; corner<NCORNERS ; corner++)
        {
            GLfloat proj = normalProjection(fstFaces[face], sndPoints[corner]);
            
            if (proj < 0.0f) {
                positive = false;
                break;
            }
        }
        
        if (positive) { return false; }
    }
    return true;
}

GLfloat CollisionDetection::normalProjection(const M3DVector3f vFace[3], const M3DVector3f vPoint)
{
    M3DVector3f vForward, vUp, vNormal;
    
    m3dSubtractVectors3(vForward, vFace[1], vFace[0]);
    m3dSubtractVectors3(vUp, vFace[1], vFace[2]);
    m3dCrossProduct3(vNormal, vUp, vForward);
    m3dNormalizeVector3(vNormal);
    
    M3DVector3f vVector;
    m3dSubtractVectors3(vVector, vPoint, vFace[0]);
    
    return m3dDotProduct3(vVector, vNormal);
}

#undef MIN
#undef MAX

#undef X
#undef Y
#undef Z
