#include <phy/Collision.h>
#include <shared.h>
#include <gfx/VisualObject.h>
#include <phy/PhyDecls.h>
#include <gfx/Camera.h>
#include <mng/Renderer.h>
#include <phy/moller.h>

using namespace eng;

bool Collision::drawBoxes = true;
bool Collision::drawFaces = true;

bool Collision::collide(CollisionResult& res, VisualObject& fst, VisualObject& snd, PhyCol shallowness, bool bJustCheck)
{
    static Face::index_type index = 0;
    
    if (res.collides())
    {
        if (!fst.collide(snd)) { return false; }
        if (!snd.collide(snd)) { return false; }
    }
    
    index++;
    
    Octree* fstTree = fst.resource().getObject()->boundbox()->octree();
    Octree* sndTree = snd.resource().getObject()->boundbox()->octree();

    if (!fstTree->computed()) { fstTree->computeOctree(fst.getOctDepth(), fst.resource().getObject()->faces()); }
    if (!sndTree->computed()) { sndTree->computeOctree(snd.getOctDepth(), snd.resource().getObject()->faces()); }
    
    bool bCollides = false;
    
    if (shallowness == PHY_DEEP)
    {
        if (!(fst.getStalling() || snd.getStalling()))
        {
            M3DVector3f vSpeed, vZero = {0.0f, 0.0f, 0.0f}, vAux;
            CollisionResult tempRes;
            size_t count = 0;

            snd.getSpeed(vAux);
            m3dSubtractVectors3(vSpeed, vZero, vAux);

            while (m3dGetVectorLength3(vSpeed) > 0.5f && count < 2 && deep_collide(tempRes, fstTree, sndTree, fst.getMatrix(), snd.getMatrix(), true, index))
            {
                vSpeed[0] /= 2;
                vSpeed[1] /= 2;
                vSpeed[2] /= 2;

                snd.getMatrix()[12] = snd.getMatrix()[12] + vSpeed[0];
                snd.getMatrix()[13] = snd.getMatrix()[13] + vSpeed[1];
                snd.getMatrix()[14] = snd.getMatrix()[14] + vSpeed[2];

                index++;
                count++;
            }
        }
        bCollides = deep_collide(res, fstTree, sndTree, fst.getMatrix(), snd.getMatrix(), bJustCheck, index);
    }
    else
    {
        bCollides = shallow_collide(fstTree, sndTree, fst.getMatrix(), snd.getMatrix(), bJustCheck, index);
        bCollides |= shallow_collide(sndTree, fstTree, snd.getMatrix(), fst.getMatrix(), bJustCheck, index);
    }
    
    return bCollides;
}

bool Collision::collide(
        CollisionResult& res,
        Octree* fst,
        Octree* snd,
        M3DMatrix44f mFst,
        M3DMatrix44f mSnd,
        Face::index_type index,
        bool bJustCheck
)
{
    Octree::faces_vpt::iterator it = fst->getFaces().begin();
    Octree::faces_vpt::iterator iend = fst->getFaces().end();
    bool bCollision = false;
    
    while (it != iend)
    {
        Octree::faces_vpt::iterator jt = snd->getFaces().begin();
        Octree::faces_vpt::iterator jend = snd->getFaces().end();
        Face::M3DTriangle& fstCorners = (*it)->extractCornerInfo(mFst, index);
        
        while (jt != jend)
        {
            Face::M3DTriangle& sndCorners = (*jt)->extractCornerInfo(mSnd, index);
            
            if (tri_tri_intersect(fstCorners[2], fstCorners[1], fstCorners[0], sndCorners[2], sndCorners[1], sndCorners[0]))
            {
                res.feed(*it, *jt, index);
                
                if (bJustCheck) { return true; }
                bCollision = true;
            }
            
            jt++;
        }
        
        it++;
    }
    return bCollision;
}

bool Collision::deep_collide(
    CollisionResult& res,
    Octree* lt, Octree* rt,
    M3DMatrix44f mFst,
    M3DMatrix44f mSnd,
    bool bJustCheck,
    Face::index_type index
)
{
    bool bFlag = false;
    
    if (lt->getFaceCount() > 0 && rt->getFaceCount() > 0)
    {
        Octree::M3DCornerVector& ltPoints = lt->extractCornerInfo(mFst, index);
        Octree::M3DCornerVector& rtPoints = rt->extractCornerInfo(mSnd, index);

        Octree::M3DFaceVector& ltFaces = lt->extractFaceInfo();
        Octree::M3DFaceVector& rtFaces = rt->extractFaceInfo();
        
        if (intersects(ltFaces, rtPoints) && intersects(rtFaces, ltPoints))
        {
            if (lt->isLeaf())
            {
                if (rt->isLeaf())
                {
//                    draw(ltPoints, lt, mFst);
//                    draw(rtPoints, rt, mSnd);
                    
//                    res.feed(lt, rt, index);
//                    return true;
                    
                    return collide(res, lt, rt, mFst, mSnd, index, bJustCheck);
                }
                else for (size_t it=0 ; it<8 ; it++)
                    {
                        if (rt->children()[it] != NULL)
                        {
                            bFlag |= deep_collide(res, lt, rt->children()[it], mFst, mSnd, bJustCheck, index);
                        
                            if (bJustCheck && bFlag) { return true; }
                        }
                    }
            }
            else
            {
                if (rt->isLeaf())
                {
                    for (size_t it=0 ; it<8 ; it++)
                    {
                        if (lt->children()[it] != NULL)
                        {
                            bFlag |= deep_collide(res, lt->children()[it], rt, mFst, mSnd, bJustCheck, index);

                            if (bJustCheck && bFlag) { return true; }
                        }
                    }
                }
                else for (size_t it=0 ; it<8 ; it++)
                    {
                        Octree* lchild = lt->children()[it];
                    
                        if (lchild != NULL && lchild->getFaceCount() > 0)
                        {
                            Octree::M3DCornerVector& lChildPoints = lchild->extractCornerInfo(mFst, index);
                            Octree::M3DFaceVector& lChildFaces = lchild->extractFaceInfo();
                            
                            if (intersects(lChildFaces, rtPoints) && intersects(rtFaces, lChildPoints))
                            {
                                for (size_t jt=0 ; jt<8 ; jt++)
                                {
                                    Octree* rchild = rt->children()[jt];
                                    
                                    if (rchild != NULL)
                                    {
                                        bFlag |= deep_collide(res, lchild, rchild, mFst, mSnd, bJustCheck, index);

                                        if (bJustCheck && bFlag) { return true; }
                                    }
                                }
                            }
                        }
                    }
            }
        }
    }
    return bFlag;
}

bool Collision::shallow_collide(
    Octree* lt, Octree* rt,
    M3DMatrix44f mFst,
    M3DMatrix44f mSnd,
    bool bJustCheck,
    Face::index_type index
)
{
    bool bFlag = false;
    
    if (lt->getFaceCount() > 0 && rt->getFaceCount() > 0)
    {
        Octree::M3DCornerVector& ltPoints = lt->extractCornerInfo(mFst, index);
        Octree::M3DCornerVector& rtPoints = rt->extractCornerInfo(mSnd, index);

        Octree::M3DFaceVector& ltFaces = lt->extractFaceInfo();
        Octree::M3DFaceVector& rtFaces = rt->extractFaceInfo();
        
        if (intersects(ltFaces, rtPoints) && intersects(rtFaces, ltPoints))
        {
            if (lt->isLeaf())
            {
                draw(ltPoints, lt, mFst);
                draw(rtPoints, rt, mSnd);
                
                return true;
            }
            else
            {
                for (size_t it=0 ; it<8 ; it++)
                {
                    if (lt->children()[it] != NULL)
                    {
                        bFlag |= shallow_collide(lt->children()[it], rt, mFst, mSnd, bJustCheck, index);

                        if (bJustCheck && bFlag) { return true; }
                    }                        
                }
            }
        }
    }
    return bFlag;
}

bool Collision::intersects(const M3DVector3f fstFaces[6][3], const M3DVector3f sndPoints[8])
{
    for (unsigned face=0 ; face<6 ; face++)
    {
        bool positive = true;
        
        for (unsigned corner=0 ; corner<8 ; corner++)
        {
            GLfloat proj = normalProjection(fstFaces[face], sndPoints[corner]);
            
            if (proj < 0.0f) {
                positive = false;
                break;
            }
        }
        
        if (positive) { return false; }
    }
    return true;
}

GLfloat Collision::normalProjection(const M3DVector3f vFace[3], const M3DVector3f vPoint)
{
    M3DVector3f vForward, vUp, vNormal;
    
    m3dSubtractVectors3(vForward, vFace[1], vFace[0]);
    m3dSubtractVectors3(vUp, vFace[1], vFace[2]);
    m3dCrossProduct3(vNormal, vUp, vForward);
    m3dNormalizeVector3(vNormal);
    
    M3DVector3f vVector;
    m3dSubtractVectors3(vVector, vPoint, vFace[0]);
    
    return m3dDotProduct3(vVector, vNormal);
}

void Collision::draw(const M3DVector3f vLeft[8], Octree* tree, const M3DMatrix44f mMatrix)
{
    Octree::faces_vpt& faces = tree->getFaces();
    Color c = colors::White;
    M3DMatrix44f mProjection, mTemp;
    
    m3dCopyMatrix44(mTemp, Camera::getActive()->getFrustum().getMatrix());
    m3dMatrixMultiply44(mProjection, mTemp, Camera::getActive()->getMatrix());
    
    if (drawBoxes)
    {       
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        Renderer::instance()->shaderManager()->UseCustomShader(
            CUSTOM_SHADER_FLAT,
            mProjection,
            c.getRGBA()
        );

        glBegin(GL_LINES);
            // Bottom
            glVertex3f(vLeft[0][0], vLeft[0][1], vLeft[0][2]);
            glVertex3f(vLeft[1][0], vLeft[1][1], vLeft[1][2]);

            glVertex3f(vLeft[1][0], vLeft[1][1], vLeft[1][2]);
            glVertex3f(vLeft[6][0], vLeft[6][1], vLeft[6][2]);

            glVertex3f(vLeft[6][0], vLeft[6][1], vLeft[6][2]);
            glVertex3f(vLeft[5][0], vLeft[5][1], vLeft[5][2]);

            glVertex3f(vLeft[5][0], vLeft[5][1], vLeft[5][2]);
            glVertex3f(vLeft[0][0], vLeft[0][1], vLeft[0][2]);

            // Top
            glVertex3f(vLeft[3][0], vLeft[3][1], vLeft[3][2]);
            glVertex3f(vLeft[2][0], vLeft[2][1], vLeft[2][2]);

            glVertex3f(vLeft[2][0], vLeft[2][1], vLeft[2][2]);
            glVertex3f(vLeft[7][0], vLeft[7][1], vLeft[7][2]);

            glVertex3f(vLeft[7][0], vLeft[7][1], vLeft[7][2]);
            glVertex3f(vLeft[4][0], vLeft[4][1], vLeft[4][2]);

            glVertex3f(vLeft[4][0], vLeft[4][1], vLeft[4][2]);
            glVertex3f(vLeft[3][0], vLeft[3][1], vLeft[3][2]);

            // Link
            glVertex3f(vLeft[0][0], vLeft[0][1], vLeft[0][2]);
            glVertex3f(vLeft[3][0], vLeft[3][1], vLeft[3][2]);

            glVertex3f(vLeft[1][0], vLeft[1][1], vLeft[1][2]);
            glVertex3f(vLeft[2][0], vLeft[2][1], vLeft[2][2]);

            glVertex3f(vLeft[6][0], vLeft[6][1], vLeft[6][2]);
            glVertex3f(vLeft[7][0], vLeft[7][1], vLeft[7][2]);

            glVertex3f(vLeft[5][0], vLeft[5][1], vLeft[5][2]);
            glVertex3f(vLeft[4][0], vLeft[4][1], vLeft[4][2]);
        glEnd();
    }
    
    if (drawFaces)
    {
        c = colors::BlueViolet;

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        Renderer::instance()->shaderManager()->UseCustomShader(
            CUSTOM_SHADER_FLAT,
            mProjection,
            c.getRGBA()
        );

        glBegin(GL_TRIANGLES);

            Octree::faces_vpt::iterator it = faces.begin();
            Octree::faces_vpt::iterator end = faces.end();

            while (it != end)
            {
                Vertex* first = (*it)->facepoints()[0]->vertex();
                Vertex* second = (*it)->facepoints()[1]->vertex();
                Vertex* third = (*it)->facepoints()[2]->vertex();

                M3DVector3f _first = {first->x(), first->y(), first->z()};
                M3DVector3f _second = {second->x(), second->y(), second->z()};
                M3DVector3f _third = {third->x(), third->y(), third->z()};
                M3DVector3f _aux;


                m3dTransformVector3(_aux, _first, mMatrix);
                _aux[0] /= mMatrix[15]; _aux[1] /= mMatrix[15]; _aux[2] /= mMatrix[15];
                glVertex3f(_aux[0], _aux[1], _aux[2]);

                m3dTransformVector3(_aux, _second, mMatrix);
                _aux[0] /= mMatrix[15]; _aux[1] /= mMatrix[15]; _aux[2] /= mMatrix[15];
                glVertex3f(_aux[0], _aux[1], _aux[2]);

                m3dTransformVector3(_aux, _third, mMatrix);
                _aux[0] /= mMatrix[15]; _aux[1] /= mMatrix[15]; _aux[2] /= mMatrix[15];
                glVertex3f(_aux[0], _aux[1], _aux[2]);

                it++;
            }

        glEnd();
    }
}
