#include <anim/FollowMouseRot.h>
#include <shared.h>
#include <mng/WindowManager.h>
#include <math.h>

using namespace eng;

FollowMouseRot::FollowMouseRot(Transformable& target) :
    MouseAnimationListener(target)
{}

void FollowMouseRot::notify(MouseAnimationEvent _evt)
{
    int mx = WindowManager::instance()->getWidth() >> 1;
    int my = WindowManager::instance()->getHeight() >> 1;
    static bool bInit = false;
    
    if (bInit)
    {
        GLfloat hcos = -(mx - _evt.getTrigger().x()) / (GLfloat)mx;
        GLfloat hsin = sqrt(1 - hcos * hcos);
        GLfloat hang = hcos / hsin;
        
        GLfloat vsin = -(my - _evt.getTrigger().y()) / (GLfloat)my;
        GLfloat vcos = sqrt(1 - vsin * vsin);
        GLfloat vang = vsin / vcos;

        _evt.getTarget().local().rotate(hang, 0.0f, 1.0f, 0.0f);
        _evt.getTarget().local().rotate(vang, 1.0f, 0.0f, 0.0f);
    }
    
    glutWarpPointer(mx, my);
    bInit = true;
}
