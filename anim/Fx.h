#ifndef FX_H
#define	FX_H

#include <gfx/ParticleSystem.h>
#include <gfx/VisualObject.h>

namespace eng
{
    namespace Fx
    {
        void explode(ParticleSystem& sys, GLfloat dispersion);
        void shoot(VisualObject& followee, VisualObject& follower);
    }
}

#endif	/* FX_H */

