#ifndef FOLLOWFPS_H
#define	FOLLOWFPS_H

#include <evts/LogicAnimationListener.h>
#include <evts/MouseAnimationListener.h>
#include <gfx/VisualObject.h>

namespace eng {
    class FollowFPS :
        public LogicAnimationListener
    {
    public:
        FollowFPS(Transformable& followee, Transformable& follower);
        
        void setRelativeTranslation(M3DVector3f translation);
        void setRelativeTranslation(GLfloat x, GLfloat y, GLfloat z);
        void setRelativeRotation(M3DVector4f rotation);
        void setRelativeRotation(GLfloat angle, GLfloat x, GLfloat y, GLfloat z);
        
        void notify(LogicAnimationEvent evt);
        
    private:
        Transformable& _followee;
        M3DVector3f _translation;
        M3DVector4f _rotation;
    };
}

#endif	/* FOLLOWFPS_H */

