#include <anim/FollowKeyboard.h>

using namespace eng;

FollowKeyboard::FollowKeyboard(Transformable& target, GLfloat xInc, GLfloat yInc, GLfloat zInc) :
    KeyboardAnimationListener(target)
{
    m3dLoadVector3(_vInc, xInc, yInc, zInc);
}

FollowKeyboard::FollowKeyboard(Transformable& target, M3DVector3f vInc) :
    KeyboardAnimationListener(target)
{
    m3dCopyVector3(_vInc, vInc);
}

void FollowKeyboard::notify(KeyboardAnimationEvent _evt)
{
    GLfloat angular = m3dDegToRad(1.0f);
    KeyboardEvent evt = _evt.getTrigger();

    if (evt.sdown(GLUT_KEY_UP)) { _evt.getTarget().local().translate(0.0f, 0.0f, _vInc[2]); }
    if (evt.sdown(GLUT_KEY_DOWN)) { _evt.getTarget().local().translate(0.0f, 0.0f, -_vInc[2]); }
    if (evt.sdown(GLUT_KEY_LEFT)) { _evt.getTarget().local().rotate(angular, 0.0f, -_vInc[1], 0.0f); }
    if (evt.sdown(GLUT_KEY_RIGHT)) { _evt.getTarget().local().rotate(angular, 0.0f, _vInc[1], 0.0f); }

    if (evt.down('w') || evt.down('W')) { _evt.getTarget().local().translate(0.0f, 0.0f, _vInc[2]); }
    if (evt.down('s') || evt.down('S')) { _evt.getTarget().local().translate(0.0f, 0.0f, -_vInc[2]); }
    if (evt.down('a') || evt.down('A')) { _evt.getTarget().local().translate(-_vInc[0], 0.0f, 0.0f); }
    if (evt.down('d') || evt.down('D')) { _evt.getTarget().local().translate(_vInc[0], 0.0f, 0.0f); }
}

void FollowKeyboard::getInc(M3DVector3f vInc) const { m3dCopyVector3(vInc, _vInc); }
void FollowKeyboard::setInc(const M3DVector3f vInc) { m3dCopyVector3(_vInc, vInc); }