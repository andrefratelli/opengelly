#ifndef FOLLOWKEYBOARD_H
#define	FOLLOWKEYBOARD_H

#include <evts/KeyboardAnimationListener.h>
#include <shared.h>

namespace eng
{
    class FollowKeyboard :
        public KeyboardAnimationListener
    {
    public:
        FollowKeyboard(Transformable& target, GLfloat xInc, GLfloat yInc, GLfloat zInc);
        FollowKeyboard(Transformable& target, M3DVector3f vInc);
        
        void notify(KeyboardAnimationEvent _evt);
        
        void getInc(M3DVector3f vInc) const;
        void setInc(const M3DVector3f vInc);
        
    private:
        M3DVector3f _vInc;
    };
}

#endif	/* FOLLOWKEYBOARD_H */

