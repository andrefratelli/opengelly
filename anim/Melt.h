#ifndef MELT_H
#define	MELT_H

#include <evts/LogicAnimationListener.h>
#include <gfx/VisualObject.h>
#include <vector>

namespace eng
{
    class Melt :
        public LogicAnimationListener
    {
    public:
        Melt(VisualObject& obj);
        
        void notify(LogicAnimationEvent evt);
        
    private:
        std::vector<GLfloat> _velocity;
    };
}

#endif	/* MELT_H */

