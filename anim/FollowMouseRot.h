#ifndef FOLLOWMOUSEROT_H
#define	FOLLOWMOUSEROT_H

#include <evts/MouseAnimationListener.h>

namespace eng
{
    class FollowMouseRot :
        public MouseAnimationListener
    {
    public:
        FollowMouseRot(Transformable& target);
        
        void notify(MouseAnimationEvent _evt);
    };
}

#endif	/* FOLLOWMOUSEROT_H */

