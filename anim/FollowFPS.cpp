#include <anim/FollowFPS.h>

using namespace eng;

FollowFPS::FollowFPS(Transformable& followee, Transformable& follower) :
    LogicAnimationListener(follower),
    _followee(followee)
{
    _translation[0] = -0.75f;
    _translation[1] = 1.0f;
    _translation[2] = -4.0f;
    
    _rotation[0] = -0.001f;
    _rotation[1] = 0.0f;
    _rotation[2] = 1.0f;
    _rotation[3] = 0.0f;
}

void FollowFPS::setRelativeTranslation(M3DVector3f translation) {
    m3dCopyVector3(_translation, translation);
}
void FollowFPS::setRelativeTranslation(GLfloat x, GLfloat y, GLfloat z) {
    _translation[0] = x;
    _translation[1] = y;
    _translation[2] = z;
}
void FollowFPS::setRelativeRotation(M3DVector4f rotation) {
    m3dCopyVector4(_rotation, rotation);
}

void FollowFPS::setRelativeRotation(GLfloat angle, GLfloat x, GLfloat y, GLfloat z) {
    _rotation[0] = angle;
    _rotation[1] = x;
    _rotation[2] = y;
    _rotation[3] = z;
}

void FollowFPS::notify(LogicAnimationEvent _evt)
{
    GLfloat scale = _evt.getTarget().getMatrix()[15];
    m3dCopyMatrix44(_evt.getTarget().getMatrix(), _followee.getMatrix());
    _evt.getTarget().getMatrix()[15] = scale;
    
    M3DVector3f vTranslation = {_translation[0], _translation[1], _translation[2]};
    
    vTranslation[0] *= scale;
    vTranslation[1] *= scale;
    vTranslation[2] *= scale;
    
    _evt.getTarget().local().translate(vTranslation);
    _evt.getTarget().world().rotate(_rotation[0], _rotation[1], _rotation[2], _rotation[3]);
}

