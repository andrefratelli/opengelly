#include <anim/Melt.h>
#include <vector>
#include <iostream>
#include <shared.h>
#include <anim/Fx.h>

using namespace eng;
using namespace std;

Melt::Melt(VisualObject& obj) :
    LogicAnimationListener(obj)
{
    Buffers::buffer_vt::size_type it = 0;
    Buffers::buffer_vt& pos = obj.resource().getObject()->buffers().vertexes();
    Buffers::buffer_vt& norms = obj.resource().getObject()->buffers().normals();
    
    while (it < norms.size())
    {
        _velocity.push_back(norms[it] * 0.1f);
        it++;
    }
}

void Melt::notify(LogicAnimationEvent evt)
{
    Buffers::buffer_vt& verts = ((VisualObject&)evt.getTarget()).resource().getObject()->buffers().vertexes();
    Buffers::buffer_vt::size_type it = 0;
    Buffers::buffer_vt::size_type iend = verts.size();
    static CStopWatch w;
    static CStopWatch end;
    
//    if (end.GetElapsedSeconds() > 5.0f) { return; }
    
    if (w.GetElapsedSeconds() > 0.07f)
    {
        w.Reset();
        
        while (it < iend)
        {
            verts[it] += _velocity[it];

            GLfloat resistance = 0.05f;

            if (it % 3 == 1)
            {
                _velocity[it] -= 0.02f;     // Gravity
                resistance = 0.01f;
            }

            // Air resistance
            if (_velocity[it] < 0.0f) {
                _velocity[it] += resistance;
                if (_velocity[it] > 0.0f) { _velocity[it] = 0.0f; }
            } else {
                _velocity[it] -= resistance;
                if (_velocity[it] < 0.0f) { _velocity[it] = 0.0f; }
            }

            // Move
            verts[it] += _velocity[it];

            // Collision with horizontal plane
            if (it % 3 == 1 && verts[it] < -1.0f) {
                verts[it] = -1.0f;
                _velocity[it] *= -1;
            }

            it++;
        }

    }
    ((VisualObject&)evt.getTarget()).resource().load();
}
