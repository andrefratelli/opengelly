#ifndef FOLLOWARROWS_H
#define	FOLLOWARROWS_H

#include <evts/KeyboardAnimationListener.h>

namespace eng
{
    class FollowArrows :
        public KeyboardAnimationListener
    {
    public:
        FollowArrows(Transformable& target);
        
        void notify(KeyboardAnimationEvent _evt);
    private:
    };
}

#endif	/* FOLLOWARROWS_H */

