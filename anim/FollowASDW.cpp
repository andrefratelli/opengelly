#include <anim/FollowASDW.h>

using namespace eng;

FollowASDW::FollowASDW(Transformable& target) :
    KeyboardAnimationListener(target)
{
    m3dLoadVector3(_vIncrement, 1.0f, 0.0f, 1.0f);
}

FollowASDW::FollowASDW(Transformable& target, GLfloat xInc, GLfloat yInc, GLfloat zInc) :
    KeyboardAnimationListener(target)
{
    m3dLoadVector3(_vIncrement, xInc, yInc, zInc);
}

FollowASDW::FollowASDW(Transformable& target, M3DVector3f vInc) :
    KeyboardAnimationListener(target)
{
    m3dCopyVector3(_vIncrement, vInc);
}

void FollowASDW::notify(KeyboardAnimationEvent _evt)
{
    KeyboardEvent evt = _evt.getTrigger();

    if (evt.down('w') || evt.down('W')) { _evt.getTarget().local().translate(0.0f, 0.0f, _vIncrement[2]); }
    if (evt.down('s') || evt.down('S')) { _evt.getTarget().local().translate(0.0f, 0.0f, -_vIncrement[2]); }
    if (evt.down('a') || evt.down('A')) { _evt.getTarget().local().translate(-_vIncrement[0], 0.0f, 0.0f); }
    if (evt.down('d') || evt.down('D')) { _evt.getTarget().local().translate(_vIncrement[0], 0.0f, 0.0f); }
}
