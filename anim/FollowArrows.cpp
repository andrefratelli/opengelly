#include <anim/FollowArrows.h>

using namespace eng;

FollowArrows::FollowArrows(Transformable& target) :
    KeyboardAnimationListener(target)
{}

void FollowArrows::notify(KeyboardAnimationEvent _evt)
{
    GLfloat angular = m3dDegToRad(1.0f);
    GLfloat linear = 0.1f;
    KeyboardEvent evt = _evt.getTrigger();

    if (evt.sdown(GLUT_KEY_UP)) { _evt.getTarget().local().translate(0.0f, 0.0f, linear); }
    if (evt.sdown(GLUT_KEY_DOWN)) { _evt.getTarget().local().translate(0.0f, 0.0f, -linear); }
    if (evt.sdown(GLUT_KEY_LEFT)) { _evt.getTarget().local().rotate(angular, 0.0f, -linear, 0.0f); }
    if (evt.sdown(GLUT_KEY_RIGHT)) { _evt.getTarget().local().rotate(angular, 0.0f, linear, 0.0f); }
}
