#ifndef FOLLOWMOUSE_H
#define	FOLLOWMOUSE_H

#include <evts/MouseAnimationListener.h>

namespace eng
{
    class FollowMouse :
        public MouseAnimationListener
    {
    public:
        FollowMouse(Transformable& target);
        
        void notify(MouseAnimationEvent _evt);
    };
}

#endif	/* FOLLOWMOUSE_H */

