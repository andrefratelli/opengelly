#ifndef FOLLOWASDW_H
#define	FOLLOWASDW_H

#include <evts/KeyboardAnimationListener.h>

namespace eng
{
    class FollowASDW :
        public KeyboardAnimationListener
    {
    public:
        FollowASDW(Transformable& target);
        FollowASDW(Transformable& target, GLfloat xInc, GLfloat yInc, GLfloat zInc);
        FollowASDW(Transformable& target, M3DVector3f vInc);
        
        void notify(KeyboardAnimationEvent _evt);
        
    private:
        M3DVector3f _vIncrement;
    };
}

#endif	/* FOLLOWASDW_H */

