#include <anim/FollowMouse.h>

using namespace eng;

FollowMouse::FollowMouse(Transformable& target) :
    MouseAnimationListener(target)
{}

void FollowMouse::notify(MouseAnimationEvent _evt)
{
    MouseEvent evt = _evt.getTrigger();
    
    _evt.getTarget().local().rotate(0.1f, evt.x(), evt.y(), 0.0f);
    
    glutWarpPointer(0,0);
}
