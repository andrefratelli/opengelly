#include <anim/Fx.h>
#include <gfx/ParticleSystem.h>

using namespace eng;
using namespace eng::Fx;

void eng::Fx::explode(ParticleSystem& sys, GLfloat dispersion)
{
    std::vector<SafePointer<VisualObject> >& particles = sys.getParticles();
    std::vector<SafePointer<VisualObject> >::size_type it;
    Object::normals_vpt& normals = sys.resource().getObject()->normals();
    M3DVector3f vNorm;
            
    for (it=0 ; it<particles.size() ; it++)
    {
        m3dLoadVector3(vNorm, normals[it]->x(), normals[it]->y(), normals[it]->z());
        
        vNorm[0] *= dispersion;
        vNorm[1] *= dispersion;
        vNorm[2] *= dispersion;
        
        particles[it]->world().setSpeed(vNorm[0], vNorm[1], vNorm[2]);
        particles[it]->local().setSpeed(0.0f, 0.0f, 0.0f);
    }
}

void eng::Fx::shoot(VisualObject& followee, VisualObject& follower)
{
    M3DVector3f vTranslation;
    M3DVector4f vRotation;
    
//    vTranslation[0] = 0.75f;
//    vTranslation[1] = -1.0f;
//    vTranslation[2] = 10.0f;

    vTranslation[0] = 0.0f;
    vTranslation[1] = 0.0f;
    vTranslation[2] = 3.0f;
    

    vRotation[0] = -0.05f;
    vRotation[1] = 0.0f;
    vRotation[2] = 1.0f;
    vRotation[3] = 0.0f;

    m3dCopyMatrix44(follower.getMatrix(), followee.Transformable::getMatrix());

    follower.world().rotate(vRotation[0], vRotation[1], vRotation[2], vRotation[3]);
    follower.local().translate(vTranslation);

    follower.local().setSpeed(0.0f, 0.0f, 5.0f);
    follower.local().setAcceleration(0.0f, 0.0f, 0.0f);
}