#ifndef HASH_FUN_H
#define	HASH_FUN_H

#include <hash_fun.h>
#include <string>

#if defined(_WIN32)
#error hash_map is not yet implemented for windows systems.
#elif defined(__APPLE__)
#error hash_map is not yet implemented for apple systems.
#else

#define _ENG_HASH_NAMESPACE __gnu_cxx
    
#define _ENG_END_HASH _GLIBCXX_END_NAMESPACE
    
_GLIBCXX_BEGIN_NAMESPACE(__gnu_cxx)
    template <> struct hash<std::string>
    {
        size_t operator()(const std::string& __s) const
        {
            return __stl_hash_string(__s.c_str());
        }
    };
_GLIBCXX_END_NAMESPACE
    
#endif
#endif	/* HASH_FUN_H */
