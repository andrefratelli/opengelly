#ifndef SAFEPOINTER_H
#define	SAFEPOINTER_H

namespace eng
{
    template <class T> class SafePointer
    {
    public:
        typedef T value_type;
        typedef value_type* pointer_type;
        typedef unsigned int size_type;
        typedef size_type* size_pointer_type;
        
        SafePointer(value_type* ptr);
        SafePointer(const SafePointer& copy);
        ~SafePointer();
        
        value_type& operator *();
        value_type* operator ->();
        
    private:
        pointer_type _ptr;
        size_pointer_type _count;
    };
    
    template <class T> SafePointer<T>::SafePointer(SafePointer<T>::pointer_type ptr) :
        _ptr(ptr),
        _count(new size_type(1))
    {}
    
    template <class T> SafePointer<T>::SafePointer(const SafePointer& copy) :
        _ptr(copy._ptr),
        _count(copy._count)
    {
        (*_count)++;
    }
    
    template <class T> SafePointer<T>::~SafePointer()
    {
        (*_count)--;
        
        if (*_count == 0) {
            delete _ptr;
            delete _count;
        }
    }
    
    template <class T> typename SafePointer<T>::value_type& SafePointer<T>::operator *()
    {
        return *_ptr;
    }
    
    template <class T> typename SafePointer<T>::value_type* SafePointer<T>::operator ->()
    {
        return _ptr;
    }
}

#endif	/* SAFEPOINTER_H */

