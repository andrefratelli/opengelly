#ifndef HASH_MAP_H
#define	HASH_MAP_H

#if defined(_WIN32)
#error hash_map is not yet implemented for windows systems.
#elif defined(__APPLE__)
#error hash_map is not yet implemented for apple systems.
#else

#include <hash_map>
#include <hash_fun.h>
#include <utils/hash_fun.h>

namespace eng
{
    template <class K, class T> class hash_map
    {
    public:
        typedef K key_type;
        typedef T value_type;
        typedef size_t size_type;
        
        size_type size() const;
        bool empty() const;
        bool contains(key_type key) const;
        void insert(key_type key, value_type value);
        void remove(key_type key);

        value_type& operator[] (const key_type& key);
        
    private:
        // Always make this private. There could be conflicts with implementations
        // for other OSes
        typedef __gnu_cxx::hash_map<key_type, value_type> bundle_type;
        
        bundle_type _bundle;
    };
    
    template <class K, class T> typename hash_map<K,T>::size_type hash_map<K,T>::size() const
    {
        return _bundle.size();
    }

    template <class K, class T> bool hash_map<K,T>::empty() const
    {
        return _bundle.empty();
    }

    template <class K, class T> bool hash_map<K,T>::contains(key_type key) const
    {
        return _bundle.find(key) != _bundle.end();
    }

    template <class K, class T> void hash_map<K,T>::insert(key_type key, value_type value)
    {
        _bundle[key] = value;
    }

    template <class K, class T> void hash_map<K,T>::remove(key_type key)
    {
        _bundle.erase(key);
    }

    template <class K, class T> typename hash_map<K,T>::value_type& hash_map<K,T>::operator[] (const key_type& key)
    {
        return _bundle[key];
    }
}
#endif
#endif	/* HASH_MAP_H */

