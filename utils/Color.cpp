#include <utils/Color.h>
#include <assert.h>

using namespace eng;

#define N(rgba) ((rgba) < 0.0f ? 0.0f : ((rgba) > 1.0f ? 1.0f : rgba))

Color::Color(const color_t rgba) :
    _rgba({N(rgba[0]), N(rgba[1]), N(rgba[2]), N(rgba[3])})
{}

Color::Color(GLfloat r, GLfloat g, GLfloat b) :
    _rgba({N(r),N(g),N(b),1.0f})
{}

Color::Color(GLfloat r, GLfloat g, GLfloat b, GLfloat a) :
    _rgba({N(r),N(g),N(b),N(a)})
{}
    
Color::Color(const Color& copy) :
    _rgba({copy._rgba[0], copy._rgba[1], copy._rgba[2], copy._rgba[3]})
{}

color_t& Color::getRGBA() { return _rgba; }
void Color::getRGBA(color_t rgba) { m3dCopyVector4(rgba, _rgba); }
void Color::setRGBA(const color_t rgba) { m3dCopyVector4(_rgba, rgba); }

GLfloat Color::r() const { return _rgba[0]; }
GLfloat Color::g() const { return _rgba[1]; }
GLfloat Color::b() const { return _rgba[2]; }
GLfloat Color::alpha() const { return _rgba[3]; }

void Color::r(GLfloat r) { _rgba[0] = N(r); }
void Color::g(GLfloat g) { _rgba[1] = N(g); }
void Color::b(GLfloat b) { _rgba[2] = N(b); }
void Color::alpha(GLfloat a) { _rgba[3] = N(a); }

Color Color::operator - () const
{
    return Color(
        1.0f - _rgba[0],
        1.0f - _rgba[1],
        1.0f - _rgba[2],
        _rgba[3]                    // Keep opacity
    );
}

#undef N_RGBA
