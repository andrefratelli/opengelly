#ifndef COLOR_H
#define COLOR_H

#include <shared.h>

/*
 * HTML colors from http://www.w3schools.com/html/html_colornames.asp
 */
namespace eng
{
    typedef GLfloat color_t[4];
    
    class Color
    {
    public:
        Color(const color_t rgba);
        Color(GLfloat r, GLfloat g, GLfloat b);
        Color(GLfloat r, GLfloat g, GLfloat b, GLfloat a);
        Color(const Color& copy);
        
        color_t& getRGBA();
        void getRGBA(color_t rgba);
        
        void setRGBA(const color_t rgba);
        
        GLfloat r() const;
        GLfloat g() const;
        GLfloat b() const;
        GLfloat alpha() const;
        
        void r(GLfloat r);
        void g(GLfloat g);
        void b(GLfloat b);
        void alpha(GLfloat a);
        
        Color operator - () const;
        
    private:
        color_t _rgba;
    };
    
    namespace colors
    {
#define N(n) ((n)/255.0f)
        const Color AliceBlue(N(240.0f),N(248.0f),N(255.0f));
        const Color AntiqueWhite(N(250.0f),N(235.0f),N(215.0f));
        const Color Aqua(N(0.0f),N(255.0f),N(255.0f));
        const Color Aquamarine(N(127.0f),N(255.0f),N(212.0f));
        const Color Azure(N(240.0f),N(255.0f),N(255.0f));
        const Color Beige(N(245.0f),N(245.0f),N(220.0f));
        const Color Bisque(N(255.0f),N(228.0f),N(196.0f));
        const Color Black(N(0.0f),N(0.0f),N(0.0f));
        const Color BlanchedAlmond(N(255.0f),N(235.0f),N(205.0f));
        const Color Blue(N(0.0f),N(0.0f),N(255.0f));
        const Color BlueViolet(N(138.0f),N(43.0f),N(226.0f));
        const Color Brown(N(165.0f),N(42.0f),N(42.0f));
        const Color BurlyWood(N(222.0f),N(184.0f),N(135.0f));
        const Color CadetBlue(N(95.0f),N(158.0f),N(160.0f));
        const Color Chartreuse(N(127.0f),N(255.0f),N(0.0f));
        const Color Chocolate(N(210.0f),N(105.0f),N(30.0f));
        const Color Coral(N(255.0f),N(127.0f),N(80.0f));
        const Color CornflowerBlue(N(100.0f),N(149.0f),N(237.0f));
        const Color Cornsilk(N(255.0f),N(248.0f),N(220.0f));
        const Color Crimson(N(220.0f),N(20.0f),N(60.0f));
        const Color Cyan(N(0.0f),N(255.0f),N(255.0f));
        const Color DarkBlue(N(0.0f),N(0.0f),N(139.0f));
        const Color DarkCyan(N(0.0f),N(139.0f),N(139.0f));
        const Color DarkGoldenRod(N(184.0f),N(134.0f),N(11.0f));
        const Color DarkGray(N(169.0f),N(169.0f),N(169.0f));
        const Color DarkGrey(N(169.0f),N(169.0f),N(169.0f));
        const Color DarkGreen(N(0.0f),N(100.0f),N(0.0f));
        const Color DarkKhaki(N(189.0f),N(183.0f),N(107.0f));
        const Color DarkMagenta(N(139.0f),N(0.0f),N(139.0f));
        const Color DarkOliveGreen(N(85.0f),N(107.0f),N(47.0f));
        const Color Darkorange(N(255.0f),N(140.0f),N(0.0f));
        const Color DarkOrchid(N(153.0f),N(50.0f),N(204.0f));
        const Color DarkRed(N(139.0f),N(0.0f),N(0.0f));
        const Color DarkSalmon(N(233.0f),N(150.0f),N(122.0f));
        const Color DarkSeaGreen(N(143.0f),N(188.0f),N(143.0f));
        const Color DarkSlateBlue(N(72.0f),N(61.0f),N(139.0f));
        const Color DarkSlateGray(N(47.0f),N(79.0f),N(79.0f));
        const Color DarkSlateGrey(N(47.0f),N(79.0f),N(79.0f));
        const Color DarkTurquoise(N(0.0f),N(206.0f),N(209.0f));
        const Color DarkViolet(N(148.0f),N(0.0f),N(211.0f));
        const Color DeepPink(N(255.0f),N(20.0f),N(147.0f));
        const Color DeepSkyBlue(N(0.0f),N(191.0f),N(255.0f));
        const Color DimGray(N(105.0f),N(105.0f),N(105.0f));
        const Color DimGrey(N(105.0f),N(105.0f),N(105.0f));
        const Color DodgerBlue(N(30.0f),N(144.0f),N(255.0f));
        const Color FireBrick(N(178.0f),N(34.0f),N(34.0f));
        const Color FloralWhite(N(255.0f),N(250.0f),N(240.0f));
        const Color ForestGreen(N(34.0f),N(139.0f),N(34.0f));
        const Color Fuchsia(N(255.0f),N(0.0f),N(255.0f));
        const Color Gainsboro(N(220.0f),N(220.0f),N(220.0f));
        const Color GhostWhite(N(248.0f),N(248.0f),N(255.0f));
        const Color Gold(N(255.0f),N(215.0f),N(0.0f));
        const Color GoldenRod(N(218.0f),N(165.0f),N(32.0f));
        const Color Gray(N(128.0f),N(128.0f),N(128.0f));
        const Color Grey(N(128.0f),N(128.0f),N(128.0f));
        const Color Green(N(0.0f),N(128.0f),N(0.0f));
        const Color GreenYellow(N(173.0f),N(255.0f),N(47.0f));
        const Color HoneyDew(N(240.0f),N(255.0f),N(240.0f));
        const Color HotPink(N(255.0f),N(105.0f),N(180.0f));
        const Color IndianRed(N(205.0f),N(92.0f),N(92.0f));
        const Color Indigo(N(75.0f),N(0.0f),N(130.0f));
        const Color Ivory(N(255.0f),N(255.0f),N(240.0f));
        const Color Khaki(N(240.0f),N(230.0f),N(140.0f));
        const Color Lavender(N(230.0f),N(230.0f),N(250.0f));
        const Color LavenderBlush(N(255.0f),N(240.0f),N(245.0f));
        const Color LawnGreen(N(124.0f),N(252.0f),N(0.0f));
        const Color LemonChiffon(N(255.0f),N(250.0f),N(205.0f));
        const Color LightBlue(N(173.0f),N(216.0f),N(230.0f));
        const Color LightCoral(N(240.0f),N(128.0f),N(128.0f));
        const Color LightCyan(N(224.0f),N(255.0f),N(255.0f));
        const Color LightGoldenRodYellow(N(250.0f),N(250.0f),N(210.0f));
        const Color LightGray(N(211.0f),N(211.0f),N(211.0f));
        const Color LightGrey(N(211.0f),N(211.0f),N(211.0f));
        const Color LightGreen(N(144.0f),N(238.0f),N(144.0f));
        const Color LightPink(N(255.0f),N(182.0f),N(193.0f));
        const Color LightSalmon(N(255.0f),N(160.0f),N(122.0f));
        const Color LightSeaGreen(N(32.0f),N(178.0f),N(170.0f));
        const Color LightSkyBlue(N(135.0f),N(206.0f),N(250.0f));
        const Color LightSlateGray(N(119.0f),N(136.0f),N(153.0f));
        const Color LightSlateGrey(N(119.0f),N(136.0f),N(153.0f));
        const Color LightSteelBlue(N(176.0f),N(196.0f),N(222.0f));
        const Color LightYellow(N(255.0f),N(255.0f),N(224.0f));
        const Color Lime(N(0.0f),N(255.0f),N(0.0f));
        const Color LimeGreen(N(50.0f),N(205.0f),N(50.0f));
        const Color Linen(N(250.0f),N(240.0f),N(230.0f));
        const Color Magenta(N(255.0f),N(0.0f),N(255.0f));
        const Color Maroon(N(128.0f),N(0.0f),N(0.0f));
        const Color MediumAquaMarine(N(102.0f),N(205.0f),N(170.0f));
        const Color MediumBlue(N(0.0f),N(0.0f),N(205.0f));
        const Color MediumOrchid(N(186.0f),N(85.0f),N(211.0f));
        const Color MediumPurple(N(147.0f),N(112.0f),N(219.0f));
        const Color MediumSeaGreen(N(60.0f),N(179.0f),N(113.0f));
        const Color MediumSlateBlue(N(123.0f),N(104.0f),N(238.0f));
        const Color MediumSpringGreen(N(0.0f),N(250.0f),N(154.0f));
        const Color MediumTurquoise(N(72.0f),N(209.0f),N(204.0f));
        const Color MediumVioletRed(N(199.0f),N(21.0f),N(133.0f));
        const Color MidnightBlue(N(25.0f),N(25.0f),N(112.0f));
        const Color MintCream(N(245.0f),N(255.0f),N(250.0f));
        const Color MistyRose(N(255.0f),N(228.0f),N(225.0f));
        const Color Moccasin(N(255.0f),N(228.0f),N(181.0f));
        const Color NavajoWhite(N(255.0f),N(222.0f),N(173.0f));
        const Color Navy(N(0.0f),N(0.0f),N(128.0f));
        const Color OldLace(N(253.0f),N(245.0f),N(230.0f));
        const Color Olive(N(128.0f),N(128.0f),N(0.0f));
        const Color OliveDrab(N(107.0f),N(142.0f),N(35.0f));
        const Color Orange(N(255.0f),N(165.0f),N(0.0f));
        const Color OrangeRed(N(255.0f),N(69.0f),N(0.0f));
        const Color Orchid(N(218.0f),N(112.0f),N(214.0f));
        const Color PaleGoldenRod(N(238.0f),N(232.0f),N(170.0f));
        const Color PaleGreen(N(152.0f),N(251.0f),N(152.0f));
        const Color PaleTurquoise(N(175.0f),N(238.0f),N(238.0f));
        const Color PaleVioletRed(N(219.0f),N(112.0f),N(147.0f));
        const Color PapayaWhip(N(255.0f),N(239.0f),N(213.0f));
        const Color PeachPuff(N(255.0f),N(218.0f),N(185.0f));
        const Color Peru(N(205.0f),N(133.0f),N(63.0f));
        const Color Pink(N(255.0f),N(192.0f),N(203.0f));
        const Color Plum(N(221.0f),N(160.0f),N(221.0f));
        const Color PowderBlue(N(176.0f),N(224.0f),N(230.0f));
        const Color Purple(N(128.0f),N(0.0f),N(128.0f));
        const Color Red(N(255.0f),N(0.0f),N(0.0f));
        const Color RosyBrown(N(188.0f),N(143.0f),N(143.0f));
        const Color RoyalBlue(N(65.0f),N(105.0f),N(225.0f));
        const Color SaddleBrown(N(139.0f),N(69.0f),N(19.0f));
        const Color Salmon(N(250.0f),N(128.0f),N(114.0f));
        const Color SandyBrown(N(244.0f),N(164.0f),N(96.0f));
        const Color SeaGreen(N(46.0f),N(139.0f),N(87.0f));
        const Color SeaShell(N(255.0f),N(245.0f),N(238.0f));
        const Color Sienna(N(160.0f),N(82.0f),N(45.0f));
        const Color Silver(N(192.0f),N(192.0f),N(192.0f));
        const Color SkyBlue(N(135.0f),N(206.0f),N(235.0f));
        const Color SlateBlue(N(106.0f),N(90.0f),N(205.0f));
        const Color SlateGray(N(112.0f),N(128.0f),N(144.0f));
        const Color SlateGrey(N(112.0f),N(128.0f),N(144.0f));
        const Color Snow(N(255.0f),N(250.0f),N(250.0f));
        const Color SpringGreen(N(0.0f),N(255.0f),N(127.0f));
        const Color SteelBlue(N(70.0f),N(130.0f),N(180.0f));
        const Color Tan(N(210.0f),N(180.0f),N(140.0f));
        const Color Teal(N(0.0f),N(128.0f),N(128.0f));
        const Color Thistle(N(216.0f),N(191.0f),N(216.0f));
        const Color Tomato(N(255.0f),N(99.0f),N(71.0f));
        const Color Turquoise(N(64.0f),N(224.0f),N(208.0f));
        const Color Violet(N(238.0f),N(130.0f),N(238.0f));
        const Color Wheat(N(245.0f),N(222.0f),N(179.0f));
        const Color White(N(255.0f),N(255.0f),N(255.0f));
        const Color WhiteSmoke(N(245.0f),N(245.0f),N(245.0f));
        const Color Yellow(N(255.0f),N(255.0f),N(0.0f));
        const Color YellowGreen(N(154.0f),N(205.0f),N(50.0f));
#undef N
    }
}

#endif /* COLOR_H */
