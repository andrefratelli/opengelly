#include <mng/WindowManager.h>
#include <shared.h>
#include <evts/Handler.h>
#include <evts/IdleListener.h>
#include <evts/KeyboardListener.h>
#include <evts/ReshapeListener.h>
#include <evts/DisplayListener.h>
#include <evts/MouseListener.h>
#include <evts/HandlerRegistration.h>
#include <string>
#include <assert.h>
#include <sstream>
#include <gfx/Camera.h>

using namespace eng;

// Callbacks
void _auxiliarGlutIdleCallback()
{
    WindowManager::instance()->
        Handler<IdleListener, IdleEvent>::
            fire(Handler<IdleListener, IdleEvent>::event_type());
}

void _auxiliarGlutKeyboardCallback(KeyboardEvent::key_type key, int x, int y)
{
    WindowManager::instance()->
        Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::
            fire(
                Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::event_type(key, false, KEY_PRESS)
    );
}

void _auxiliarGlutKeyboardUpCallback(KeyboardEvent::key_type key, int x, int y)
{
    WindowManager::instance()->
        Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::
            fire(
                Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::event_type(key, false, KEY_RELEASE)
    );
}

void _auxiliarGlutSpecialCallback(int key, int x, int y)
{
    WindowManager::instance()->
        Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::
            fire(
                Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::event_type(key, true, KEY_PRESS)
    );
}

void _auxiliarGlutSpecialUpCallback(int key, int x, int y)
{
    WindowManager::instance()->
        Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::
            fire(
                Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::event_type(key, true, KEY_RELEASE)
    );
}

void _auxiliarGlutReshapeCallback(int w, int h)
{
    WindowManager::instance()->
        Handler<ReshapeListener, ReshapeEvent>::
            fire(Handler<ReshapeListener, ReshapeEvent>::event_type(w,h));
}

void _auxiliarGlutMouseCallback(int button, int state, int x, int y)
{
    WindowManager::instance()->
        Handler<MouseListener, MouseEvent>::
            fire(Handler<MouseListener, MouseEvent>::event_type(button, state, x, y));
}

void _auxiliarGlutPassiveMotionCallback(int x, int y)
{
    WindowManager::instance()->
        Handler<MouseListener, MouseEvent>::
            fire(Handler<MouseListener, MouseEvent>::event_type(-1, -1, x, y));
}

void _auxiliarGlutMotionCallback(int x, int y)
{
    WindowManager::instance()->
        Handler<MouseListener, MouseEvent>::
            fire(Handler<MouseListener, MouseEvent>::event_type(-1, -1, x, y));
}

void _auxiliarGlutDisplayCallback()
{
    WindowManager* wm = WindowManager::instance();
    bool clear = !!wm->getClear();
    
    // Returning zero clears nothing
    if (clear) { glClear(wm->getClear()); }
    
    wm->Handler<DisplayListener, DisplayEvent>::fire(Handler<DisplayListener, DisplayEvent>::event_type());

    if (!!glutGet(GLUT_WINDOW_DOUBLEBUFFER)) { glutSwapBuffers(); }
}

WindowManager* WindowManager::_instance = NULL;

bool WindowManager::_keyState[KEYBOARD_KEY_COUNT] = {false};
bool WindowManager::_keySpecial[KEYBOARD_KEY_COUNT] = {false};

unsigned int WindowManager::_residualKey = 0;
unsigned int WindowManager::_residualSpecial = 0;

void WindowManager::initialize()
{
    // Multiple managers?
    assert(_instance == NULL);
    
    _instance = this;

    // No need to keep the registration. Because the handler and the listener
    // are one and the same, both die at the same time. Never invalidates the
    // collection
    Handler<ReshapeListener, ReshapeEvent>::addEventListener(this);
    Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::addEventListener(this);
}

#define __ENG_WINDOW_MANAGER_DEFAULT_WINDOW_NAME "OpenGelly Window"
#define __ENG_WINDOW_MANAGER_DEFAULT_CLEAR_COLOR colors::Black
#define __ENG_WINDOW_MANAGER_DEFAULT_MODE GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_STENCIL
#define __ENG_WINDOW_MANAGER_DEFAULT_CLEAR GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT

WindowManager::WindowManager() :
        Handler<IdleListener, IdleEvent>::Handler(),
        Handler<KeyboardListener, KeyboardEvent>::Handler(),
        Handler<ReshapeListener, ReshapeEvent>::Handler(),
        Handler<DisplayListener, DisplayEvent>::Handler(),
        Handler<MouseListener, MouseEvent>::Handler(),
        Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::Handler(),
        ReshapeListener(),
        _mode(__ENG_WINDOW_MANAGER_DEFAULT_MODE),
        _clear(__ENG_WINDOW_MANAGER_DEFAULT_CLEAR),
        _name(__ENG_WINDOW_MANAGER_DEFAULT_WINDOW_NAME),
        _clearColor(__ENG_WINDOW_MANAGER_DEFAULT_CLEAR_COLOR),      // Default GL clear color
        _ready(false),
        _visible(true)
{
    initialize();
}

WindowManager::WindowManager(const std::string name) :
        Handler<IdleListener, IdleEvent>::Handler(),
        Handler<KeyboardListener, KeyboardEvent>::Handler(),
        Handler<ReshapeListener, ReshapeEvent>::Handler(),
        Handler<DisplayListener, DisplayEvent>::Handler(),
        Handler<MouseListener, MouseEvent>::Handler(),
        Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::Handler(),
        ReshapeListener(),
        _mode(__ENG_WINDOW_MANAGER_DEFAULT_MODE),
        _clear(__ENG_WINDOW_MANAGER_DEFAULT_CLEAR),
        _name(name),
        _clearColor(__ENG_WINDOW_MANAGER_DEFAULT_CLEAR_COLOR),      // Default GL clear color
        _ready(false),
        _visible(true)
{
    initialize();
}

WindowManager::WindowManager(const std::string name, const Color clearColor) :
        Handler<IdleListener, IdleEvent>::Handler(),
        Handler<KeyboardListener, KeyboardEvent>::Handler(),
        Handler<ReshapeListener, ReshapeEvent>::Handler(),
        Handler<DisplayListener, DisplayEvent>::Handler(),
        Handler<MouseListener, MouseEvent>::Handler(),
        Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::Handler(),
        ReshapeListener(),
        _mode(__ENG_WINDOW_MANAGER_DEFAULT_MODE),
        _clear(__ENG_WINDOW_MANAGER_DEFAULT_CLEAR),
        _name(name),
        _clearColor(clearColor),      // Default GL clear color
        _ready(false),
        _visible(true)
{
    initialize();
}

WindowManager::WindowManager(const Color clearColor) :
        Handler<IdleListener, IdleEvent>::Handler(),
        Handler<KeyboardListener, KeyboardEvent>::Handler(),
        Handler<ReshapeListener, ReshapeEvent>::Handler(),
        Handler<DisplayListener, DisplayEvent>::Handler(),
        Handler<MouseListener, MouseEvent>::Handler(),
        Handler<KeyboardNotificationListener, KeyboardNotificationEvent>::Handler(),
        ReshapeListener(),
        _mode(__ENG_WINDOW_MANAGER_DEFAULT_MODE),
        _clear(__ENG_WINDOW_MANAGER_DEFAULT_CLEAR),
        _name(__ENG_WINDOW_MANAGER_DEFAULT_WINDOW_NAME),
        _clearColor(clearColor),
        _ready(false),
        _visible(true)
{
    initialize();
}

void WindowManager::fireKeyboardEvent()
{
    fire(KeyboardEvent(_keyState, _keySpecial));
}

void WindowManager::notify(ReshapeEvent evt)
{
    int height = evt.height() == 0 ? 1 : evt.height();
    
    glViewport(0, 0, evt.width(), height);
    
    //gluPerspective(35.0f, evt.width()/evt.height(), 1.0f, 100.0f);
    Camera::getActive()->getFrustum().setPerspective(35.0f, evt.width()/evt.height(), 1.0f, 10000.0f);
    //Camera::getActive()->getFrustum().setOrthographic(-8.0f, 8.0f, -8.0f, 8.0f, -8.0f, 8.0f);
    
    // On the first run, tells everything else we are ready to draw
    _ready = true;
}

void WindowManager::notify(KeyboardNotificationEvent evt)
{
    bool* _map = (evt.special() ? _keySpecial : _keyState);
    unsigned int* _residual = (evt.special() ? &_residualSpecial : &_residualKey);

    if (evt.event() == KEY_PRESS)
    {
        if (!_map[evt.key()])
        {
            _map[evt.key()] = true;
            (*_residual)++;
        }
    }
    else
    {
        if (_map[evt.key()])
        {
            _map[evt.key()] = false;
            (*_residual)--;
        }
    }
}

WindowManager* WindowManager::instance()
{
    if (_instance == NULL)
    {
        _instance = new WindowManager();
    }
    return _instance;
}

void WindowManager::fire(KeyboardEvent evt)
{
    // We have to fire a ghost event to notify key releases
    static bool ghost = false;
    
    if (_residualKey > 0 || _residualSpecial > 0 || ghost)
        WindowManager::instance()->
            Handler<KeyboardListener, KeyboardEvent>::
                fire(Handler<KeyboardListener, KeyboardEvent>::event_type(_keyState, _keySpecial));
    
    ghost = _residualKey > 0 || _residualSpecial > 0;
}

unsigned int WindowManager::getMode() const { return _mode; }
unsigned int WindowManager::getClear() const { return _clear; }
// Return zero and clearing will not be done

std::string WindowManager::getName() const { return _name; }
Color WindowManager::getClearColor() const { return _clearColor; }
bool WindowManager::getVisible() const { return _visible; }
int WindowManager::getWidth() const { return glutGet(GLUT_WINDOW_WIDTH); }
int WindowManager::getHeight() const { return glutGet(GLUT_WINDOW_HEIGHT); }
int WindowManager::getTop() const { return glutGet(GLUT_WINDOW_Y); }
int WindowManager::getLeft() const { return glutGet(GLUT_WINDOW_X); }
bool WindowManager::isReady() const { return _ready; }
void WindowManager::setMode(unsigned int mode) { _mode = mode; }
void WindowManager::setClear(unsigned int clear) { _clear = clear; }
void WindowManager::setName(const std::string name) { _name = name; }

void WindowManager::setClearColor(const Color cc)
{
    _clearColor = cc;
    
    glClearColor(_clearColor.r(), _clearColor.g(), _clearColor.b(), _clearColor.alpha());
}

void WindowManager::setVisible(bool visible)
{
    if (_visible != visible)
    {
        _visible = visible;
        
        if (_visible) { glutShowWindow(); }
        else { glutHideWindow(); }
    }
}

void WindowManager::setSize(int width, int height)
{
    glutReshapeWindow(width, height);
}

bool WindowManager::create(WindowType wnd, int width, int height)
{
    destroy();
    
    if (wnd != OGWT_NONE)
    {
        glutInitDisplayMode(getMode());
        
        switch (wnd)
        {
            case OGWT_WINDOWED:
                glutInitWindowSize(width, height);
                
            case OGWT_FULLSCREEN:
                if (!!glutCreateWindow(getName().c_str()))
                {
                    if (wnd == OGWT_FULLSCREEN) { glutFullScreen(); }
                }
                else { return false; }
            break;
            
            case OGWT_GAMEMODE:
                std::stringstream ss; ss << width << 'x' << height;
                
                glutGameModeString(ss.str().c_str());
                
                if (!!glutGameModeGet(GLUT_GAME_MODE_POSSIBLE)) 
                {
                    glutEnterGameMode();
                    
                    if (!glutGameModeGet(GLUT_GAME_MODE_DISPLAY_CHANGED)) { return false; }
                }
            break;
        }
        
        glutReshapeFunc(_auxiliarGlutReshapeCallback);
        glutDisplayFunc(_auxiliarGlutDisplayCallback);
        glutKeyboardFunc(_auxiliarGlutKeyboardCallback);
        glutKeyboardUpFunc(_auxiliarGlutKeyboardUpCallback);
        glutSpecialFunc(_auxiliarGlutSpecialCallback);
        glutSpecialUpFunc(_auxiliarGlutSpecialUpCallback);
        glutMouseFunc(_auxiliarGlutMouseCallback);
        glutPassiveMotionFunc(_auxiliarGlutPassiveMotionCallback);
        glutMotionFunc(_auxiliarGlutMotionCallback);
        glutIdleFunc(_auxiliarGlutIdleCallback);
    }
    else { return false; }      // OGWT_NONE
    
    GLenum err = glewInit();
    
    if (GLEW_OK != err) {
        
        destroy();
        
        fprintf(stderr, "GLEW error: %s\n", glewGetErrorString(err));
        return false;
    }
    return true;
}

void WindowManager::destroy()
{
    // Don't draw right now..
    _ready = false;
    
    if (!!glutGameModeGet(GLUT_GAME_MODE_ACTIVE)) { glutLeaveGameMode(); }
    else
    {
        int wnd = glutGetWindow();
        
        if (!!wnd) { glutDestroyWindow(wnd); }
    }
}
