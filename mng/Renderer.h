#ifndef RENDERER_H
#define	RENDERER_H

#include <mng/ResourceManager.h>
#include <mng/ShaderManager.h>
#include <utils/Color.h>
#include <gfx/ParticleSystem.h>
#include <gfx/VisualObject.h>
#include <evts/LogicListener.h>
#include <evts/DisplayListener.h>
#include <shared.h>
#include <vector>

namespace eng
{
    class Renderer :
        public LogicListener,
        public DisplayListener
    {
    public:
        Renderer();

        SafePointer<VisualObject> load(const ResourceManager::visual_key_type path, size_t octDepth, const ResourceManager::texture_key_type texturePath);
        SafePointer<VisualObject> load(const ResourceManager::visual_key_type path, size_t octDepth, const ResourceManager::texture_key_type texturePath, CUSTOM_SHADER shaderID);
        SafePointer<VisualObject> load(const ResourceManager::visual_key_type path, size_t octDepth, Color c);
        SafePointer<VisualObject> load(const ResourceManager::visual_key_type path, size_t octDepth, Color c, CUSTOM_SHADER shaderID);

        SafePointer<VisualObject> load(const ResourceManager::visual_key_type path, size_t octDepth, GLenum primitive, const ResourceManager::texture_key_type texturePath);
        SafePointer<VisualObject> load(const ResourceManager::visual_key_type path, size_t octDepth, GLenum primitive, const ResourceManager::texture_key_type texturePath, CUSTOM_SHADER shaderID);
        SafePointer<VisualObject> load(const ResourceManager::visual_key_type path, size_t octDepth, GLenum primitive, Color c);
        SafePointer<VisualObject> load(const ResourceManager::visual_key_type path, size_t octDepth, GLenum primitive, Color c, CUSTOM_SHADER shaderID);
        SafePointer<VisualObject> load(
                const ResourceManager::visual_key_type key,
                size_t octDepth,
                GLfloat vertexes[],
                GLuint size,
                GLenum primitive,
                Color c
        );
        SafePointer<VisualObject> load(
                const ResourceManager::visual_key_type key,
                size_t octDepth,
                Buffers::buffer_vt vertexes,
                GLenum primitive,
                Color c
        );
        
        SafePointer<ParticleSystem> loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, const ResourceManager::texture_key_type texturePath);
        SafePointer<ParticleSystem> loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, const ResourceManager::texture_key_type texturePath, CUSTOM_SHADER shaderID);
        SafePointer<ParticleSystem> loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, Color c);
        SafePointer<ParticleSystem> loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, Color c, CUSTOM_SHADER shaderID);

        SafePointer<ParticleSystem> loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, GLenum primitive, const ResourceManager::texture_key_type texturePath);
        SafePointer<ParticleSystem> loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, GLenum primitive, const ResourceManager::texture_key_type texturePath, CUSTOM_SHADER shaderID);
        SafePointer<ParticleSystem> loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, GLenum primitive, Color c);
        SafePointer<ParticleSystem> loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, GLenum primitive, Color c, CUSTOM_SHADER shaderID);

        ShaderManager* shaderManager();
        
        void notify(LogicEvent evt);
        void notify(DisplayEvent evt);
        
        static Renderer* instance();
        
    private:
        std::vector<SafePointer<VisualObject> > _collection;
        
        ShaderManager* _shaderManager;
        
        static Renderer* _instance;
        
        void loadParticles(ParticleSystem& ps);
    };
}

#endif	/* RENDERER_H */
