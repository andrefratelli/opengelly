%{
#include <stdlib.h>
#include <string.h>
#include "obj.tab.h"

%}
%option noyywrap
nl					"\r""\n"?|"\n"
comment				"#".*?{nl}
ws					[[:space:]]+

sign				[+-]|""
dot					[.]
nzdigit				[1-9]
zero				[0]
digit				[0-9]
integer				{sign}{nzdigit}{digit}*|{zero}
numseq				{digit}+
decimal				{sign}{integer}?{dot}{numseq}
scy					{decimal}[eE]{sign}{numseq}
number				{integer}|{decimal}|{scy}

mtlfile				{dot}?({sep}?[a-zA-Z_][a-zA-Z0-9_]*{dot}[mM][tT][lL])+
material			[a-zA-Z_][a-zA-Z0-9_]*

sep					"/"
dsep				{sep}{sep}
%%
{ws}				{}
{mtlfile}			{ yylval.string_t = strdup(yytext); return MTLFILE; }
"mtllib"/{ws}		{ return MTLLIB; }
"usemtl"/{ws}		{ return USEMTL; }
"v"/{ws}			{ return VERT_COORDS; }
"vt"/{ws}			{ return TEXT_COORDS; }
"vn"/{ws}			{ return NORM_COORDS; }
"g"/{ws}			{ return GROUP; }
"f"/{ws}			{ return FACE; }
"p"/{ws}			{ return POINT; }
"l"/{ws}			{ return LINE; }
"off"/{ws}			{ return OFF; }
"o"/{ws}			{ yylval.string_t = strdup(yytext); return OBJ_NAME; }
"s"/{ws}			{ return SMOOTH; }
{material}			{ yylval.string_t = strdup(yytext); return NAME; }
{number}			{ yylval.number_t = atof(yytext); return NUMBER; }
{sep}				{ return SEP; }
{dsep}				{ return DSEP; }
{comment}			{}
.					{}
%%

