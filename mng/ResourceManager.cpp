#include <mng/ResourceManager.h>
#include <mng/WindowManager.h>

using namespace eng;

#include <iostream>
using namespace std;

extern Object* parseObj(const std::string path);
extern Object* parseObj(const std::string path, FILE* err);

ResourceManager* ResourceManager::_instance = NULL;

ResourceManager* ResourceManager::instance()
{
    if (_instance == NULL) {
        _instance = new ResourceManager();
    }
    return _instance;
}

VisualResource ResourceManager::loadVisual(const visual_key_type path, GLenum primitive)
{
    return _loadVisual(path, NULL, primitive);
}

VisualResource ResourceManager::loadVisual(const visual_key_type path, FILE* err, GLenum primitive)
{
    return _loadVisual(path, err, primitive);
}

VisualResource ResourceManager::loadVisual(const visual_key_type key, Buffers::buffer_vt buffer, GLenum primitive)
{
    // TODO :: how do we compute normals?
    
    // Bound box stuff
    M3DVector2f vX;
    M3DVector2f vY;
    M3DVector2f vZ;
    bool initbb = false;
    
    if (!_visuals_map.contains(key))
    {
        SafePointer<Object> obj = new Object();
        Buffers::buffer_vt::iterator it = buffer.begin();
        Buffers::buffer_vt& verts = obj->buffers().vertexes();

        while (it != buffer.end()) {
            
            GLfloat x = *(it + 0);
            GLfloat y = *(it + 1);
            GLfloat z = *(it + 2);
            
            verts.push_back(x);
            verts.push_back(y);
            verts.push_back(z);
            
            if (initbb)
            {
                if (x < vX[0]) { vX[0] = x; }
                if (x > vX[1]) { vX[1] = x; }
                
                if (y < vY[0]) { vY[0] = y; }
                if (y > vY[1]) { vY[1] = y; }
                
                if (z < vZ[0]) { vZ[0] = z; }
                if (z > vZ[1]) { vZ[1] = z; }
            }
            else
            {
                vX[0] = vX[1] = x;
                vY[0] = vY[1] = y;
                vZ[0] = vZ[1] = z;
                initbb = true;
            }

            it += 3;
        }
        
        obj->boundbox() = new BoundBox(vX, vY, vZ);
        
        _visuals_map[key] = new VisualResource(obj, primitive, key);
    }
    
    return *_visuals_map[key];
}

ResourceManager::visual_value_type ResourceManager::_loadVisual(const visual_key_type path, FILE* err, GLenum primitive)
{
    // TODO canonical path
//    if (!_visuals_map.contains(path))
    {
        // Resource not loaded. Load it
        SafePointer<Object> obj = (err == NULL ? parseObj(path) : parseObj(path, err));
        
        // Store in hash table
        _visuals_map[path] = new VisualResource(obj, primitive, path);
    }
    
    // This is where copy actually happens. I prefer to return a copy every time
    // a resource is requested instead of allowing an empty construction of
    // VisualResource.
    //
    // Notice that VisualResource uses nothing other then SafePointers (and a
    // constant GLenum, which doesn't change from instance to instance), which
    // are all OK to copy.
    return *_visuals_map[path];
}

TextureResource ResourceManager::loadTexture(const texture_key_type path) {
    return _loadTexture(path);
}

ResourceManager::texture_value_type ResourceManager::_loadTexture(const texture_key_type path) {
    if (!_textures_map.contains(path))
    {
        _textures_map[path] = new TextureResource(path);
    }
    return *_textures_map[path];
}