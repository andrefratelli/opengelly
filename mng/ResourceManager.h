#ifndef RESOURCEMANAGER_H
#define	RESOURCEMANAGER_H

#include <utils/hash_map.h>
#include <mng/VisualResource.h>
#include <mng/TextureResource.h>
#include <model/Buffers.h>
#include <list>
#include <string>

namespace eng
{
    class ResourceManager
    {
    public:
          
        // Visuals
        typedef VisualResource visual_value_type;
        typedef visual_value_type* visual_pointer_type;
        typedef std::string visual_key_type;
        
        static ResourceManager* instance();
        
        VisualResource loadVisual(const visual_key_type path, GLenum primitive);
        VisualResource loadVisual(const visual_key_type path, FILE* err, GLenum primitive);
        VisualResource loadVisual(const visual_key_type key, Buffers::buffer_vt buffer, GLenum primitive);
        
        // Textures
        typedef TextureResource texture_value_type;
        typedef texture_value_type* texture_pointer_type;
        typedef std::string texture_key_type;
        
        TextureResource loadTexture(const texture_key_type path);
    private:
        
        // HashMaps for Visual e Texture
        typedef hash_map<visual_key_type, visual_pointer_type> visual_map_type;
        typedef hash_map<texture_key_type, texture_pointer_type> texture_map_type;
        
        // Maps
        visual_map_type _visuals_map;
        texture_map_type _textures_map;
        
        // Other stuff
        static ResourceManager* _instance;
        
        visual_value_type _loadVisual(const visual_key_type path, FILE* err, GLenum primitive);
        texture_value_type _loadTexture(const texture_key_type path);
    };
}

#endif	/* RESOURCEMANAGER_H */