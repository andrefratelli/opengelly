#ifndef WINDOWMANAGER_H
#define	WINDOWMANAGER_H

#include <evts/Handler.h>
#include <evts/IdleListener.h>
#include <evts/KeyboardListener.h>
#include <evts/ReshapeListener.h>
#include <evts/DisplayListener.h>
#include <evts/MouseListener.h>
#include <evts/LogicListener.h>
#include <evts/KeyboardNotificationEvent.h>
#include <utils/Color.h>
#include <string>

#define KEYBOARD_KEY_COUNT 256
#define KEYBOARD_SPECIAL_KEY_COUNT 256

namespace eng
{
    enum WindowType
    {
        OGWT_NONE,
        OGWT_WINDOWED,
        OGWT_FULLSCREEN,
        OGWT_GAMEMODE
    };
    
    class WindowManager :
        public Handler<IdleListener, IdleEvent>,
        public Handler<KeyboardListener, KeyboardEvent>,
        public Handler<ReshapeListener, ReshapeEvent>,
        public Handler<DisplayListener, DisplayEvent>,
        public Handler<MouseListener, MouseEvent>,
        public Handler<LogicListener, LogicEvent>,
        public Handler<KeyboardNotificationListener, KeyboardNotificationEvent>,
        protected ReshapeListener,
        protected KeyboardNotificationListener
    {
        friend class GameEngine;
        
    private:
        static WindowManager* _instance;
        
        // There seems to be a bug with glut. If we press a key, then a second,
        // then release the second, but not the first, the key event for the first
        // one is not notified anymore.
        //
        // I had to come up with an elaborated work around for this. First there
        // are two key maps: one for standard keys, another for special keys.
        // Each key map (256 booleans large each) tells whether a key is down (true)
        // or up (false). This enables multiple key pressing. The work around
        // then consists of catching the glut keyboard events and map the keys
        // to these buffers, but not triggering the event right away. Later,
        // the GameEngine tells the WindowManager to fire the events (every
        // fixed interval) and the WindowManager dispatches an event, telling
        // that some key has either been pressed or released.
        //
        // Therefore, the final user does not have events such as key pressed or
        // key released, but instead a general keyboard event which gives access
        // to the boolean buffers, allowing the user to query which keys are up
        // or down.
        //
        // The listeners are not always notified, though. The listeners are
        // only notified for as long as there are keys down. We use two counters
        // for this, one for normal keys (_residualKey) and other for special
        // keys (_redisualSpecial). Each tell us how many keys are now of each
        // type at any given moment. The engine only fires the listeners if these
        // counters are non zero.
        static bool _keyState[KEYBOARD_KEY_COUNT];
        static bool _keySpecial[KEYBOARD_KEY_COUNT];
        
        static unsigned int _residualKey;
        static unsigned int _residualSpecial;
        
        
        /* TODO draw the depth buffer first? **/
        unsigned int _mode;
        unsigned int _clear;
        std::string _name;
        Color _clearColor;
        bool _visible;
        
        // Sometimes the engine tries to draw before the window is ready, which
        // causes an exception and crashes the application. This flag tells
        // whether the engine is ready to draw or not.
        bool _ready;
        
        void initialize();
        
    protected:
        WindowManager();
        WindowManager(const std::string name);
        WindowManager(const std::string name, const Color clearColor);
        WindowManager(const Color clearColor);
        
        void fireKeyboardEvent();       // Called by GameEngine to provoke a keyboard event
        
        void notify(ReshapeEvent evt);
        void notify(KeyboardNotificationEvent evt);
        
    public:
        static WindowManager* instance();
        
        void fire(KeyboardEvent evt);
        
        // Getters
        unsigned int getMode() const;
        unsigned int getClear() const;
        std::string getName() const;
        Color getClearColor() const;    // Don't return a reference. We need to know when to call glClearColor
        bool getVisible() const;
        int getWidth() const;
        int getHeight() const;
        int getTop() const;
        int getLeft() const;
        
        bool isReady() const;
        
        // Setters
        void setMode(unsigned int mode);
        void setClear(unsigned int clear);      // Setting zero disables clear
        void setName(const std::string name);
        void setClearColor(const Color cc);
        void setVisible(bool visible);
        void setSize(int width, int height);
        
        bool create(WindowType wnd, int width, int height);
        void destroy();
    };
}

#endif	/* WINDOWMANAGER_H */
