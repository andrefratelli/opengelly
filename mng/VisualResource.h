#ifndef VISUALRESOURCE_H
#define	VISUALRESOURCE_H

#include <mng/Resource.h>
#include <utils/SafePointer.h>
#include <model/Object.h>
#include <shared.h>
#include <string>

namespace eng
{
    class VisualResource : public Resource
    {
    private:
        /*
         * Both the batch and the object are shared by all instances that refer
         * to this object. Never duplicate either of them.
         */
        SafePointer<GLBatch> _batch;
        SafePointer<Object> _object;
        GLenum _primitive;
        std::string _location;
        
    public:
        VisualResource(SafePointer<Object> object, GLenum primitive, std::string location);
        VisualResource(const VisualResource& copy);
        
        SafePointer<GLBatch> getBatch();
        SafePointer<Object> getObject();
        
        GLenum getPrimitive() const;
        
        std::string getLocation() const;
        
        void load();
        void reset();
    };
}

#endif	/* VISUALRESOURCE_H */