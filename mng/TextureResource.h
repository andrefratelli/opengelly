#ifndef TEXTURERESOURCE_H
#define	TEXTURERESOURCE_H

#include <mng/Resource.h>
#include <utils/SafePointer.h>
#include <shared.h>
#include <string>

namespace eng
{
    class TextureResource : public Resource
    {                
    public:
        TextureResource(const std::string path);
        GLuint getTextureID(void);
        std::string getPath(void);
        void load(void);
    private:
        GLuint _textureID;
        std::string _path;
        bool LoadTGATexture(const char *szFileName, GLenum minFilter, GLenum magFilter, GLenum wrapMode);
    };
}

#endif	/* TEXTURERESOURCE_H */

