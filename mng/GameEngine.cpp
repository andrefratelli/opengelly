#include <mng/GameEngine.h>
#include <assert.h>
#include <shared.h>

using namespace eng;

GameEngine* GameEngine::_instance = NULL;

void GameEngine::initialize(int* argcp, char** argv)
{
    // Multiple engines?
    assert( _instance == NULL );
    
    _instance = this;
    
    gltSetWorkingDirectory(argv[0]);
    glutInit(argcp, argv);
}

GameEngine::GameEngine(int* argcp, char** argv, WindowManager* wm) :
    _wm(wm),
    _registration(wm->Handler<IdleListener, IdleEvent>::addEventListener(this)),
    _idleInterval(0.005f)
{
    initialize(argcp, argv);
}

GameEngine::GameEngine(int* argcp, char** argv, WindowManager* wm, GLfloat idleInterval) :
    _wm(wm),
    _registration(wm->Handler<IdleListener, IdleEvent>::addEventListener(this)),
    _idleInterval(idleInterval)
{
    initialize(argcp, argv);
}

GameEngine::~GameEngine() { _registration.remove(); }

GameEngine* GameEngine::instance() { return _instance; }

#include <mng/Renderer.h>

void GameEngine::notify(IdleEvent evt)
{
    if (getWatch().GetElapsedSeconds() >= getIdleInterval()) {
       
        // glut seems to use BIOS key repetition to trigger the keyboard events. The
        // problem is, each key is read, and only after an initial interval is each
        // key triggered (if continuously pressed) causing a very bad response from
        // the keyboard. This is bad in gamming...
        WindowManager::instance()->fireKeyboardEvent();
        
        LogicEvent evt;
        Renderer::instance()->notify(evt);
        getWindow()->Handler<LogicListener, LogicEvent>::fire(evt);
        
        getWatch().Reset();
    }
    
    glutPostRedisplay();
}

void GameEngine::setIdleInterval(GLfloat idleInterval)
{
    _idleInterval = idleInterval;
}

GLfloat GameEngine::getIdleInterval() const
{
    return _idleInterval;
}

WindowManager* GameEngine::getWindow() { return _wm; }

CStopWatch& GameEngine::getWatch() { return _watch; }
