#include <mng/VisualResource.h>

using namespace eng;

VisualResource::VisualResource(SafePointer<Object> object, GLenum primitive, std::string location) :
    _batch(new GLBatch()),
    _object(object),
    _primitive(primitive),
    _location(location)
{
    load();
}

/*
 * This is OK because both the batch and the object are pointers. Copying them
 * (the instances, not the pointers, that is) would be very painful.
 */
VisualResource::VisualResource(const VisualResource& copy) :
    _batch(copy._batch),
    _object(copy._object),
    _primitive(copy._primitive),
    _location(copy._location)
{}

SafePointer<GLBatch> VisualResource::getBatch()
{
    return _batch;
}

SafePointer<Object> VisualResource::getObject()
{
    return _object;
}

        
GLenum VisualResource::getPrimitive() const
{
    return _primitive;
}

void VisualResource::load()
{
    _batch->Begin(_primitive, _object->vertexes().size(), 1);
    _batch->CopyVertexData3f(&_object->buffers().vertexes()[0]);
    _batch->CopyNormalDataf(&_object->buffers().normals()[0]);
    _batch->CopyTexCoordData2f(&_object->buffers().textures()[0], 0);
    _batch->End();
}

void VisualResource::reset()
{
    _batch->Reset();
}

std::string VisualResource::getLocation() const
{
    return _location;
}