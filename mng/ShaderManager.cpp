#include <stdarg.h>
#include <stdio.h>
#include <shared.h>
#include <assert.h>
#include <mng/ShaderManager.h>
#include <mng/TextureResource.h>
#include <mng/ResourceManager.h>

#ifdef linux
#include <cstdlib>
#endif

using namespace eng;

ShaderManager::ShaderManager(void) :
    _dissolveTexture(ResourceManager::instance()->loadTexture("../OpenGelly/shaders/dissolveEffect.tga"))
{
    for (size_t i = 0; i < CUSTOM_SHADER_LAST; i++) {
        _uiCustomShaders[i] = 0;
    }
    
    InitializeCustomShaders();
    
    _ambientColor[0] = 0.1f;
    _ambientColor[1] = 0.1f;
    _ambientColor[2] = 0.1f;
    _ambientColor[3] = 1.0f;
    
    _diffuseColor[0] = 1.0f;
    _diffuseColor[1] = 1.0f;
    _diffuseColor[2] = 1.0f;
    _diffuseColor[3] = 1.0f;
    
    _specularColor[0] = 0.5f;
    _specularColor[1] = 0.5f;
    _specularColor[2] = 0.5f;
    _specularColor[3] = 1.0f;
}

void ShaderManager::InitializeCustomShaders(void)
{
    for (size_t i = 0; i < CUSTOM_SHADER_LAST; i++) {
        switch(i) {
            case CUSTOM_SHADER_FLAT:
                _uiCustomShaders[CUSTOM_SHADER_FLAT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Flat.vp", "../OpenGelly/shaders/Flat.fp", 1, ATTRIBUTE_VERTEX, "vPosition");
                break;
            case CUSTOM_SHADER_LAMBERT_1POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_LAMBERT_1POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Lambert.vp","../OpenGelly/shaders/Lambert.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal");
                break;
            case CUSTOM_SHADER_LAMBERT_2POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_LAMBERT_2POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Lambert.vp","../OpenGelly/shaders/Lambert.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal");
                break;
            case CUSTOM_SHADER_GOURAUD_1POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_GOURAUD_1POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Gouraud.vp", "../OpenGelly/shaders/Gouraud.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal");
                break;
            case CUSTOM_SHADER_GOURAUD_2POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_GOURAUD_2POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Gouraud.vp", "../OpenGelly/shaders/Gouraud.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal");
                break;
            case CUSTOM_SHADER_PHONG_1POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_PHONG_1POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Phong.vp", "../OpenGelly/shaders/Phong.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal");
                break;
            case CUSTOM_SHADER_PHONG_2POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_PHONG_2POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Phong.vp", "../OpenGelly/shaders/Phong.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal");
                break;
            case CUSTOM_SHADER_TOON_1POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_TOON_1POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Toon.vp", "../OpenGelly/shaders/Toon.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal");
                break;
            case CUSTOM_SHADER_TOON_2POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_TOON_2POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Toon.vp", "../OpenGelly/shaders/Toon.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal");
                break;
            case CUSTOM_SHADER_DISSOLVE_1POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_DISSOLVE_1POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Dissolve.vp","../OpenGelly/shaders/Dissolve.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_DISSOLVE_2POINT_LIGHT:
                _uiCustomShaders[CUSTOM_SHADER_DISSOLVE_2POINT_LIGHT] = LoadShaderPairWithAttributes("../OpenGelly/shaders/Dissolve.vp","../OpenGelly/shaders/Dissolve.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_FLAT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_FLAT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/FlatTexture.vp", "../OpenGelly/shaders/FlatTexture.fp", 2, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_LAMBERT_1POINT_LIGHT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_LAMBERT_1POINT_LIGHT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/LambertTexture.vp", "../OpenGelly/shaders/LambertTexture.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_LAMBERT_2POINT_LIGHT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_LAMBERT_2POINT_LIGHT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/LambertTexture.vp", "../OpenGelly/shaders/LambertTexture.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_GOURAUD_1POINT_LIGHT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_GOURAUD_1POINT_LIGHT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/GouraudTexture.vp", "../OpenGelly/shaders/GouraudTexture.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_GOURAUD_2POINT_LIGHT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_GOURAUD_2POINT_LIGHT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/GouraudTexture.vp", "../OpenGelly/shaders/GouraudTexture.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_PHONG_1POINT_LIGHT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_PHONG_1POINT_LIGHT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/PhongTexture.vp", "../OpenGelly/shaders/PhongTexture.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_PHONG_2POINT_LIGHT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_PHONG_2POINT_LIGHT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/PhongTexture.vp", "../OpenGelly/shaders/PhongTexture.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");
                break;
            case CUSTOM_SHADER_TOON_1POINT_LIGHT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_TOON_1POINT_LIGHT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/ToonTexture.vp", "../OpenGelly/shaders/ToonTexture.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");                
                break;
            case CUSTOM_SHADER_TOON_2POINT_LIGHT_TEXTURED:
                _uiCustomShaders[CUSTOM_SHADER_TOON_2POINT_LIGHT_TEXTURED] = LoadShaderPairWithAttributes("../OpenGelly/shaders/ToonTexture.vp", "../OpenGelly/shaders/ToonTexture.fp", 3, ATTRIBUTE_VERTEX, "vPosition", ATTRIBUTE_NORMAL, "vNormal", ATTRIBUTE_TEXTURE0, "vTexCoords");                
                break;
        }
    }
}

void ShaderManager::setAmbientColor(GLfloat r, GLfloat g, GLfloat b, GLfloat alpha)
{
    _ambientColor[0] = r;
    _ambientColor[1] = g;
    _ambientColor[2] = b;
    _ambientColor[3] = alpha; 
}

void ShaderManager::setAmbientColor(M3DVector4f ambientColor)
{
    m3dCopyVector4(_ambientColor, ambientColor);
}

void ShaderManager::setDiffuseColor(GLfloat r, GLfloat g, GLfloat b, GLfloat alpha)
{
    _diffuseColor[0] = r;
    _diffuseColor[1] = g;
    _diffuseColor[2] = b;
    _diffuseColor[3] = alpha;
}

void ShaderManager::setDiffuseColor(M3DVector4f diffuseColor)
{
    m3dCopyVector4(_diffuseColor, diffuseColor);
}

void ShaderManager::setSpecularColor(GLfloat r, GLfloat g, GLfloat b, GLfloat alpha)
{
    _specularColor[0] = r;
    _specularColor[1] = g;
    _specularColor[2] = b;
    _specularColor[3] = alpha;
}

void ShaderManager::setSpecularColor(M3DVector4f specularColor)
{
    m3dCopyVector4(_specularColor, specularColor);
}

ShaderManager::~ShaderManager(void)
{
    if(_uiCustomShaders[0] != 0)
        for (size_t i=0; i < CUSTOM_SHADER_LAST; i++)
            glDeleteProgram(_uiCustomShaders[i]);
}

/* All the shaders receive some arguments in the following order:
 * 1- mvMatrix
 * 2- mvpMatrix
 * 3- normalMatrix
 */
GLint ShaderManager::UseCustomShader(CUSTOM_SHADER nShaderID, ...)
{
    if(nShaderID >= CUSTOM_SHADER_LAST)
        return -1; 

    va_list uniformList;
    va_start(uniformList, nShaderID);
    
    glUseProgram(_uiCustomShaders[nShaderID]);
    
    GLint iAmbientColor, iDiffuseColor, iSpecularColor;
    GLint iColor;
    GLint iModelViewMatrix, iModelViewProjectionMatrix, iTextureUnit, iDissolveFactor;
    GLint iPointLight1, iPointLight2, iPointLight1On, iPointLight2On;
    
    static CStopWatch rotTimer;
    float dissolveFactor;
    
    int iInteger;
    
    bool pointLight1On = true, pointLight2On = false;
    
    M3DMatrix44f* mModelViewProjection;
    M3DMatrix44f* mModelView;
    M3DVector4f* vLightPos1;
    M3DVector4f* vLightPos2;
    M3DVector4f* vColor;
    
    /*
     * AMBIENT, SPECULAR AND DIFFUSE COLORS
     */
    iAmbientColor = glGetUniformLocation(_uiCustomShaders[nShaderID], "vAmbientColor");
    glUniform4fv(iAmbientColor, 1, _ambientColor);
    
    iDiffuseColor = glGetUniformLocation(_uiCustomShaders[nShaderID], "vDiffuseColor");
    glUniform4fv(iDiffuseColor, 1, _diffuseColor);
    
    iSpecularColor = glGetUniformLocation(_uiCustomShaders[nShaderID], "vSpecularColor");
    glUniform4fv(iSpecularColor, 1, _specularColor);

    iModelViewMatrix = glGetUniformLocation(_uiCustomShaders[nShaderID], "mvMatrix");
    mModelView = va_arg(uniformList, M3DMatrix44f*);
    glUniformMatrix4fv(iModelViewMatrix, 1, GL_FALSE, *mModelView);
    
    iModelViewProjectionMatrix = glGetUniformLocation(_uiCustomShaders[nShaderID], "mvpMatrix");
    mModelViewProjection = va_arg(uniformList, M3DMatrix44f*);
    glUniformMatrix4fv(iModelViewProjectionMatrix, 1, GL_FALSE, *mModelViewProjection);
    
    iPointLight1 = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight1Position");
    vLightPos1 = va_arg(uniformList, M3DVector4f*);
    glUniform4fv(iPointLight1, 1, *vLightPos1);

    iPointLight1On = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight1On");
    glUniform1i(iPointLight1On, pointLight1On);
    
    switch(nShaderID)
    {
        case CUSTOM_SHADER_FLAT:
            iColor = glGetUniformLocation(_uiCustomShaders[nShaderID], "vColor");
            vColor = va_arg(uniformList, M3DVector4f*);
            glUniform4fv(iColor, 1, *vColor);
            break;
        default:
            switch(nShaderID)
            {
                /*
                 * SHADERS WITH COLORS
                 */
                //ONE LIGHT SOURCE
                case CUSTOM_SHADER_LAMBERT_1POINT_LIGHT:
                case CUSTOM_SHADER_GOURAUD_1POINT_LIGHT:
                case CUSTOM_SHADER_PHONG_1POINT_LIGHT:
                case CUSTOM_SHADER_TOON_1POINT_LIGHT:
                    iPointLight2On = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2On");
                    glUniform1i(iPointLight2On, pointLight2On);

                    /*
                     * GET COLOR
                     */
                    iColor = glGetUniformLocation(_uiCustomShaders[nShaderID], "vColor");
                    vColor = va_arg(uniformList, M3DVector4f*);
                    glUniform4fv(iColor, 1, *vColor);
                    break;
                case CUSTOM_SHADER_DISSOLVE_1POINT_LIGHT:
                    iColor = glGetUniformLocation(_uiCustomShaders[nShaderID], "vColor");
                    vColor = va_arg(uniformList, M3DVector4f*);
                    glUniform4fv(iColor, 1, *vColor);
                    
                    iPointLight2On = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2On");
                    glUniform1i(iPointLight2On, pointLight2On);
                    
                    iTextureUnit = glGetUniformLocation(_uiCustomShaders[nShaderID], "dissolveTexture");
                    glUniform1i(iTextureUnit, _dissolveTexture.getTextureID());

                    dissolveFactor = fmod(rotTimer.GetElapsedSeconds(), 12.0f);
                    dissolveFactor /= 10.0f;
                    iDissolveFactor = glGetUniformLocation(_uiCustomShaders[nShaderID], "dissolveFactor");
                    glUniform1f(iDissolveFactor, dissolveFactor);
                    break;
                //TWO LIGHT SOURCES
                case CUSTOM_SHADER_LAMBERT_2POINT_LIGHT:
                case CUSTOM_SHADER_GOURAUD_2POINT_LIGHT:
                case CUSTOM_SHADER_PHONG_2POINT_LIGHT:
                case CUSTOM_SHADER_TOON_2POINT_LIGHT:
                    iPointLight2 = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2Position");
                    vLightPos2 = va_arg(uniformList, M3DVector4f*);
                    glUniform4fv(iPointLight2, 1, *vLightPos2);

                    pointLight2On = true;
                    iPointLight2On = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2On");
                    glUniform1i(iPointLight2On, pointLight2On);

                    /*
                     * GET COLOR
                     */
                    iColor = glGetUniformLocation(_uiCustomShaders[nShaderID], "vColor");
                    vColor = va_arg(uniformList, M3DVector4f*);
                    glUniform4fv(iColor, 1, *vColor);
                    break;
                case CUSTOM_SHADER_DISSOLVE_2POINT_LIGHT:
                    iColor = glGetUniformLocation(_uiCustomShaders[nShaderID], "vColor");
                    vColor = va_arg(uniformList, M3DVector4f*);
                    glUniform4fv(iColor, 1, *vColor);
                    
                    iPointLight2On = true;
                    iPointLight2On = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2On");
                    glUniform1i(iPointLight2On, pointLight2On);
                    
                    iPointLight2 = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2Position");
                    vLightPos2 = va_arg(uniformList, M3DVector4f*);
                    glUniform4fv(iPointLight2, 1, *vLightPos2);
                    
                    iTextureUnit = glGetUniformLocation(_uiCustomShaders[nShaderID], "dissolveTexture");
                    glUniform1i(iTextureUnit, _dissolveTexture.getTextureID());
                    
                    dissolveFactor = fmod(rotTimer.GetElapsedSeconds(), 12.0f);
                    dissolveFactor /= 10.0f;
                    iDissolveFactor = glGetUniformLocation(_uiCustomShaders[nShaderID], "dissolveFactor");
                    glUniform1f(iDissolveFactor, dissolveFactor);
                    break;
                /*
                 * SHADERS WITH TEXTURES
                 */    
                //ONE LIGHT SOURCE
                case CUSTOM_SHADER_FLAT_TEXTURED:
                    iTextureUnit = glGetUniformLocation(_uiCustomShaders[nShaderID], "colorMap");
                    iInteger = va_arg(uniformList, int);
                    glUniform1i(iTextureUnit, iInteger);
                    break;
                case CUSTOM_SHADER_LAMBERT_1POINT_LIGHT_TEXTURED:
                case CUSTOM_SHADER_GOURAUD_1POINT_LIGHT_TEXTURED:
                case CUSTOM_SHADER_PHONG_1POINT_LIGHT_TEXTURED:
                case CUSTOM_SHADER_TOON_1POINT_LIGHT_TEXTURED:
                    iPointLight2On = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2On");
                    glUniform1i(iPointLight2On, pointLight2On);

                    /*
                     * GET TEXTURES
                     */
                    iTextureUnit = glGetUniformLocation(_uiCustomShaders[nShaderID], "colorMap");
                    iInteger = va_arg(uniformList, int);
                    glUniform1i(iTextureUnit, iInteger);
                    break;
                //TWO LIGHTS SOURCES
                case CUSTOM_SHADER_TOON_2POINT_LIGHT_TEXTURED:
                case CUSTOM_SHADER_LAMBERT_2POINT_LIGHT_TEXTURED:
                case CUSTOM_SHADER_GOURAUD_2POINT_LIGHT_TEXTURED:
                case CUSTOM_SHADER_PHONG_2POINT_LIGHT_TEXTURED:            
                    iPointLight2 = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2Position");
                    vLightPos2 = va_arg(uniformList, M3DVector4f*);
                    glUniform4fv(iPointLight2, 1, *vLightPos2);
                    pointLight2On = true;
                    iPointLight2On = glGetUniformLocation(_uiCustomShaders[nShaderID], "vPointLight2On");
                    glUniform1i(iPointLight2On, pointLight2On);

                    /*
                     * GET TEXTURES
                     */
                    iTextureUnit = glGetUniformLocation(_uiCustomShaders[nShaderID], "colorMap");
                    iInteger = va_arg(uniformList, int);
                    glUniform1i(iTextureUnit, iInteger);
                    break;
             }
    }
    
    va_end(uniformList);
    
    return _uiCustomShaders[nShaderID];
}

static GLubyte shaderText[MAX_SHADER_LENGTH];

void ShaderManager::LoadShaderSrc(const char *szShaderSrc, GLuint shader)
{
    GLchar *fsStringPtr[1];
    
    fsStringPtr[0] = (GLchar *)szShaderSrc;
    glShaderSource(shader, 1, (const GLchar **)fsStringPtr, NULL);
}

bool ShaderManager::LoadShaderFile(const char *szFile, GLuint shader)
{
    GLint shaderLength = 0;
    FILE *fp;
    
    fp = fopen(szFile, "r");
    if (fp != NULL)
    {
        while (fgetc(fp) != EOF)
            shaderLength++;
        
        assert(shaderLength < MAX_SHADER_LENGTH);
        if (shaderLength > MAX_SHADER_LENGTH)
        {
            fclose(fp);
            return false;
        }
        
        rewind(fp);
        
        if (shaderText != NULL)
            fread(shaderText, 1, shaderLength, fp);
        
        shaderText[shaderLength] = '\0';
        
        fclose(fp);
    }
    else
        return false;
    
    LoadShaderSrc((const char *)shaderText, shader);
    
    return true;
}

GLuint ShaderManager::LoadShaderPairWithAttributes(const char* szVertexProg, const char* szFragmentProg, ...)
{    
    //Temporary Shader Objects
    GLuint hVertexShader;
    GLuint hFragmentShader;
    GLuint hReturn = 0;
    GLint testVal;
    
    //Create shader objects
    hVertexShader = glCreateShader(GL_VERTEX_SHADER);
    hFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    //Load them
    if (LoadShaderFile(szVertexProg, hVertexShader) == false)
    {   
        glDeleteShader(hVertexShader);
        glDeleteShader(hFragmentShader);
        fprintf(stderr, "The shader at %s could not be found.\n", szVertexProg);
        return (GLuint)NULL;
    }
    
    if (LoadShaderFile(szFragmentProg, hFragmentShader) == false)
    {
        glDeleteShader(hVertexShader);
        glDeleteShader(hFragmentShader);
        fprintf(stderr, "The shader at %s could not be found.\n", szFragmentProg);
        return (GLuint)NULL;
    }
    
    //Compile both
    glCompileShader(hVertexShader);
    glCompileShader(hFragmentShader);
    
    //Check for errors
    glGetShaderiv(hVertexShader, GL_COMPILE_STATUS, &testVal);
    if (testVal == GL_FALSE)
    {
        char infoLog[1024];
        glGetShaderInfoLog(hVertexShader, 1024, NULL, infoLog);
        fprintf(stderr, "The shader at %s failed to compile with the following error:\n%s\n", szVertexProg, infoLog);
        glDeleteShader(hVertexShader);
        glDeleteShader(hFragmentShader);
        return (GLuint)NULL;
    }
    
    glGetShaderiv(hFragmentShader, GL_COMPILE_STATUS, &testVal);
    if (testVal == GL_FALSE)
    {
        char infoLog[1024];
        glGetShaderInfoLog(hFragmentShader, 1024, NULL, infoLog);
        fprintf(stderr, "The shader at %s failed to compile with the following error:\n%s\n", szFragmentProg, infoLog);
        glDeleteShader(hVertexShader);
        glDeleteShader(hFragmentShader);
        return (GLuint)NULL;
    }
    
    //Create the final program object, and attach the shaders
    hReturn = glCreateProgram();
    glAttachShader(hReturn, hVertexShader);
    glAttachShader(hReturn, hFragmentShader);
    
    //Bind the attribute names to their specific locations
    va_list attributeList;
    va_start(attributeList, szFragmentProg);

    //Iterate over the list arguments
    char *szNextArg;
    int iArgCount = va_arg(attributeList, int);
    for (size_t i=0; i < iArgCount; i++) {
        int index = va_arg(attributeList, int);
        szNextArg = va_arg(attributeList, char*);
        glBindAttribLocation(hReturn, index, szNextArg);
    }
    va_end(attributeList);
    
    //Attempt to link
    glLinkProgram(hReturn);
    
    //These are no longer needed
    glDeleteShader(hVertexShader);
    glDeleteShader(hFragmentShader);
    
    //Make sure link worked
    glGetProgramiv(hReturn, GL_LINK_STATUS, &testVal);
    if (testVal == GL_FALSE)
    {
        char infoLog[1024];
        glGetProgramInfoLog(hReturn, 1024, NULL, infoLog);
        fprintf(stderr, "The program %s and %s failed to link with the following errors:\n%s\n", szVertexProg, szFragmentProg, infoLog);
        glDeleteProgram(hReturn);
        return (GLuint)NULL;
    }
   
    //Return the shader program
    return hReturn;
}