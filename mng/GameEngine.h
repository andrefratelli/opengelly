#ifndef GAMEENGINE_H
#define	GAMEENGINE_H

#include <evts/IdleListener.h>
#include <evts/HandlerRegistration.h>
#include <mng/WindowManager.h>
#include <evts/Handler.h>
#include <shared.h>

namespace eng
{
    class GameEngine :
        public IdleListener
    {
    private:
        static GameEngine* _instance;
        
        HandlerRegistration<IdleListener, IdleEvent> _registration;
        CStopWatch _watch;
        WindowManager* _wm;
        GLfloat _idleInterval;
        
        void initialize(int* argcp, char** argv);
        
    protected:
        GameEngine(int* argcp, char** argv, WindowManager* wm);
        GameEngine(int* argcp, char** argv, WindowManager* wm, GLfloat idleInterval);
        ~GameEngine();
        
    public:
        static GameEngine* instance();
        
        void notify(IdleEvent evt);
        
        void setIdleInterval(GLfloat idleInterval);
        GLfloat getIdleInterval() const;
        
        WindowManager* getWindow();
        CStopWatch& getWatch();
    };
}

#endif	/* GAMEENGINE_H */
