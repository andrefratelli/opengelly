#ifndef SHADERMANAGER_H
#define	SHADERMANAGER_H

#include <shared.h>
#include <mng/TextureResource.h>

#define MAX_SHADER_NAME_LENGTH 64
#define MAX_SHADER_LENGTH 8192

enum CUSTOM_SHADER {CUSTOM_SHADER_FLAT = 0,
                    CUSTOM_SHADER_LAMBERT_1POINT_LIGHT,
                    CUSTOM_SHADER_LAMBERT_2POINT_LIGHT,
                    CUSTOM_SHADER_GOURAUD_1POINT_LIGHT,
                    CUSTOM_SHADER_GOURAUD_2POINT_LIGHT,
                    CUSTOM_SHADER_PHONG_1POINT_LIGHT,
                    CUSTOM_SHADER_PHONG_2POINT_LIGHT,
                    CUSTOM_SHADER_TOON_1POINT_LIGHT,
                    CUSTOM_SHADER_TOON_2POINT_LIGHT,
                    CUSTOM_SHADER_DISSOLVE_1POINT_LIGHT,
                    CUSTOM_SHADER_DISSOLVE_2POINT_LIGHT,
                    CUSTOM_SHADER_FLAT_TEXTURED,
                    CUSTOM_SHADER_LAMBERT_1POINT_LIGHT_TEXTURED,
                    CUSTOM_SHADER_LAMBERT_2POINT_LIGHT_TEXTURED,
                    CUSTOM_SHADER_GOURAUD_1POINT_LIGHT_TEXTURED,
                    CUSTOM_SHADER_GOURAUD_2POINT_LIGHT_TEXTURED,
                    CUSTOM_SHADER_PHONG_1POINT_LIGHT_TEXTURED,
                    CUSTOM_SHADER_PHONG_2POINT_LIGHT_TEXTURED,
                    CUSTOM_SHADER_TOON_1POINT_LIGHT_TEXTURED,
                    CUSTOM_SHADER_TOON_2POINT_LIGHT_TEXTURED,
                    CUSTOM_SHADER_LAST};
                    
enum CUSTOM_SHADER_ATTRIBUTE {ATTRIBUTE_VERTEX = 0,
                              ATTRIBUTE_COLOR,
                              ATTRIBUTE_NORMAL,
                              ATTRIBUTE_TEXTURE0,
                              ATTRIBUTE_TEXTURE1,
                              ATTRIBUTE_TEXTURE2,
                              ATTRIBUTE_TEXTURE3,
                              ATTRIBUTE_LAST};
                              
struct SHADERLOOKUPENTRY {
    char szVertexShaderName[MAX_SHADER_NAME_LENGTH];
    char szFragShaderName[MAX_SHADER_NAME_LENGTH];
    GLuint uiShaderID;
};

namespace eng
{
    class ShaderManager
    {
    public:
        ShaderManager(void);
        ~ShaderManager(void);        
        
        void setSpecularColor(GLfloat r, GLfloat g, GLfloat b, GLfloat alpha);
        void setSpecularColor(M3DVector4f specularColor);
        
        void setDiffuseColor(GLfloat r, GLfloat g, GLfloat b, GLfloat alpha);
        void setDiffuseColor(M3DVector4f diffuseColor);
        
        void setAmbientColor(GLfloat r, GLfloat g, GLfloat b, GLfloat alpha);
        void setAmbientColor(M3DVector4f ambientColor);
        
        GLint UseCustomShader(CUSTOM_SHADER nShaderID, ...);        
    private:
        void InitializeCustomShaders(void);
        void LoadShaderSrc(const char *szShaderSrc, GLuint shader);
        bool LoadShaderFile(const char *szFile, GLuint shader);
        GLuint LoadShaderPairWithAttributes(const char *szVertexProg, const char *szFragmentProg, ...);
        TextureResource _dissolveTexture;
    protected:
        GLuint _uiCustomShaders[CUSTOM_SHADER_LAST];
        M3DVector4f _ambientColor;
        M3DVector4f _diffuseColor;
        M3DVector3f _specularColor;
    };
}

#endif	/* SHADERMANAGER_H */