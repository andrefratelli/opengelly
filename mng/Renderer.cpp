#include <mng/Renderer.h>
#include <gfx/Camera.h>
#include <gfx/VisualObject.h>
#include <mng/ShaderManager.h>
#include "phy/CollisionDetection.h"

using namespace eng;

Renderer* Renderer::_instance = new Renderer();

Renderer::Renderer() :
    _shaderManager(NULL)
{
    WindowManager::instance()->Handler<DisplayListener, DisplayEvent>::addEventListener(this);
}

SafePointer<VisualObject> Renderer::load(const ResourceManager::visual_key_type path, size_t octDepth, const ResourceManager::texture_key_type texturePath) {
    _collection.push_back(SafePointer<VisualObject>(new VisualObject(ResourceManager::instance()->loadVisual(path, GL_TRIANGLES), octDepth, ResourceManager::instance()->loadTexture(texturePath))));
    return _collection.back();
}

SafePointer<VisualObject> Renderer::load(const ResourceManager::visual_key_type path, size_t octDepth, const ResourceManager::texture_key_type texturePath, CUSTOM_SHADER shaderID) {
    _collection.push_back(SafePointer<VisualObject>(new VisualObject(ResourceManager::instance()->loadVisual(path, GL_TRIANGLES), octDepth, ResourceManager::instance()->loadTexture(texturePath), shaderID)));
    return _collection.back();
}

SafePointer<VisualObject> Renderer::load(const ResourceManager::visual_key_type path, size_t octDepth, Color c)
{
    _collection.push_back(SafePointer<VisualObject>(new VisualObject(ResourceManager::instance()->loadVisual(path, GL_TRIANGLES), octDepth, c)));
    return _collection.back();
}

SafePointer<VisualObject> Renderer::load(const ResourceManager::visual_key_type path, size_t octDepth, Color c, CUSTOM_SHADER shaderID)
{
    _collection.push_back(SafePointer<VisualObject>(new VisualObject(ResourceManager::instance()->loadVisual(path, GL_TRIANGLES), octDepth, c, shaderID)));
    return _collection.back();
}

SafePointer<VisualObject> Renderer::load(const ResourceManager::visual_key_type path, size_t octDepth, GLenum primitive, const ResourceManager::texture_key_type texturePath) {
    _collection.push_back(SafePointer<VisualObject>(
        new VisualObject(
            ResourceManager::instance()->loadVisual(path, primitive),
            octDepth,
            ResourceManager::instance()->loadTexture(texturePath)
        ))
    );
    return _collection.back();
}

SafePointer<VisualObject> Renderer::load(const ResourceManager::visual_key_type path, size_t octDepth, GLenum primitive, const ResourceManager::texture_key_type texturePath, CUSTOM_SHADER shaderID) {
    _collection.push_back(SafePointer<VisualObject>(new VisualObject(ResourceManager::instance()->loadVisual(path, primitive), octDepth, ResourceManager::instance()->loadTexture(texturePath), shaderID)));
    return _collection.back();
}

SafePointer<VisualObject> Renderer::load(const ResourceManager::visual_key_type path, size_t octDepth, GLenum primitive, Color c)
{
    _collection.push_back(SafePointer<VisualObject>(new VisualObject(ResourceManager::instance()->loadVisual(path, primitive), octDepth, c)));
    return _collection.back();
}

SafePointer<VisualObject> Renderer::load(const ResourceManager::visual_key_type path, size_t octDepth, GLenum primitive, Color c, CUSTOM_SHADER shaderID)
{
    _collection.push_back(SafePointer<VisualObject>(new VisualObject(ResourceManager::instance()->loadVisual(path, primitive), octDepth, c, shaderID)));
    return _collection.back();
}

SafePointer<VisualObject> Renderer::load(
        const ResourceManager::visual_key_type key,
        size_t octDepth, 
        GLfloat vertexes[],
        GLuint size,
        GLenum primitive,
        Color c
)
{
    Buffers::buffer_vt vert(vertexes, vertexes + size);
    return load(key, octDepth, vert, primitive, c);
}

SafePointer<VisualObject> Renderer::load(
        const ResourceManager::visual_key_type key,
        size_t octDepth, 
        Buffers::buffer_vt vertexes,
        GLenum primitive,
        Color c
)
{
    _collection.push_back(SafePointer<VisualObject>(new VisualObject(ResourceManager::instance()->loadVisual(key, vertexes, primitive), octDepth, c)));
    return _collection.back();
}

SafePointer<ParticleSystem> Renderer::loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, const ResourceManager::texture_key_type texturePath)
{
    SafePointer<ParticleSystem> p = new ParticleSystem(
        ResourceManager::instance()->loadVisual(path, GL_TRIANGLES),
        ResourceManager::instance()->loadVisual(path, GL_TRIANGLES),
        octDepth,
        ResourceManager::instance()->loadTexture(texturePath)
    );
    
    loadParticles(*p);
    
    return SafePointer<ParticleSystem>((ParticleSystem*)&(*_collection.back()));
}

SafePointer<ParticleSystem> Renderer::loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, const ResourceManager::texture_key_type texturePath, CUSTOM_SHADER shaderID)
{
    SafePointer<ParticleSystem> p = new ParticleSystem(
        ResourceManager::instance()->loadVisual(path, GL_TRIANGLES),
        ResourceManager::instance()->loadVisual(pp, GL_TRIANGLES),
        octDepth,
        ResourceManager::instance()->loadTexture(texturePath),
        shaderID
    );
    
    loadParticles(*p);
    
    return p;
}

SafePointer<ParticleSystem> Renderer::loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, Color c)
{
    SafePointer<ParticleSystem> p = new ParticleSystem(
        ResourceManager::instance()->loadVisual(path, GL_TRIANGLES),
        ResourceManager::instance()->loadVisual(pp, GL_TRIANGLES),
        octDepth,
        c
    );
    
    loadParticles(*p);
    
    return p;
}

SafePointer<ParticleSystem> Renderer::loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, Color c, CUSTOM_SHADER shaderID)
{
    SafePointer<ParticleSystem> p = new ParticleSystem(
        ResourceManager::instance()->loadVisual(path, GL_TRIANGLES),
        ResourceManager::instance()->loadVisual(pp, GL_TRIANGLES),
        octDepth,
        c,
        shaderID
    );
    
    loadParticles(*p);
    
    return p;
}

SafePointer<ParticleSystem> Renderer::loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, GLenum primitive, const ResourceManager::texture_key_type texturePath)
{
    SafePointer<ParticleSystem> p = new ParticleSystem(
        ResourceManager::instance()->loadVisual(path, primitive),
        ResourceManager::instance()->loadVisual(pp, primitive),
        octDepth,
        ResourceManager::instance()->loadTexture(texturePath)
    );
    
    loadParticles(*p);
    
    return p;
}

SafePointer<ParticleSystem> Renderer::loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, GLenum primitive, const ResourceManager::texture_key_type texturePath, CUSTOM_SHADER shaderID)
{
    SafePointer<ParticleSystem> p = new ParticleSystem(
        ResourceManager::instance()->loadVisual(path, primitive),
        ResourceManager::instance()->loadVisual(pp, primitive),
        octDepth,
        ResourceManager::instance()->loadTexture(texturePath),
        shaderID
    );
    
    loadParticles(*p);
    
    return p;
}

SafePointer<ParticleSystem> Renderer::loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, GLenum primitive, Color c)
{
    SafePointer<ParticleSystem> p = new ParticleSystem(
        ResourceManager::instance()->loadVisual(path, primitive),
        ResourceManager::instance()->loadVisual(pp, primitive),
        octDepth,
        c
    );
    
    loadParticles(*p);
    
    return p;
}

SafePointer<ParticleSystem> Renderer::loadParticles(const ResourceManager::visual_key_type path, const ResourceManager::visual_key_type pp, size_t octDepth, GLenum primitive, Color c, CUSTOM_SHADER shaderID)
{
    SafePointer<ParticleSystem> p = new ParticleSystem(
        ResourceManager::instance()->loadVisual(path, primitive),
        ResourceManager::instance()->loadVisual(pp, primitive),
        octDepth,
        c,
        shaderID
    );
    
    loadParticles(*p);
    
    return p;
}

ShaderManager* Renderer::shaderManager()
{
    if (_shaderManager == NULL)
          _shaderManager = new ShaderManager();
    return _shaderManager;
}

Renderer* Renderer::instance()
{
    return _instance;
}

void Renderer::notify(LogicEvent evt)
{
    std::vector<SafePointer<VisualObject> >::iterator it = _collection.begin();

    while (it + 1 != _collection.end())
    {
        if ((*it)->getCollisionEnabled() && (*it)->getVisible())
        {
            std::vector<SafePointer<VisualObject> >::iterator jt=it+1;
            
            while (jt != _collection.end())
            {
                if ((*jt)->getCollisionEnabled() && (*jt)->getVisible())
                {
                    if ((*it)->collides(**jt, PHY_DEEP, true))
                    {
                        // TODO response
                    }
                }
                jt++;
            }
        }
        it++;
    }
}

void Renderer::loadParticles(ParticleSystem& ps)
{
    std::vector<SafePointer<VisualObject> >& particles = ps.getParticles();
    size_t it;
    
    for (it=0 ; it<particles.size() ; it++)
    {
        _collection.push_back(particles[it]);
    }
}

void Renderer::notify(DisplayEvent evt)
{
    glEnable(GL_DEPTH_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    if (WindowManager::instance()->isReady())
    {
        M3DVector4f vLightPos = {0.0f, 200.0f, 0.0f, 1.0f};
        M3DVector4f vLightEyePos;
        m3dTransformVector4(vLightEyePos, vLightPos, Camera::getActive()->getMatrix());

        std::vector<SafePointer<VisualObject> >::iterator it = _collection.begin();

        while (it != _collection.end()) {
            if ((*it)->getVisible()) {
                (*it)->draw(vLightEyePos);
            }
            it++;
        }
    }
}
