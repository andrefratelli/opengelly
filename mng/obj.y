%{
#include <model/Buffers.h>
#include <model/Object.h>
#include <model/Group.h>
#include <vector>
#include <GL/gl.h>
#include <sstream>
#include <stdio.h>
#include <string>

using namespace eng;

Buffers::buffer_vt* _vertexes = NULL;
Buffers::buffer_vt* _textures = NULL;
Buffers::buffer_vt* _normals = NULL;
Object* _object = NULL;
Group* _active_group = NULL;

FILE *_errStream = NULL;
const char *_parsePath = NULL;

size_t _facepoints_size;
size_t _facepoints_index;

int yylex();
void yyerror(const char*);

Vertex* pushVertex(size_t index);
Texture* pushTexture(size_t index);
Normal* pushNormal(size_t index);
Object* parseObj(std::string& path);
Object* parseObj(std::string& path, FILE* err);
Object* parseObj(const char*);
Object* parseObj(const char* path, FILE* err);

/*
 * We cannot init the bound box buffer to any value because then one would never
 * be sure if the vertices would be within that value.
 */
bool _tbb = false;

// Bound box
M3DVector2f vX, vY, vZ;

%}
%union
{
    double number_t;
    class FacePoint* facepoint_t;
    char* string_t;
    class FacePoint** facepoint_vp;
    class Face* face_t;
    class Group* group_t;
}
%token <number_t> NUMBER
%token MTLLIB
%token POINT
%token LINE
%token SEP
%token DSEP
%token USEMTL
%token FACE
%token NORM_COORDS
%token OFF
%token MATERIAL
%token TEXT_COORDS
%token GROUP
%token SMOOTH
%token OBJ_NAME
%token <string_t> NAME
%token VERT_COORDS
%token MTLFILE
%type <face_t> face
%type <facepoint_vp> facepoint_sequence
%type <facepoint_t> facepoint
%type <group_t> group
%%
obj_file:   {
                _object = new Object();
                _vertexes = new Buffers::buffer_vt();
                _textures = new Buffers::buffer_vt();
                _normals = new Buffers::buffer_vt();
                _tbb = false;
            }
            obj
            {
                _object->setLimits(vX, vY, vZ);
                _object->setBaseVertices(_vertexes);
                // delete _vertexes; _vertexes = NULL;

                delete _textures; _textures = NULL;
                delete _normals; _normals = NULL;
            }
;

obj:    /* empty */         {}
        | obj line_entry    {}
;

line_entry: vertex      { /* done */ }
            | texture   { /* done */ }
            | normal    { /* node */ }
            | mtllib    {}
            | group     { _active_group = $1; _object->groups().push_back($1); }
            | point     {}
            | line      {}
            | face      {
                            _object->faces().push_back($1);
                            if (_active_group != NULL) { _active_group->faces().push_back($1); }
                        }
            | usemtllib {}
            | object    {}
            | smooth    {}
;

vertex: VERT_COORDS NUMBER NUMBER NUMBER    { _vertexes->push_back($2); _vertexes->push_back($3); _vertexes->push_back($4); }
;

texture: TEXT_COORDS NUMBER NUMBER NUMBER   { _textures->push_back($2); _textures->push_back($3); }
        | TEXT_COORDS NUMBER NUMBER         { _textures->push_back($2); _textures->push_back($3); }
;

normal: NORM_COORDS NUMBER NUMBER NUMBER    { _normals->push_back($2); _normals->push_back($3); _normals->push_back($4); }
;

mtllib: MTLLIB MTLFILE                      {}
;

group:  GROUP NAME              { $$ = new Group($2); }
        | GROUP NUMBER          { $$ = new Group(""); /* TODO */ }
        | GROUP NUMBER NAME     { $$ = new Group(""); /* TODO */ }
        | GROUP                 { $$ = new Group(""); }
;

point:  POINT NUMBER            {}
;

line:   LINE point_sequence     {}
;

face:   FACE facepoint_sequence { $$ = new Face( Face::facepoints_vpt($2, $2 + _facepoints_size) ); _facepoints_size = 0; }
;

point_sequence:	/* empty */														{}
                | { /*_linepoints_size++; */} NUMBER point_sequence {}
;

facepoint_sequence: /* empty */ { $$ = new FacePoint *[_facepoints_size]; _facepoints_index = 0; }
                    | { _facepoints_size++; }
                        facepoint facepoint_sequence
                      {
                        _facepoints_index++;
                        $3[_facepoints_size - _facepoints_index] = $2;
                        _object->facepoints().push_back($2);
                        $$ = $3;
                      }
;

facepoint:NUMBER DSEP NUMBER            {
                                            pushVertex($1);
                                            pushNormal($3);

                                            $$ = new FacePoint(
                                                &_object->vertexes(),
                                                NULL,
                                                &_object->normals()
                                            );
                                        }
        | NUMBER SEP NUMBER SEP NUMBER  {
                                            pushVertex($1);
                                            pushTexture($3);
                                            pushNormal($5);
                                            $$ = new FacePoint(
                                                &_object->vertexes(),
                                                &_object->textures(),
                                                &_object->normals()
                                            );
                                        }
        | NUMBER SEP NUMBER             {
                                            pushVertex($1);
                                            pushTexture($3);
                                            $$ = new FacePoint(
                                                &_object->vertexes(),
                                                &_object->textures(),
                                                NULL
                                            );
                                        }
        | NUMBER                        {
                                            pushVertex($1);
                                            $$ = new FacePoint(
                                                &_object->vertexes(),
                                                NULL,
                                                NULL
                                            );
                                        }
;

usemtllib:  USEMTL NAME                 {}
;

object: OBJ_NAME NAME                   {}
;

smooth: SMOOTH NUMBER   {}
        | SMOOTH OFF    {}
;
%%

Vertex* pushVertex(size_t idx)
{
    idx = (idx - 1) * 3;

    GLfloat x, y, z;

    x = (*_vertexes)[idx + 0];
    y = (*_vertexes)[idx + 1];
    z = (*_vertexes)[idx + 2];

    
    _object->buffers().vertexes().push_back(x);
    _object->buffers().vertexes().push_back(y);
    _object->buffers().vertexes().push_back(z);

    Vertex* vertex = new Vertex(&_object->buffers().vertexes());

    _object->vertexes().push_back(vertex);

    if (_tbb)
    {
        if (x < vX[0]) { vX[0] = x; }
        if (x > vX[1]) { vX[1] = x; }

        if (y < vY[0]) { vY[0] = y; }
        if (y > vY[1]) { vY[1] = y; }

        if (z < vZ[0]) { vZ[0] = z; }
        if (z > vZ[1]) { vZ[1] = z; }
    }
    else
    {
        vX[0] = vX[1] = x;
        vY[0] = vY[1] = y;
        vZ[0] = vZ[1] = z;

        // Indicate that we have already triggered the bound box initialization
        // for this object
        _tbb = true;
    }

    return vertex;
}

Texture* pushTexture(size_t idx)
{
    idx = (idx - 1) * 2;

    _object->buffers().textures().push_back( (*_textures)[idx + 0] );
    _object->buffers().textures().push_back( (*_textures)[idx + 1] );

    Texture* texture = new Texture(&_object->buffers().textures());

    _object->textures().push_back(texture);

    return texture;
}

Normal* pushNormal(size_t idx)
{
    idx = (idx - 1) * 3;

    _object->buffers().normals().push_back( (*_normals)[idx + 0] );
    _object->buffers().normals().push_back( (*_normals)[idx + 1] );
    _object->buffers().normals().push_back( (*_normals)[idx + 2] );

    Normal* normal = new Normal(&_object->buffers().normals());

    _object->normals().push_back(normal);

    return normal;
}

void yyerror(char const *pmsg)
{
    if (_errStream != NULL) {
        fprintf(_errStream, "Error parsing file [%s]: %s\n", _parsePath, pmsg);
    }
}

Object* parseObj(const std::string path)
{
    return parseObj(path.c_str());
}

Object* parseObj(const std::string path, FILE* err)
{
    return parseObj(path.c_str(), err);
}

Object* parseObj(const char* path)
{
    return parseObj(path, stderr);
}

Object* parseObj(const char* path, FILE* err)
{
    // Initialize
    _errStream = err;
    _parsePath = path;

    FILE *fp = freopen(path, "r", stdin);
    int _parse = yyparse();

    fclose(fp);

    // Restore
    _errStream = NULL;
    _parsePath = NULL;

    if (_parse == 0) {
        return _object;
    }
    else return NULL;
}
