#version 330

uniform vec4 vColor;

smooth out vec4 vFragColor;

void main(void)
{
    vFragColor = vColor;
}