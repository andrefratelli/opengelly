/******************************/
/*      Gouraud shader        */
/*      Fragment Shader       */
/*      CG, FCUP, 2011        */
/******************************/

#version 330

//ColorMap
uniform sampler2D colorMap;

//INPUT
in vec4 vVaryingColor;
smooth in vec2 vVaryingTexCoords;

//OUTPUT
out vec4 vFragColor;


void main(void) 
{
  vFragColor = texture(colorMap, vVaryingTexCoords)*vVaryingColor;
}