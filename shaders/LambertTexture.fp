/******************************/
/*      Lambert shader        */
/*      Fragment Shader       */
/*      CG, FCUP, 2011        */
/******************************/

#version 330

uniform sampler2D colorMap;

in vec4 vVaryingColor;
smooth in vec2 vVaryingTexCoords;

out vec4 vFragColor;

void main(void) 
{
  vFragColor = texture(colorMap, vVaryingTexCoords)*vVaryingColor;
}