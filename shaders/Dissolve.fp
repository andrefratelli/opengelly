/*****************************/
/*      Dissolve Shader      */
/*      Fragment Shader      */
/*      CG, FCUP, 2011       */
/*****************************/

#version 330

smooth in vec3 vVaryingNormal;
smooth in vec2 vVaryingTexCoords;
smooth in vec3 vVaryingLight1Dir;
smooth in vec3 vVaryingLight2Dir;

uniform vec4 vAmbientColor;
uniform vec4 vDiffuseColor;
uniform vec4 vSpecularColor;
uniform vec4 vColor;
uniform sampler2D cloudTexture;
uniform float dissolveFactor;

out vec4 vFragColor;

void main(void)
{
    vFragColor = vAmbientColor;
    
    vec4 vCloudSample = texture(cloudTexture, vVaryingTexCoords);

    if (vCloudSample.r < dissolveFactor)
        discard;

    float diff = max(0.0, dot(normalize(vVaryingNormal), normalize(vVaryingLight1Dir)));
    vFragColor += diff * vDiffuseColor;
    vec3 vReflection = normalize(reflect(-normalize(vVaryingLight1Dir), normalize(vVaryingNormal)));
    float spec = max(0.0, dot(normalize(vVaryingNormal), vReflection));
    if (diff != 0) {
        float fSpec = pow(spec, 128.0);
        vFragColor.rgb += vec3(fSpec, fSpec, fSpec);
    }

    diff = max(0.0, dot(normalize(vVaryingNormal), normalize(vVaryingLight2Dir)));
    vFragColor += diff * vDiffuseColor;
    vReflection = normalize(reflect(-normalize(vVaryingLight2Dir), normalize(vVaryingNormal)));
    spec = max(0.0, dot(normalize(vVaryingNormal), vReflection));
    if (diff != 0) {
        float fSpec = pow(spec, 128.0);
        vFragColor.rgb += vec3(fSpec, fSpec, fSpec);
    }

    vFragColor = vFragColor*vColor;
}