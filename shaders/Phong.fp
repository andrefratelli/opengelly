/****************************/
/*       Phong shader       */
/*      Fragment Shader     */
/*      CG, FCUP, 2011      */
/****************************/

#version 330

smooth in vec3 vVaryingNormal;
smooth in vec3 vVaryingLightDir1;
smooth in vec3 vVaryingLightDir2;

out vec4 vFragColor;

uniform vec4 vColor;
uniform vec4 vDiffuseColor;
uniform vec4 vAmbientColor;
uniform vec4 vSpecularColor;

uniform bool vPointLight1On;
uniform bool vPointLight2On;

void main(void)
{
    vFragColor = vAmbientColor;

    if (vPointLight1On)
    {
        float diff = max(0.0, dot(vVaryingNormal, vVaryingLightDir1));
        
        vFragColor += diff * vDiffuseColor;

        vec3 vReflection = normalize(reflect(-vVaryingLightDir1, vVaryingNormal));
        float spec = max(0.0, dot(vVaryingNormal, vReflection));
        
        if (diff != 0)
        {
            float fSpec = pow(spec, 128.0);
            vFragColor.rgb += vec3(fSpec, fSpec, fSpec);
        }
    }
    if (vPointLight2On)
    {
        float diff = max(0.0, dot(vVaryingNormal, vVaryingLightDir2));

        vFragColor += diff * vDiffuseColor;

        vec3 vReflection = normalize(reflect(-vVaryingLightDir2, vVaryingNormal));
        float spec = max(0.0, dot(vVaryingNormal, vReflection));

        if (diff != 0)
        {
            float fSpec = pow(spec, 128.0);
            vFragColor.rgb += vec3(fSpec, fSpec, fSpec);
        }
    }
    vFragColor = vColor * vFragColor;
}