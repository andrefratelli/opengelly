/******************************/
/*      Phong shader      	  */
/*      Fragment Shader       */
/*      CG, FCUP, 2011        */
/******************************/

#version 330

//INPUT
smooth in vec3 vVaryingNormal;
smooth in vec2 vVaryingTexCoords;
smooth in vec3 vVaryingLightDir1;
smooth in vec3 vVaryingLightDir2;

//OUTPUT
out vec4 vFragColor;

//UNIFORMS
uniform vec4 vDiffuseColor;
uniform vec4 vSpecularColor;
uniform vec4 vAmbientColor;
uniform sampler2D colorMap;

uniform bool vPointLight1On;
uniform bool vPointLight2On;

void main(void) 
{
	//Default Color
	vFragColor = vAmbientColor;

	//If The Point Light 1 is ON
	if (vPointLight1On) {
		//Diffuse itensity
		float diff = max(0.0, dot(vVaryingNormal, vVaryingLightDir1));
		
		//Multiply itensity by diffuse color, force alpha to 1.0
		vFragColor += diff * vDiffuseColor;
	  
		//Specular Light
		vec3 vReflection = normalize(reflect(-vVaryingLightDir1, vVaryingNormal));
		float spec = max(0.0, dot(vVaryingNormal, vReflection));
	  
		//Don't apply specular color if there is no diffuse light
		if (diff != 0) {
			float fSpec = pow(spec, 128.0);
			vFragColor.rgb += vec3(fSpec, fSpec, fSpec);
		}
	}
	//If The Point Light 2 is ON
	if (vPointLight2On) {
		//Diffuse itensity
		float diff = max(0.0, dot(vVaryingNormal, vVaryingLightDir2));
		
		//Multiply itensity by diffuse color, force alpha to 1.0
		vFragColor += diff * vDiffuseColor;
	  
		//Specular Light
		vec3 vReflection = normalize(reflect(-vVaryingLightDir2, vVaryingNormal));
		float spec = max(0.0, dot(vVaryingNormal, vReflection));
	  
		//Don't apply specular color if there is no diffuse light
		if (diff != 0) {
			float fSpec = pow(spec, 128.0);
			vFragColor.rgb += vec3(fSpec, fSpec, fSpec);
		}
	} 
	vFragColor = texture(colorMap, vVaryingTexCoords)*vFragColor;
}

