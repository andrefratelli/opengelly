/****************************/
/*        Toon shader       */
/*      Fragment Shader     */
/*      CG, FCUP, 2011      */
/****************************/

#version 330

uniform vec4 vAmbientColor;
uniform vec4 vColor;
uniform bool vPointLight1On;
uniform bool vPointLight2On;

out vec4 vFragColor;

smooth in float diffuseLight1;
smooth in float diffuseLight2;

void main(void)
{
    vFragColor = vAmbientColor;

    if (vPointLight1On)
    {
        if (diffuseLight1 > 0.95)
            vFragColor += vec4(1.0, 1.0, 1.0, 1.0);
        else if (diffuseLight1 > 0.5)
            vFragColor += vec4(0.7, 0.7, 0.7, 1.0);
        else if (diffuseLight1 > 0.25)
            vFragColor += vec4(0.4, 0.4, 0.4, 1.0);
        else if (diffuseLight1 > 0.0)
            vFragColor += vec4(0.2, 0.2, 0.2, 1.0);
    }
    if (vPointLight2On)
    {
        if (diffuseLight2 > 0.95)
            vFragColor += vec4(1.0, 1.0, 1.0, 1.0);
        else if (diffuseLight2 > 0.5)
            vFragColor += vec4(0.7, 0.7, 0.7, 1.0);
        else if (diffuseLight2 > 0.25)
            vFragColor += vec4(0.4, 0.4, 0.4, 1.0);
        else if (diffuseLight2 > 0.0)
            vFragColor += vec4(0.2, 0.2, 0.2, 1.0);
    }

    vFragColor = vColor * vFragColor;
}