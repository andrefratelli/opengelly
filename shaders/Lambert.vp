/******************************/
/*      Lambert shader       */
/*      Vertex Shader         */
/*      CG, FCUP, 2011        */
/******************************/

#version 330

in vec4 vPosition;
in vec3 vNormal;

uniform mat4 mvMatrix;
uniform mat4 mvpMatrix;

uniform vec4 vPointLight1Position;
uniform bool vPointLight1On;
uniform vec4 vPointLight2Position;
uniform bool vPointLight2On;

uniform vec4 vAmbientColor;
uniform vec4 vDiffuseColor;
uniform vec4 vSpecularColor;

smooth out vec4 vVaryingColor;

void main(void)
{
    mat3 normalMatrix;
    normalMatrix[0] = mvMatrix[0].xyz;
    normalMatrix[1] = mvMatrix[1].xyz;
    normalMatrix[2] = mvMatrix[2].xyz;
    
    vVaryingColor = vAmbientColor;
    vec3 vEyeNormal = normalize(normalMatrix * vNormal);

    vec4 vPosition4 = mvMatrix * vPosition;
    vec3 vPosition3 = vPosition.xyz / vPosition.w;

    if (vPointLight1On)
    {
        vec3 vLightVector3 = normalize(vPointLight1Position.xyz - vPosition3);
        float diff = max(0.0, dot(vEyeNormal, vLightVector3));
        vVaryingColor.rgb += diff * vDiffuseColor.rgb;
        vVaryingColor.a = 1.0;
    }

    if (vPointLight2On)
    {
        vec3 vLightVector3 = normalize(vPointLight2Position.xyz - vPosition3);
        float diff = max(0.0, dot(vEyeNormal, vLightVector3));
        vVaryingColor.rgb += diff * vDiffuseColor.rgb;
        vVaryingColor.a = 1.0;
    }


    gl_Position = mvpMatrix * vPosition;
}