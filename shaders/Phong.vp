/****************************/
/*       Phong shader       */
/*      Vertex Shader       */
/*      CG, FCUP, 2011      */
/****************************/

#version 330

in vec4 vPosition;
in vec3 vNormal;

uniform mat4 mvMatrix;
uniform mat4 mvpMatrix;

uniform vec4 vPointLight1Position;
uniform vec4 vPointLight2Position;

smooth out vec3 vVaryingNormal;
smooth out vec3 vVaryingLightDir1;
smooth out vec3 vVaryingLightDir2;

void main(void)
{
    mat3 normalMatrix;
    normalMatrix[0] = mvMatrix[0].xyz;
    normalMatrix[1] = mvMatrix[1].xyz;
    normalMatrix[2] = mvMatrix[2].xyz;

    vVaryingNormal = normalize(normalMatrix * vNormal);

    vec4 vPosition4 = mvMatrix * vPosition;
    vec3 vPosition3 = vPosition4.xyz / vPosition4.w;

    vVaryingLightDir1 = normalize(vPointLight1Position.xyz - vPosition3);
    vVaryingLightDir2 = normalize(vPointLight2Position.xyz - vPosition3);

    gl_Position = mvpMatrix * vPosition;
}