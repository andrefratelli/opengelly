/*****************************/
/*      Gouraud shader       */
/*      Fragment Shader      */
/*      CG, FCUP, 2011       */
/*****************************/

#version 330

in vec4 vVaryingColor;

out vec4 vFragColor;

uniform vec4 vColor;

void main(void)
{
    vFragColor = vColor * vVaryingColor;
}