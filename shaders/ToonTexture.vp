/******************************/
/*       Toon shader          */
/*      Vertex Shader         */
/*      CG, FCUP, 2011        */
/******************************/

#version 330

// Incoming per vertex... position, normal, texture
in vec4 vPosition;
in vec3 vNormal;
in vec2 vTexCoords;

uniform vec3	vPointLight1Position;
uniform vec3	vPointLight2Position;

uniform mat4	mvpMatrix;
uniform mat4	mvMatrix;
uniform mat3	normalMatrix;

smooth out float diffuseLight1;
smooth out float diffuseLight2;
smooth out vec2 vVaryingTexCoords;

void main(void) 
{ 
	//Texture Coordinates
	vVaryingTexCoords = vTexCoords;

    // Get surface normal in eye coordinates
    vec3 vEyeNormal = normalize(normalMatrix * vNormal);

    // Get vertex position in eye coordinates
    vec4 vPosition4 = mvMatrix * vPosition;
    vec3 vPosition3 = vPosition4.xyz / vPosition4.w;

    // Get vector to light sources
    vec3 vLightDir1 = normalize(vPointLight1Position - vPosition3);
    vec3 vLightDir2 = normalize(vPointLight2Position - vPosition3);

    // Dot product gives us diffuse intensity
    diffuseLight1 = max(0.0, dot(vLightDir1, vEyeNormal));
    diffuseLight2 = max(0.0, dot(vLightDir2, vEyeNormal));

    // Don't forget to transform the geometry!
    gl_Position = mvpMatrix * vPosition;
}