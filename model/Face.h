#ifndef FACE_H
#define	FACE_H

#include <vector>
#include <model/FacePoint.h>

namespace eng
{
    class Face
    {
    public:
        typedef std::vector<FacePoint*> facepoints_vpt;
        typedef M3DVector3f M3DTriangle[3];
        typedef size_t index_type;
        
        Face(facepoints_vpt facepoints);
        
        facepoints_vpt& facepoints();
        
        M3DTriangle& extractCornerInfo(const M3DMatrix44f mMatrix, index_type index);
        bool getAverage(M3DVector3f vAvg, index_type index);
        
    private:
        facepoints_vpt _facepoints;
        M3DTriangle _vertices;
        M3DVector3f _vAvg;
        size_t _index;
        size_t _TransIndex;
    };
}


#endif	/* FACE_H */

