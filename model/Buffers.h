#ifndef BUFFERS_H
#define	BUFFERS_H

#include <vector>
#include <shared.h>

namespace eng
{
    class Buffers
    {
    public:
        typedef std::vector<GLfloat> buffer_vt;
        
        Buffers();
        
        buffer_vt& vertexes();
        buffer_vt& textures();
        buffer_vt& normals();
        
    private:
        buffer_vt _vertexes;
        buffer_vt _textures;
        buffer_vt _normals;
    };
}

#endif	/* BUFFERS_H */

