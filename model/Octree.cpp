#include <model/Octree.h>
#include <shared.h>
#include <phy/CollisionDetection.h>
#include <iostream>

using namespace eng;
using namespace std;

Octree::Octree(M3DVector2f vX, M3DVector2f vY, M3DVector2f vZ) :
    _bLeaf(false),
    _bComputed(false),
    _face_count(0),
    _transIndex(0),
    _index(0)
{
    m3dCopyVector2(_vX, vX);
    m3dCopyVector2(_vY, vY);
    m3dCopyVector2(_vZ, vZ);
}

Octree::Octree(GLfloat minx, GLfloat maxx, GLfloat miny, GLfloat maxy, GLfloat minz, GLfloat maxz) :
    _bLeaf(false),
    _bComputed(false),
    _face_count(0),
    _transIndex(0),
    _index(0)
{
    m3dLoadVector2(_vX, minx, maxx);
    m3dLoadVector2(_vY, miny, maxy);
    m3dLoadVector2(_vZ, minz, maxz);
}

void Octree::computeOctree(size_t depth, faces_vpt& faces)
{
    buildTree(depth);
    
    faces_vpt::iterator it = faces.begin();
    faces_vpt::iterator end = faces.end();
    
    while (it != end)
    {
        associate(*it);
        it++;
    }
    
    cutTree();
}

bool Octree::isLeaf() { return _bLeaf; }
bool Octree::computed() const { return _bComputed; }
size_t Octree::getFaceCount() const { return _face_count; }
Octree** Octree::children() { return _vChildren; }
Octree::faces_vpt& Octree::getFaces() { return _faces; }

Octree::M3DCornerVector& Octree::extractCornerInfo(const M3DMatrix44f mMatrix, Face::index_type index)
{
    if (index != _transIndex)
    {
        M3DVector3f vAux[8];
        
        m3dLoadVector3(vAux[0], _vX[0], _vY[0], _vZ[0]);
        m3dLoadVector3(vAux[1], _vX[1], _vY[0], _vZ[0]);
        m3dLoadVector3(vAux[2], _vX[1], _vY[1], _vZ[0]);
        m3dLoadVector3(vAux[3], _vX[0], _vY[1], _vZ[0]);

        m3dLoadVector3(vAux[4], _vX[0], _vY[1], _vZ[1]);
        m3dLoadVector3(vAux[5], _vX[0], _vY[0], _vZ[1]);
        m3dLoadVector3(vAux[6], _vX[1], _vY[0], _vZ[1]);
        m3dLoadVector3(vAux[7], _vX[1], _vY[1], _vZ[1]);
        
        for (size_t point=0 ; point<8 ; point++)
        {
            GLfloat scale = mMatrix[15];
            
            m3dTransformVector3(_transPoints[point], vAux[point], mMatrix);
            
            _transPoints[point][0] /= scale;
            _transPoints[point][1] /= scale;
            _transPoints[point][2] /= scale;
        }
        
        m3dCopyVector3(_transFaces[0][0], _transPoints[0]);
        m3dCopyVector3(_transFaces[0][1], _transPoints[3]);
        m3dCopyVector3(_transFaces[0][2], _transPoints[2]);

        m3dCopyVector3(_transFaces[1][0], _transPoints[7]);
        m3dCopyVector3(_transFaces[1][1], _transPoints[4]);
        m3dCopyVector3(_transFaces[1][2], _transPoints[5]);

        m3dCopyVector3(_transFaces[2][0], _transPoints[3]);
        m3dCopyVector3(_transFaces[2][1], _transPoints[4]);
        m3dCopyVector3(_transFaces[2][2], _transPoints[7]);

        m3dCopyVector3(_transFaces[3][0], _transPoints[0]);
        m3dCopyVector3(_transFaces[3][1], _transPoints[1]);
        m3dCopyVector3(_transFaces[3][2], _transPoints[6]);

        m3dCopyVector3(_transFaces[4][0], _transPoints[1]);
        m3dCopyVector3(_transFaces[4][1], _transPoints[2]);
        m3dCopyVector3(_transFaces[4][2], _transPoints[7]);

        m3dCopyVector3(_transFaces[5][0], _transPoints[4]);
        m3dCopyVector3(_transFaces[5][1], _transPoints[3]);
        m3dCopyVector3(_transFaces[5][2], _transPoints[0]);
        
        _transIndex = index;
    }
    return _transPoints;
}

Octree::M3DFaceVector& Octree::extractFaceInfo()
{
    return _transFaces;
}

bool Octree::getAverage(M3DVector3f vAvg, Face::index_type index)
{
    if (index != _index)
    {
        m3dCopyVector3(vAvg, _avgNormal);
        
        _index = index;
        return true;
    }
    else
    {
        m3dLoadVector3(vAvg, 0.0f, 0.0f, 0.0f);
        
        return false;
    }
}
M3DVector2f& Octree::x() { return _vX; }
M3DVector2f& Octree::y() { return _vY; }
M3DVector2f& Octree::z() { return _vZ; }

void Octree::x(M3DVector2f vX) { m3dCopyVector2(_vX, vX); }
void Octree::y(M3DVector2f vY) { m3dCopyVector2(_vY, vY); }
void Octree::z(M3DVector2f vZ) { m3dCopyVector2(_vZ, vZ); }

void Octree::buildTree(size_t depth)
{
    if (depth != 0)
    {
        GLfloat mx = (_vX[0] + _vX[1]) / 2.0f;
        GLfloat my = (_vY[0] + _vY[1]) / 2.0f;
        GLfloat mz = (_vZ[0] + _vZ[1]) / 2.0f;

        _vChildren[0] = new Octree(_vX[0], mx, _vY[0], my, _vZ[0], mz);
        _vChildren[1] = new Octree(mx, _vX[1], _vY[0], my, _vZ[0], mz);
        _vChildren[2] = new Octree(mx, _vX[1], _vY[0], my, mz, _vZ[1]);
        _vChildren[3] = new Octree(_vX[0], mx, _vY[0], my, mz, _vZ[1]);

        _vChildren[4] = new Octree(_vX[0], mx, my, _vY[1], _vZ[0], mz);
        _vChildren[5] = new Octree(mx, _vX[1], my, _vY[1], _vZ[0], mz);
        _vChildren[6] = new Octree(mx, _vX[1], my, _vY[1], mz, _vZ[1]);
        _vChildren[7] = new Octree(_vX[0], mx, my, _vY[1], mz, _vZ[1]);

        for (size_t it=0 ; it<QUADRANTS ; it++) { _vChildren[it]->buildTree(depth - 1); }

        _bLeaf = false;
    }
    else
    {
        for (size_t it=0 ; it<QUADRANTS ; it++) { _vChildren[it] = NULL; }
        
        _bLeaf = true;
    }
    
    _bComputed = true;
}

size_t Octree::associate(Face* face)
{
    size_t count = 0;
    
    if (triangleCollides(face, true))
    {
        if (isLeaf())
        {
            Face::facepoints_vpt::iterator it = face->facepoints().begin();
            Face::facepoints_vpt::iterator end = face->facepoints().end();
            bool bInit = false;

            while (it != end)
            {
                if (bInit)
                {
                    _sumNormal[0] += (*it)->normal()->x();
                    _sumNormal[1] += (*it)->normal()->y();
                    _sumNormal[2] += (*it)->normal()->z();
                }
                else
                {
                    _sumNormal[0] = (*it)->normal()->x();
                    _sumNormal[1] = (*it)->normal()->y();
                    _sumNormal[2] = (*it)->normal()->z();

                    bInit = true;
                }
                it++;
            }
            
            _faces.push_back(face);
            _face_count++;
            count = 1;
            
            _avgNormal[0] = _sumNormal[0] / face->facepoints().size();
            _avgNormal[1] = _sumNormal[1] / face->facepoints().size();
            _avgNormal[2] = _sumNormal[2] / face->facepoints().size();
        }
        else
        {
            for (size_t i=0 ; i<QUADRANTS ; i++)
            {
                count += _vChildren[i]->associate(face);
            }
            _face_count += count;
        }
    }
    return count;
}

bool Octree::cutTree()
{
    if (!isLeaf())
    {
        M3DVector2f vX = {_vX[1], _vX[0]};
        M3DVector2f vY = {_vY[1], _vY[0]};
        M3DVector2f vZ = {_vZ[1], _vZ[0]};

        for (size_t it=0 ; it<QUADRANTS ; it++)
        {
            if (_vChildren[it]->cutTree())
            {
                delete _vChildren[it];
                _vChildren[it] = NULL;
            }
            else
            {
                if (_vChildren[it]->_vX[0] < vX[0]) { vX[0] = _vChildren[it]->_vX[0]; }
                if (_vChildren[it]->_vX[1] > vX[1]) { vX[1] = _vChildren[it]->_vX[1]; }
                
                if (_vChildren[it]->_vY[0] < vY[0]) { vY[0] = _vChildren[it]->_vY[0]; }
                if (_vChildren[it]->_vY[1] > vY[1]) { vY[1] = _vChildren[it]->_vY[1]; }
                
                if (_vChildren[it]->_vZ[0] < vZ[0]) { vZ[0] = _vChildren[it]->_vZ[0]; }
                if (_vChildren[it]->_vZ[1] > vZ[1]) { vZ[1] = _vChildren[it]->_vZ[1]; }
            }
        }
        
        m3dCopyVector2(_vX, vX);
        m3dCopyVector2(_vY, vY);
        m3dCopyVector2(_vZ, vZ);
    }
    return _face_count == 0;
}

bool Octree::planeBoxOverlap(M3DVector3f vNormal, GLfloat d, M3DVector3f maxbox) const
{
    M3DVector3f vMin, vMax;
    
    for (size_t q=0 ; q<3 ; q++)
    {
        if(vNormal[q] > 0.0f)
        {
            vMin[q] = -maxbox[q];
            vMax[q] = maxbox[q];
        }
        else
        {
            vMin[q] = maxbox[q];
            vMax[q] = -maxbox[q];
        }
    }
    
    if (m3dDotProduct3(vNormal, vMin) + d > 0.0f) return false;
    if (m3dDotProduct3(vNormal, vMax) + d >= 0.0f) return true;

    return false;
}

bool Octree::boundedCheck(Face* face)
{
    M3DVector2f vX, vY, vZ;
    Face::facepoints_vpt::iterator it = face->facepoints().begin();
    Face::facepoints_vpt::iterator end = face->facepoints().end();
    bool bInit = false;
    
    while (it != end)
    {
        GLfloat x = (*it)->vertex()->x();
        GLfloat y = (*it)->vertex()->y();
        GLfloat z = (*it)->vertex()->z();
        
        if (bInit)
        {
            if (x < vX[0]) { vX[0] = x; }
            if (x > vX[1]) { vX[1] = x; }
            
            if (y < vY[0]) { vY[0] = y; }
            if (y > vY[1]) { vY[1] = y; }
            
            if (z < vZ[0]) { vZ[0] = z; }
            if (z > vZ[1]) { vZ[1] = z; }
        }
        else
        {
            vX[0] = vX[1] = x;
            vY[0] = vY[1] = y;
            vZ[0] = vZ[1] = z;
            
            bInit = true;
        }
        
        it++;
    }
    return CollisionDetection::collide(vX, vY, vZ, _vX, _vY, _vZ, false);
}

bool Octree::triangleCollides(Face* face, bool bJustCheck)
{
#define X 0
#define Y 1
#define Z 2

#define CROSS(dest,v1,v2) \
          dest[0]=v1[1]*v2[2]-v1[2]*v2[1]; \
          dest[1]=v1[2]*v2[0]-v1[0]*v2[2]; \
          dest[2]=v1[0]*v2[1]-v1[1]*v2[0]; 

#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])

#define SUB(dest,v1,v2) \
          dest[0]=v1[0]-v2[0]; \
          dest[1]=v1[1]-v2[1]; \
          dest[2]=v1[2]-v2[2]; 

#define FINDMINMAX(x0,x1,x2,min,max) \
  min = max = x0;   \
  if(x1<min) min=x1;\
  if(x1>max) max=x1;\
  if(x2<min) min=x2;\
  if(x2>max) max=x2;


/*======================== X-tests ========================*/
#define AXISTEST_X01(a, b, fa, fb)			   \
	p0 = a*v0[Y] - b*v0[Z];			       	   \
	p2 = a*v2[Y] - b*v2[Z];			       	   \
        if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
	rad = fa * boxhalfsize[Y] + fb * boxhalfsize[Z];   \
	if(min>rad || max<-rad) return 0;

#define AXISTEST_X2(a, b, fa, fb)			   \
	p0 = a*v0[Y] - b*v0[Z];			           \
	p1 = a*v1[Y] - b*v1[Z];			       	   \
        if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxhalfsize[Y] + fb * boxhalfsize[Z];   \
	if(min>rad || max<-rad) return 0;

/*======================== Y-tests ========================*/
#define AXISTEST_Y02(a, b, fa, fb)			   \
	p0 = -a*v0[X] + b*v0[Z];		      	   \
	p2 = -a*v2[X] + b*v2[Z];	       	       	   \
        if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
	rad = fa * boxhalfsize[X] + fb * boxhalfsize[Z];   \
	if(min>rad || max<-rad) return 0;

#define AXISTEST_Y1(a, b, fa, fb)			   \
	p0 = -a*v0[X] + b*v0[Z];		      	   \
	p1 = -a*v1[X] + b*v1[Z];	     	       	   \
        if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxhalfsize[X] + fb * boxhalfsize[Z];   \
	if(min>rad || max<-rad) return 0;

/*======================== Z-tests ========================*/

#define AXISTEST_Z12(a, b, fa, fb)			   \
	p1 = a*v1[X] - b*v1[Y];			           \
	p2 = a*v2[X] - b*v2[Y];			       	   \
        if(p2<p1) {min=p2; max=p1;} else {min=p1; max=p2;} \
	rad = fa * boxhalfsize[X] + fb * boxhalfsize[Y];   \
	if(min>rad || max<-rad) return 0;

#define AXISTEST_Z0(a, b, fa, fb)			   \
	p0 = a*v0[X] - b*v0[Y];				   \
	p1 = a*v1[X] - b*v1[Y];			           \
        if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
	rad = fa * boxhalfsize[X] + fb * boxhalfsize[Y];   \
	if(min>rad || max<-rad) return 0;

//int triBoxOverlap(float boxcenter[3],float boxhalfsize[3],float triverts[3][3])
//{
    
#define AV(v) ((v[0] + v[1]) / 2.0f)
    Vertex* vert0 = face->facepoints()[0]->vertex();
    Vertex* vert1 = face->facepoints()[1]->vertex();
    Vertex* vert2 = face->facepoints()[2]->vertex();
    
    M3DVector3f boxcenter = {AV(_vX), AV(_vY), AV(_vZ)};
    M3DVector3f boxhalfsize = {_vX[1] - boxcenter[0], _vY[1] - boxcenter[1], _vZ[1] - boxcenter[2]};
    M3DVector3f triverts[3] = {
        {vert0->x(), vert0->y(), vert0->z()},
        {vert1->x(), vert1->y(), vert1->z()},
        {vert2->x(), vert2->y(), vert2->z()}
    };
    
#undef AV

  /*    use separating axis theorem to test overlap between triangle and box */
  /*    need to test for overlap in these directions: */
  /*    1) the {x,y,z}-directions (actually, since we use the AABB of the triangle */
  /*       we do not even need to test these) */
  /*    2) normal of the triangle */
  /*    3) crossproduct(edge from tri, {x,y,z}-directin) */
  /*       this gives 3x3=9 more tests */
   GLfloat v0[3],v1[3],v2[3];
   GLfloat axis[3];
   GLfloat min,max,d,p0,p1,p2,rad,fex,fey,fez;  
   GLfloat normal[3],e0[3],e1[3],e2[3];

   /* This is the fastest branch on Sun */
   /* move everything so that the boxcenter is in (0,0,0) */
   SUB(v0,triverts[0],boxcenter);
   SUB(v1,triverts[1],boxcenter);
   SUB(v2,triverts[2],boxcenter);

   /* compute triangle edges */
   SUB(e0,v1,v0);      /* tri edge 0 */
   SUB(e1,v2,v1);      /* tri edge 1 */
   SUB(e2,v0,v2);      /* tri edge 2 */

   /* Bullet 3:  */
   /*  test the 9 tests first (this was faster) */
   fex = fabs(e0[X]);
   fey = fabs(e0[Y]);
   fez = fabs(e0[Z]);
   AXISTEST_X01(e0[Z], e0[Y], fez, fey);
   AXISTEST_Y02(e0[Z], e0[X], fez, fex);
   AXISTEST_Z12(e0[Y], e0[X], fey, fex);

   fex = fabs(e1[X]);
   fey = fabs(e1[Y]);
   fez = fabs(e1[Z]);
   AXISTEST_X01(e1[Z], e1[Y], fez, fey);
   AXISTEST_Y02(e1[Z], e1[X], fez, fex);
   AXISTEST_Z0(e1[Y], e1[X], fey, fex);

   fex = fabs(e2[X]);
   fey = fabs(e2[Y]);
   fez = fabs(e2[Z]);
   AXISTEST_X2(e2[Z], e2[Y], fez, fey);
   AXISTEST_Y1(e2[Z], e2[X], fez, fex);
   AXISTEST_Z12(e2[Y], e2[X], fey, fex);

   /* Bullet 1: */
   /*  first test overlap in the {x,y,z}-directions */
   /*  find min, max of the triangle each direction, and test for overlap in */
   /*  that direction -- this is equivalent to testing a minimal AABB around */
   /*  the triangle against the AABB */

   /* test in X-direction */
   FINDMINMAX(v0[X],v1[X],v2[X],min,max);
   if(min>boxhalfsize[X] || max<-boxhalfsize[X]) return 0;

   /* test in Y-direction */
   FINDMINMAX(v0[Y],v1[Y],v2[Y],min,max);
   if(min>boxhalfsize[Y] || max<-boxhalfsize[Y]) return 0;

   /* test in Z-direction */
   FINDMINMAX(v0[Z],v1[Z],v2[Z],min,max);
   if(min>boxhalfsize[Z] || max<-boxhalfsize[Z]) return 0;

   /* Bullet 2: */
   /*  test if the box intersects the plane of the triangle */
   /*  compute plane equation of triangle: normal*x+d=0 */
   CROSS(normal,e0,e1);
   d=-DOT(normal,v0);  /* plane eq: normal.x+d=0 */
   if(!planeBoxOverlap(normal,d,boxhalfsize)) return 0;

   return 1;   /* box and triangle overlaps */
}

bool Octree::triangleCollides(M3DVector2f vX, M3DVector2f vY, M3DVector2f vZ, bool bJustCheck)
{
    return CollisionDetection::collide(vX, vY, vZ, _vX, _vY, _vZ, bJustCheck);
}
