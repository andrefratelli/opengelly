#ifndef BOUNDBOX_H
#define	BOUNDBOX_H

#include <shared.h>
#include <model/Buffers.h>
#include <model/Octree.h>

namespace eng
{
    class BoundBox
    {
    public:
        BoundBox();
        BoundBox(M3DVector2f vX, M3DVector2f vY, M3DVector2f vZ);
        BoundBox(GLfloat minx, GLfloat maxx, GLfloat miny, GLfloat maxy, GLfloat minz, GLfloat maxz);
        BoundBox(const BoundBox& copy);
        
        Buffers::buffer_vt& box();
        
        /*
         * :: WARNING ::
         * 
         * Highly unstable! Never call!
         */
        Octree* octree();
        
        M3DVector2f& x();
        M3DVector2f& y();
        M3DVector2f& z();
        
        void x(M3DVector2f vX);
        void y(M3DVector2f vY);
        void z(M3DVector2f vZ);
        
    private:
        M3DVector2f _vX;
        M3DVector2f _vY;
        M3DVector2f _vZ;
        Buffers::buffer_vt _buffer;
        Octree* _octree;
    };
}

#endif	/* BOUNDBOX_H */

