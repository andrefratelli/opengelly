#include <model/Group.h>

using namespace eng;

Group::Group(const char* name) :
        _faces(faces_vpt()),
        _name(name)
{}

Group::faces_vpt& Group::faces() { return _faces; }
std::string& Group::name() { return _name; }
