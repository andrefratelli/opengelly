#include <model/Object.h>
#include <model/Vertex.h>
#include <model/Texture.h>
#include <model/Normal.h>
#include <model/Face.h>
#include <model/FacePoint.h>
#include <model/Group.h>
#include <model/BoundBox.h>

using namespace eng;

Object::Object() :
        _buffers(Buffers()),
        _vertexes(Object::vertexes_vpt()),
        _textures(Object::textures_vpt()),
        _normals(Object::normals_vpt()),
        _faces(Object::faces_vpt()),
        _groups(Object::groups_vpt()),
        _boundbox(new BoundBox(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f))
/*
 * :: HACK ::
 * 
 * The bound box was added to the project way to late. We would go through a lot
 * of effort to fix the structure so that we could construct the object without
 * passing these parameters to the BB.
 * 
 * This is only called by the parser, so we're safe...
 */
{}

Buffers& Object::buffers() { return _buffers; }
SafePointer<BoundBox> Object::boundbox() { return _boundbox; }

void Object::setLimits(M3DVector2f vX, M3DVector2f vY, M3DVector2f vZ)
{
    boundbox()->x(vX);
    boundbox()->y(vY);
    boundbox()->z(vZ);
}

Object::vertexes_vpt& Object::vertexes() { return _vertexes; }
Object::textures_vpt& Object::textures() { return _textures; }
Object::normals_vpt& Object::normals() { return _normals; }
Object::faces_vpt& Object::faces() { return _faces; }
Object::facepoints_vpt& Object::facepoints() { return _facepoints; }
Object::groups_vpt& Object::groups() { return _groups; }

void Object::setBaseVertices(Buffers::buffer_vt* buffer)
{
    _baseVertices = buffer;
}

Buffers::buffer_vt* Object::getBaseVertices()
{
    return _baseVertices;
}
