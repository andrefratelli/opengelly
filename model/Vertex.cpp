#include <model/Vertex.h>
#include <assert.h>

using namespace eng;

Vertex::Vertex(Buffers::buffer_vt* buffer) :
        _buffer(buffer)
{
    assert(buffer != NULL);
    
    _index = buffer->size() - 3;
}

GLfloat& Vertex::x() { return (*_buffer)[_index + 0]; }
GLfloat& Vertex::y() { return (*_buffer)[_index + 1]; }
GLfloat& Vertex::z() { return (*_buffer)[_index + 2]; }
