#ifndef GROUP_H
#define	GROUP_H

#include <model/Face.h>
#include <vector>
#include <string>

namespace eng
{
    class Group
    {
    public:
        typedef std::vector<Face*> faces_vpt;
        
        Group(const char* name);
        
        faces_vpt& faces();
        std::string& name();
        
    private:
        faces_vpt _faces;
        std::string _name;
    };
}

#endif	/* GROUP_H */

