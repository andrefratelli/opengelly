#ifndef OBJECT_H
#define	OBJECT_H

#include <vector>
#include <model/Vertex.h>
#include <model/Texture.h>
#include <model/Normal.h>
#include <model/Face.h>
#include <model/FacePoint.h>
#include <model/Group.h>
#include <model/BoundBox.h>
#include <utils/SafePointer.h>

namespace eng
{
    class Object
    {
    public:
        typedef std::vector<Vertex*> vertexes_vpt;
        typedef std::vector<Texture*> textures_vpt;
        typedef std::vector<Normal*> normals_vpt;
        typedef std::vector<Face*> faces_vpt;
        typedef std::vector<FacePoint*> facepoints_vpt;
        typedef std::vector<Group*> groups_vpt;
        
        Object();
        
        Buffers& buffers();
        SafePointer<BoundBox> boundbox();
        
        // :: WARNING :: never use!
        void setLimits(M3DVector2f x, M3DVector2f y, M3DVector2f z);
        
        vertexes_vpt& vertexes();
        textures_vpt& textures();
        normals_vpt& normals();
        faces_vpt& faces();
        facepoints_vpt& facepoints();
        groups_vpt& groups();
        
        void setBaseVertices(Buffers::buffer_vt* buffer);
        Buffers::buffer_vt* getBaseVertices();
        
        
    private:
        Buffers _buffers;
        SafePointer<BoundBox> _boundbox;
        vertexes_vpt _vertexes;
        textures_vpt _textures;
        normals_vpt _normals;
        faces_vpt _faces;
        facepoints_vpt _facepoints;
        groups_vpt _groups;
        Buffers::buffer_vt* _baseVertices;
    };
}

#endif	/* OBJECT_H */
