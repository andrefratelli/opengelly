#ifndef FACEPOINT_H
#define	FACEPOINT_H

#include <model/Vertex.h>
#include <model/Texture.h>
#include <model/Normal.h>
#include <vector>

namespace eng
{
    class FacePoint
    {
    public:
        FacePoint(
                std::vector<Vertex*>* vertexes,
                std::vector<Texture*>* textures,
                std::vector<Normal*>* normals
        );
//        FacePoint(Vertex* vertex, Texture* texture, Normal* normal);
        
        Vertex* vertex();
        Texture* texture();
        Normal* normal();
        
    private:
        std::vector<Vertex*>* _vertexes;
        std::vector<Texture*>* _textures;
        std::vector<Normal*>* _normals;
        
        std::vector<Vertex*>::size_type _vertex;
        std::vector<Texture*>::size_type _texture;
        std::vector<Normal*>::size_type _normal;
        
//        Vertex* _vertex;
//        Texture* _texture;
//        Normal* _normal;
    };
}

#endif	/* FACEPOINT_H */

