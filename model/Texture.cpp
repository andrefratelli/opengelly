#include <model/Texture.h>
#include <assert.h>

using namespace eng;

Texture::Texture(Buffers::buffer_vt* buffer) :
        _buffer(buffer)
{
    assert(buffer != NULL);
    
    _index = buffer->size() - 2;
}

GLfloat& Texture::u() { return (*_buffer)[_index + 0]; }
GLfloat& Texture::v() { return (*_buffer)[_index + 1]; }
