#include <model/BoundBox.h>
#include <model/Buffers.h>

using namespace eng;

#define NVERTS (14 * 3)

BoundBox::BoundBox() :
    _buffer(NVERTS),
    _octree(NULL)
{
    m3dLoadVector2(_vX, 0.0f, 0.0f);
    m3dLoadVector2(_vY, 0.0f, 0.0f);
    m3dLoadVector2(_vZ, 0.0f, 0.0f);
}

BoundBox::BoundBox(M3DVector2f vX, M3DVector2f vY, M3DVector2f vZ) :
    _buffer(NVERTS),
    _octree(NULL)
{
    m3dCopyVector2(_vX, vX);
    m3dCopyVector2(_vY, vY);
    m3dCopyVector2(_vZ, vZ);
}

BoundBox::BoundBox(GLfloat minx, GLfloat maxx, GLfloat miny, GLfloat maxy, GLfloat minz, GLfloat maxz) :
    _buffer(NVERTS),
    _octree(NULL)
{
    m3dLoadVector2(_vX, minx, maxx);
    m3dLoadVector2(_vY, miny, maxy);
    m3dLoadVector2(_vZ, minz, maxz);
}

BoundBox::BoundBox(const BoundBox& copy) :
    _buffer(copy._buffer),
    _octree(copy._octree)
{
    m3dCopyVector2(_vX, copy._vX);
    m3dCopyVector2(_vY, copy._vY);
    m3dCopyVector2(_vZ, copy._vZ);
}

Buffers::buffer_vt& BoundBox::box()
{
    // Bound box
    //     [3] +-------+ [2]
    //        /       /|
    //   [0] +-------+ | [1]
    //       |       | |
    //       |       | |
    //   [7] |       | + [6]
    //   [4] +-------+/  [5]
    _buffer[0] = _vX[0]; _buffer[1] = _vY[1]; _buffer[2] = _vZ[0];         // [0]
    _buffer[3] = _vX[1]; _buffer[4] = _vY[1]; _buffer[5] = _vZ[0];         // [1]
    _buffer[6] = _vX[0]; _buffer[7] = _vY[0]; _buffer[8] = _vZ[0];         // [4]
    _buffer[9] = _vX[1]; _buffer[10] = _vY[0]; _buffer[11] = _vZ[0];       // [5]
    _buffer[12] = _vX[1]; _buffer[13] = _vY[0]; _buffer[14] = _vZ[1];      // [6]
    _buffer[15] = _vX[1]; _buffer[16] = _vY[1]; _buffer[17] = _vZ[0];      // [1]
    _buffer[18] = _vX[1]; _buffer[19] = _vY[1]; _buffer[20] = _vZ[1];      // [2]
    _buffer[21] = _vX[0]; _buffer[22] = _vY[1]; _buffer[23] = _vZ[0];      // [0]
    _buffer[24] = _vX[0]; _buffer[25] = _vY[1]; _buffer[26] = _vZ[1];      // [3]
    _buffer[27] = _vX[0]; _buffer[28] = _vY[0]; _buffer[29] = _vZ[0];      // [4]
    _buffer[30] = _vX[0]; _buffer[31] = _vY[0]; _buffer[32] = _vZ[1];      // [7]
    _buffer[33] = _vX[1]; _buffer[34] = _vY[0]; _buffer[35] = _vZ[1];      // [6]
    _buffer[36] = _vX[0]; _buffer[37] = _vY[1]; _buffer[38] = _vZ[1];      // [3]
    _buffer[39] = _vX[1]; _buffer[40] = _vY[1]; _buffer[41] = _vZ[1];      // [2]
    
    return _buffer;
}

M3DVector2f& BoundBox::x() { return _vX; }
M3DVector2f& BoundBox::y() { return _vY; }
M3DVector2f& BoundBox::z() { return _vZ; }

void BoundBox::x(M3DVector2f vX) { m3dCopyVector2(_vX, vX); }
void BoundBox::y(M3DVector2f vY) { m3dCopyVector2(_vY, vY); }
void BoundBox::z(M3DVector2f vZ) { m3dCopyVector2(_vZ, vZ); }

Octree* BoundBox::octree()
{
    if (_octree == NULL)
    {
        _octree = new Octree(_vX, _vY, _vZ);
    }
    return _octree;
}
