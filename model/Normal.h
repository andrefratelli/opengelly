#ifndef NORMAL_H
#define	NORMAL_H

#include <shared.h>
#include <model/Buffers.h>

namespace eng
{
    class Normal
    {
    public:
        Normal(Buffers::buffer_vt* buffer);
        
        GLfloat& x();
        GLfloat& y();
        GLfloat& z();
        
    private:
        Buffers::buffer_vt* _buffer;
        Buffers::buffer_vt::size_type _index;
    };
}

#endif	/* NORMAL_H */

