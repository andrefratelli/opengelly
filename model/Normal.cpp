#include <model/Normal.h>
#include <shared.h>
#include <assert.h>

using namespace eng;

Normal::Normal(Buffers::buffer_vt* buffer) :
        _buffer(buffer)
{
    assert(buffer != NULL);
    
    _index = _buffer->size() - 3;
}

GLfloat& Normal::x() { return (*_buffer)[_index + 0]; }
GLfloat& Normal::y() { return (*_buffer)[_index + 1]; }
GLfloat& Normal::z() { return (*_buffer)[_index + 2]; }
