#include <model/FacePoint.h>
#include <vector>
#include <model/Vertex.h>
#include <model/Texture.h>
#include <model/Normal.h>

using namespace eng;

FacePoint::FacePoint(
        std::vector<Vertex*>* vertexes,
        std::vector<Texture*>* textures,
        std::vector<Normal*>* normals
) :
        _vertexes(vertexes),
        _textures(textures),
        _normals(normals)
{
    if (vertexes != NULL) { _vertex = vertexes->size() - 1; }
    if (textures != NULL) { _texture = textures->size() - 1; }
    if (normals != NULL) { _normal = normals->size() - 1; }
}

//FacePoint::FacePoint(Vertex* vertex, Texture* texture, Normal* normal) :
//        _vertex(vertex),
//        _texture(texture),
//        _normal(normal)
//{}

Vertex* FacePoint::vertex() { return _vertexes != NULL ? (*_vertexes)[_vertex] : NULL; }
Texture* FacePoint::texture() { return _textures != NULL ? (*_textures)[_texture] : NULL; }
Normal* FacePoint::normal() { return _normals != NULL ? (*_normals)[_normal] : NULL; }
