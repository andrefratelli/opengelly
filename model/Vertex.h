#ifndef VERTEX_H
#define	VERTEX_H

#include <model/Buffers.h>
#include <shared.h>

namespace eng
{
    class Vertex
    {
    public:
        Vertex(Buffers::buffer_vt* buffer);
        
        GLfloat& x();
        GLfloat& y();
        GLfloat& z();
        
    private:
        Buffers::buffer_vt* _buffer;
        Buffers::buffer_vt::size_type _index;
    };
}

#endif	/* VERTEX_H */

