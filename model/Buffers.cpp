#include <model/Buffers.h>

using namespace eng;

Buffers::Buffers() :
        _vertexes(Buffers::buffer_vt()),
        _textures(Buffers::buffer_vt()),
        _normals(Buffers::buffer_vt())
{}

Buffers::buffer_vt& Buffers::vertexes() { return _vertexes; }
Buffers::buffer_vt& Buffers::textures() { return _textures; }
Buffers::buffer_vt& Buffers::normals() { return _normals; }
