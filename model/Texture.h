#ifndef TEXTURE_H
#define	TEXTURE_H

#include <shared.h>
#include <model/Buffers.h>

namespace eng
{
    class Texture
    {
    public:
        Texture(Buffers::buffer_vt* buffer);
        
        GLfloat& u();
        GLfloat& v();
        
    private:
        Buffers::buffer_vt* _buffer;
        Buffers::buffer_vt::size_type _index;
    };
}

#endif	/* TEXTURE_H */

