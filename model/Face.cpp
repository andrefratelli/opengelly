#include <model/Face.h>
#include <model/FacePoint.h>

using namespace eng;

Face::Face(facepoints_vpt facepoints) :
        _facepoints(facepoints),
        _index(0),
        _TransIndex(0)
{
    if (facepoints.size() != 0)
    {
        facepoints_vpt::iterator it = facepoints.begin();
        facepoints_vpt::iterator end = facepoints.end();

        M3DVector3f vSum = {0.0f, 0.0f, 0.0f};

        while (it != end)
        {
            vSum[0] += (*it)->normal()->x();
            vSum[1] += (*it)->normal()->y();
            vSum[2] += (*it)->normal()->z();

            it++;
        }
        
        m3dLoadVector3(_vAvg, vSum[0] / facepoints.size(), vSum[1] / facepoints.size(), vSum[2] / facepoints.size());
    }
    else { m3dLoadVector3(_vAvg, 0.0f, 0.0f, 0.0f); }
}

Face::facepoints_vpt& Face::facepoints() { return _facepoints; }

Face::M3DTriangle& Face::extractCornerInfo(const M3DMatrix44f mMatrix, index_type index)
{
    if (_TransIndex != index)
    {
        Vertex* vertexes[3] = {facepoints()[0]->vertex(), facepoints()[1]->vertex(), facepoints()[2]->vertex()};
        M3DVector3f vAux;
        
        m3dLoadVector3(vAux, vertexes[0]->x(), vertexes[0]->y(), vertexes[0]->z());
        m3dTransformVector3(_vertices[0], vAux, mMatrix);
        _vertices[0][0] /= mMatrix[15];
        _vertices[0][1] /= mMatrix[15];
        _vertices[0][2] /= mMatrix[15];
        
        m3dLoadVector3(vAux, vertexes[1]->x(), vertexes[1]->y(), vertexes[1]->z());
        m3dTransformVector3(_vertices[1], vAux, mMatrix);
        _vertices[1][0] /= mMatrix[15];
        _vertices[1][1] /= mMatrix[15];
        _vertices[1][2] /= mMatrix[15];
        
        m3dLoadVector3(vAux, vertexes[2]->x(), vertexes[2]->y(), vertexes[2]->z());
        m3dTransformVector3(_vertices[2], vAux, mMatrix);
        _vertices[2][0] /= mMatrix[15];
        _vertices[2][1] /= mMatrix[15];
        _vertices[2][2] /= mMatrix[15];
        
        _TransIndex = index;
    }
    return _vertices;
}

bool Face::getAverage(M3DVector3f vAvg, index_type index)
{   
    if (_index != index)
    {
        m3dCopyVector3(vAvg, _vAvg);
        _index = index;
        return true;
    }
    else
    {
//        m3dLoadVector3(vAvg, 0.0f, 0.0f, 0.0f);
        return false;
    }
}
