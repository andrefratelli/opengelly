#ifndef OCTREE_H
#define	OCTREE_H

#include <model/Face.h>
#include <shared.h>
#include <vector>

namespace eng
{
    class Octree
    {
    public:
        typedef std::vector<Face*> faces_vpt;
//        typedef size_t index_type;
        typedef M3DVector3f M3DCornerVector[8];
        typedef M3DVector3f M3DFaceVector[6][3];
        
        static const int QUADRANTS = 8;
        
        Octree(M3DVector2f vX, M3DVector2f vY, M3DVector2f vZ);
        Octree(GLfloat minx, GLfloat maxx, GLfloat miny, GLfloat maxy, GLfloat minz, GLfloat maxz);
        
        void computeOctree(size_t depth, faces_vpt& faces);
        bool computed() const;
        bool isLeaf();
        size_t getFaceCount() const;
        Octree** children();
        faces_vpt& getFaces();
        /*
         * :: HACK ::
         * 
         * Collision detection is painfully slow. This hack allows us to transform
         * the corners and faces only once per collision check.
         */
        M3DCornerVector& extractCornerInfo(const M3DMatrix44f mMatrix, Face::index_type index);
        M3DFaceVector& extractFaceInfo();
        bool getAverage(M3DVector3f vAvg, Face::index_type index);
        
        M3DVector2f& x();
        M3DVector2f& y();
        M3DVector2f& z();
        
        void x(M3DVector2f vX);
        void y(M3DVector2f vY);
        void z(M3DVector2f vZ);
        
    private:
        Octree* _vChildren[QUADRANTS];
        bool _bLeaf;
        bool _bComputed;
        faces_vpt _faces;
        size_t _face_count;
        
        M3DVector2f _vX;
        M3DVector2f _vY;
        M3DVector2f _vZ;
        M3DVector3f _avgNormal;
        M3DVector3f _sumNormal;
        
        // For stored transformations
        Face::index_type _transIndex;
        M3DCornerVector _transPoints;
        M3DFaceVector _transFaces;
        
        Face::index_type _index;
        
        void buildTree(size_t depth);
        size_t associate(Face* face);
        bool cutTree();
        
        bool triangleCollides(Face* face, bool bJustCheck);
        bool triangleCollides(M3DVector2f vX, M3DVector2f vY, M3DVector2f vZ, bool bJustCheck);
        bool planeBoxOverlap(M3DVector3f vNormal, GLfloat d, M3DVector3f maxbox) const;
        bool triangleIntersects(Face* face) const;
        bool boundedCheck(Face* face);
        
        /*
        
        
        
        bool computedOctree();
        size_t getFaceCount() const;
        
        
       
        
    private:
        void _computeOctree(size_t depth);
        void _computeOctree(Face* face, size_t depth);
        
        size_t _associateFace(Face* face);
        bool _pointCollides(Face* face, bool bJustCheck);
        bool _pointCollides(M3DVector2f vX, M3DVector2f vY, M3DVector2f vZ, bool bJustCheck);
        
        std::vector<Octree*> _children;
        
        M3DVector2f _vX;
        M3DVector2f _vY;
        M3DVector2f _vZ;
        bool _leaf;
        faces_vpt _faces;
        bool _computed;
        size_t _face_count;
       
        
        M3DVector3f _avgNormal;
        M3DVector3f _sumNormal;*/
    };
};

#endif	/* OCTREE_H */