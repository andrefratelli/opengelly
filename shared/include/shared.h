#ifndef SHARED_H
#define	SHARED_H

#include <glew.h>

#ifdef __APPLE__
#include <glut/glut.h>
#else
#define FREEGLUT_STATIC
#include <GL/glut.h>
#endif

#include <GLBatch.h>
#include <GLBatchBase.h>
#include <GLFrame.h>
#include <GLFrustum.h>
#include <GLMatrixStack.h>
#include <GLShaderManager.h>
#include <GLTools.h>
#include <GLTriangleBatch.h>
#include <StopWatch.h>
#include <math3d.h>
#include <GLGeometryTransform.h>

#include <GL/gl.h>

#endif	/* SHARED_H */

