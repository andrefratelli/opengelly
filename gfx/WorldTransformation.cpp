#include <gfx/WorldTransformation.h>

using namespace eng;

void WorldTransformation::rotate(const M3DMatrix44f mRotation)
{
    M3DMatrix44f& mMatrix = getMatrix();
    M3DVector3f vRight;
    const M3DVector3f vUp = {mMatrix[4], mMatrix[5], mMatrix[6]};
    const M3DVector3f vForward = {mMatrix[8], mMatrix[9], mMatrix[10]};
    
    m3dCrossProduct3(vRight, vUp, vForward);
    
    mMatrix[0] = mRotation[0] * vRight[0] + mRotation[4] * vRight[1] + mRotation[8] * vRight[2];
    mMatrix[1] = mRotation[1] * vRight[0] + mRotation[5] * vRight[1] + mRotation[9] * vRight[2];
    mMatrix[2] = mRotation[2] * vRight[0] + mRotation[6] * vRight[1] + mRotation[10] * vRight[2];
    
    mMatrix[4] = mRotation[0] * vUp[0] + mRotation[4] * vUp[1] + mRotation[8] * vUp[2];
    mMatrix[5] = mRotation[1] * vUp[0] + mRotation[5] * vUp[1] + mRotation[9] * vUp[2];
    mMatrix[6] = mRotation[2] * vUp[0] + mRotation[6] * vUp[1] + mRotation[10] * vUp[2];
    
    mMatrix[8] = mRotation[0] * vForward[0] + mRotation[4] * vForward[1] + mRotation[8] * vForward[2];
    mMatrix[9] = mRotation[1] * vForward[0] + mRotation[5] * vForward[1] + mRotation[9] * vForward[2];
    mMatrix[10] = mRotation[2] * vForward[0] + mRotation[6] * vForward[1] + mRotation[10] * vForward[2];
}

WorldTransformation::WorldTransformation(M3DMatrix44f& mMatrix) :
    Transformation(mMatrix)
{}

WorldTransformation::WorldTransformation(const WorldTransformation& copy) :
    Transformation(copy)
{}

void WorldTransformation::translate(GLfloat x, GLfloat y, GLfloat z)
{
    M3DMatrix44f& mMatrix = getMatrix();
    
    // Translate the 4th column
    mMatrix[12] += x;
    mMatrix[13] += y;
    mMatrix[14] += z;
}

void WorldTransformation::translate(const M3DVector3f vTranslation)
{
    translate(vTranslation[0], vTranslation[1], vTranslation[2]);
}

void WorldTransformation::setPosition(GLfloat x, GLfloat y, GLfloat z)
{
    M3DMatrix44f& mMatrix = getMatrix();
    
    // Set the 4th column
    mMatrix[12] = x;
    mMatrix[13] = y;
    mMatrix[14] = z;
}

void WorldTransformation::setPosition(const M3DVector3f vPosition)
{
    setPosition(vPosition[0], vPosition[1], vPosition[2]);
}

void WorldTransformation::rotate(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z)
{
    M3DMatrix44f mRotation;

    m3dRotationMatrix44(mRotation, fAngle, x, y, z);
    
    rotate(mRotation);
}

void WorldTransformation::rotate(GLfloat fAngle, const M3DVector3f vAxis)
{
    rotate(fAngle, vAxis[0], vAxis[1], vAxis[2]);
}

void WorldTransformation::setRotation(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z)
{
    M3DMatrix44f& mMatrix = getMatrix();
    M3DMatrix44f mRotation;
    
    m3dRotationMatrix44(mRotation, fAngle, x, y, z);

    mMatrix[0] = mRotation[0];
    mMatrix[1] = mRotation[1];
    mMatrix[2] = mRotation[2];
    
    mMatrix[4] = mRotation[4];
    mMatrix[5] = mRotation[5];
    mMatrix[6] = mRotation[6];
    
    mMatrix[8] = mRotation[8];
    mMatrix[9] = mRotation[9];
    mMatrix[10] = mRotation[10];
}

void WorldTransformation::setRotation(GLfloat fAngle, const M3DVector3f vAxis)
{
    setRotation(fAngle, vAxis[0], vAxis[1], vAxis[2]);
}

void WorldTransformation::scale(GLfloat fScale)
{
    getMatrix()[15] += 1.0f / fScale;
}

void WorldTransformation::setScale(GLfloat fScale)
{
    getMatrix()[15] = 1.0f / fScale;
}
