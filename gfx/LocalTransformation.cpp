#include <gfx/LocalTransformation.h>

using namespace eng;

void LocalTransformation::toWorldRotation(M3DVector3f vWorld, GLfloat x, GLfloat y, GLfloat z)
{
    M3DMatrix44f& mMatrix = getMatrix();
    
    // To world coordinates
    vWorld[0] = mMatrix[0] * x + mMatrix[4] * y + mMatrix[8] * z;
    vWorld[1] = mMatrix[1] * x + mMatrix[5] * y + mMatrix[9] * z;
    vWorld[2] = mMatrix[2] * x + mMatrix[6] * y + mMatrix[10] * z;
}

void LocalTransformation::toWorldRotation(M3DVector3f vWorld, M3DVector3f vLocal)
{
    toWorldRotation(vWorld, vLocal[0], vLocal[1], vLocal[2]);
}

void LocalTransformation::rotate(const M3DMatrix44f mRotation)
{
    M3DMatrix44f& mMatrix = getMatrix();
    M3DVector3f vRight;
    const M3DVector3f vUp = {mMatrix[4], mMatrix[5], mMatrix[6]};
    const M3DVector3f vForward = {mMatrix[8], mMatrix[9], mMatrix[10]};
    
    m3dCrossProduct3(vRight, vUp, vForward);
    
    mMatrix[0] = mRotation[0] * vRight[0] + mRotation[4] * vRight[1] + mRotation[8] * vRight[2];
    mMatrix[1] = mRotation[1] * vRight[0] + mRotation[5] * vRight[1] + mRotation[9] * vRight[2];
    mMatrix[2] = mRotation[2] * vRight[0] + mRotation[6] * vRight[1] + mRotation[10] * vRight[2];
    
    mMatrix[4] = mRotation[0] * vUp[0] + mRotation[4] * vUp[1] + mRotation[8] * vUp[2];
    mMatrix[5] = mRotation[1] * vUp[0] + mRotation[5] * vUp[1] + mRotation[9] * vUp[2];
    mMatrix[6] = mRotation[2] * vUp[0] + mRotation[6] * vUp[1] + mRotation[10] * vUp[2];
    
    mMatrix[8] = mRotation[0] * vForward[0] + mRotation[4] * vForward[1] + mRotation[8] * vForward[2];
    mMatrix[9] = mRotation[1] * vForward[0] + mRotation[5] * vForward[1] + mRotation[9] * vForward[2];
    mMatrix[10] = mRotation[2] * vForward[0] + mRotation[6] * vForward[1] + mRotation[10] * vForward[2];
}

LocalTransformation::LocalTransformation(M3DMatrix44f& mMatrix) :
    Transformation(mMatrix)
{}

LocalTransformation::LocalTransformation(const LocalTransformation& copy) :
    Transformation(copy)
{}

void LocalTransformation::translate(GLfloat x, GLfloat y, GLfloat z)
{
    M3DMatrix44f& mMatrix = getMatrix();
    const M3DVector3f vUp = {mMatrix[4], mMatrix[5], mMatrix[6]};
    const M3DVector3f vForward = {mMatrix[8], mMatrix[9], mMatrix[10]};
    M3DVector3f vRight;
    
    m3dCrossProduct3(vRight, vUp, vForward);
    
    mMatrix[12] += vRight[0] * x + vUp[0] * y + vForward[0] * z;
    mMatrix[13] += vRight[1] * x + vUp[1] * y + vForward[1] * z;
    mMatrix[14] += vRight[2] * x + vUp[2] * y + vForward[2] * z;
}

void LocalTransformation::translate(const M3DVector3f vTranslation)
{
    translate(vTranslation[0], vTranslation[1], vTranslation[2]);
}

void LocalTransformation::setPosition(GLfloat x, GLfloat y, GLfloat z)
{
    M3DMatrix44f& mMatrix = getMatrix();
    const M3DVector3f vUp = {mMatrix[4], mMatrix[5], mMatrix[6]};
    const M3DVector3f vForward = {mMatrix[8], mMatrix[9], mMatrix[10]};
    M3DVector3f vRight;
    
    m3dCrossProduct3(vRight, vUp, vForward);
    
    mMatrix[12] = vRight[0] * x + vUp[0] * y + vForward[0] * z;
    mMatrix[13] = vRight[1] * x + vUp[1] * y + vForward[1] * z;
    mMatrix[14] = vRight[2] * x + vUp[2] * y + vForward[2] * z;
}

void LocalTransformation::setPosition(const M3DVector3f vPosition)
{
    setPosition(vPosition[0], vPosition[1], vPosition[2]);
}

void LocalTransformation::rotate(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z)
{
    M3DVector3f vWorld;
    M3DMatrix44f mRotation;
    
    toWorldRotation(vWorld, x, y, z);

    m3dRotationMatrix44(mRotation, fAngle, vWorld[0], vWorld[1], vWorld[2]);
    
    rotate(mRotation);
}

void LocalTransformation::rotate(GLfloat fAngle, const M3DVector3f vLocal)
{
    rotate(fAngle, vLocal[0], vLocal[1], vLocal[2]);
}

void LocalTransformation::setRotation(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z)
{
    M3DMatrix44f& mMatrix = getMatrix();
    M3DVector3f vWorld;
    M3DMatrix44f mRotation;
    
    toWorldRotation(vWorld, x, y, z);
    
    m3dRotationMatrix44(mRotation, fAngle, vWorld[0], vWorld[1], vWorld[2]);

    mMatrix[0] = mRotation[0];
    mMatrix[1] = mRotation[1];
    mMatrix[2] = mRotation[2];
    
    mMatrix[4] = mRotation[4];
    mMatrix[5] = mRotation[5];
    mMatrix[6] = mRotation[6];
    
    mMatrix[8] = mRotation[8];
    mMatrix[9] = mRotation[9];
    mMatrix[10] = mRotation[10];
}

void LocalTransformation::setRotation(GLfloat fAngle, const M3DVector3f vAxis)
{
    setRotation(fAngle, vAxis[0], vAxis[1], vAxis[2]);
}

void LocalTransformation::scale(GLfloat fScale)
{
    getMatrix()[15] += 1.0f / fScale;
}

void LocalTransformation::setScale(GLfloat fScale)
{
    getMatrix()[15] = 1.0f / fScale;
}
