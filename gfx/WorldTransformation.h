#ifndef WORLDTRANSFORMATION_H
#define	WORLDTRANSFORMATION_H

#include <gfx/Transformation.h>

/*
 * == WorldTransformation ==
 * 
 * This is used to transform objects using world axis.
 */
namespace eng
{
    class WorldTransformation : public Transformation
    {
    private:
        void rotate(const M3DMatrix44f mRotation);
        
    public:
        WorldTransformation(M3DMatrix44f& mMatrix);
        WorldTransformation(const WorldTransformation& copy);
        
        void translate(GLfloat x, GLfloat y, GLfloat z);
        void translate(const M3DVector3f vTranslation);
        
        void setPosition(GLfloat x, GLfloat y, GLfloat z);
        void setPosition(const M3DVector3f vPosition);
        
        void rotate(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z);
        void rotate(GLfloat fAngle, const M3DVector3f vAxis);
        
        void setRotation(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z);
        void setRotation(GLfloat fAngle, const M3DVector3f vAxis);
        
        void scale(GLfloat fScale);
        void setScale(GLfloat fScale);
        
        // TODO
        // void scale(GLfloat fX, GLfloat fY, GLfloat fZ);
        // void scale(const M3DVector3f vScale);

        // void setScale(GLfloat fX, GLfloat fY, GLfloat fZ);
        // void setScale(const M3DVector3f vScale);
    };
}

#endif	/* WORLDTRANSFORMATION_H */

