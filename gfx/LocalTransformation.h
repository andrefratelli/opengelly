#ifndef LOCALTRANSFORMATION_H
#define	LOCALTRANSFORMATION_H

#include <gfx/Transformation.h>

/*
 * == LocalTransformation ==
 * 
 * This is used to transform objects using their own axis, instead of the world.
 */
namespace eng
{
    class LocalTransformation : public Transformation
    {
    private:
        void toWorldRotation(M3DVector3f vWorld, GLfloat x, GLfloat y, GLfloat z);
        void toWorldRotation(M3DVector3f vWorld, M3DVector3f vLocal);
        void rotate(const M3DMatrix44f mRotation);
        
    public:
        LocalTransformation(M3DMatrix44f& mMatrix);
        LocalTransformation(const LocalTransformation& copy);
        
        void translate(GLfloat x, GLfloat y, GLfloat z);
        void translate(const M3DVector3f vTranslation);
        
        void setPosition(GLfloat x, GLfloat y, GLfloat z);
        void setPosition(const M3DVector3f vPosition);
        
        void rotate(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z);
        void rotate(GLfloat fAngle, const M3DVector3f vLocal);
        
        void setRotation(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z);
        void setRotation(GLfloat fAngle, const M3DVector3f vAxis);
        
        void scale(GLfloat fScale);
        void setScale(GLfloat fScale);
        
        // TODO
        // void scale(GLfloat fX, GLfloat fY, GLfloat fZ);
        // void scale(const M3DVector3f vScale);

        // void setScale(GLfloat fX, GLfloat fY, GLfloat fZ);
        // void setScale(const M3DVector3f vScale);
    };
}

#endif	/* LOCALTRANSFORMATION_H */

