#ifndef FRUSTUM_H
#define	FRUSTUM_H

#include <shared.h>

/*
 * == Frustum ==
 * 
 * This is similar to the GLFrustum class from GLTools. The only differences are
 * that it does not work with GLFrame(s) and it skips over some unecessary math
 * and data.
 */
namespace eng
{
    class Frustum
    {
    public:
        Frustum();
        Frustum(GLfloat fFov, GLfloat fAspect, GLfloat fNear, GLfloat fFar);
        Frustum(GLfloat xMin, GLfloat xMax, GLfloat yMin, GLfloat yMax, GLfloat zMin, GLfloat zMax);
        Frustum(M3DVector2f xLim, M3DVector2f yLim, M3DVector2f zLim);
        
        void setOrthographic(GLfloat xMin, GLfloat xMax, GLfloat yMin, GLfloat yMax, GLfloat zMin, GLfloat zMax);
        void setOrthographic(M3DVector2f xLim, M3DVector2f yLim, M3DVector2f zLim);
        
        void setPerspective(GLfloat fFov, GLfloat fAspect, GLfloat fNear, GLfloat fFar);
        
        const M3DMatrix44f& getMatrix();
        void getMatrix(M3DMatrix44f mMatrix);
        
    private:
        
        // Projection matrix
        M3DMatrix44f mProjection;
    };
}

#endif	/* FRUSTUM_H */
