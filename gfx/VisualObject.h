#ifndef VISUALOBJECT_H
#define	VISUALOBJECT_H

#include <gfx/Transformable.h>
#include <mng/VisualResource.h>
#include <mng/TextureResource.h>
#include <mng/ShaderManager.h>
#include <evts/LogicListener.h>
#include <utils/Color.h>
#include <shared.h>
#include <phy/PhyDecls.h>

/*
 * == VisualObject ==
 * 
 * Defines objects to be drawn. It's transformable, colorable, drawable, etc.
 * It also associated a VisualResource with the object.
 */
namespace eng
{
    class VisualObject :
        public Transformable
    {
        friend class ParticleSystem;
        
    public:
        VisualObject(VisualResource resource, size_t octDepth);
        VisualObject(VisualResource resource, size_t octDepth, CUSTOM_SHADER shaderID);
        VisualObject(VisualResource resource, size_t octDepth, Color color);
        VisualObject(VisualResource resource, size_t octDepth, Color color, CUSTOM_SHADER shaderID);
        VisualObject(VisualResource resource, size_t octDepth, TextureResource textureResource);
        VisualObject(VisualResource resource, size_t octDepth, TextureResource textureResource, CUSTOM_SHADER shaderID);
        
        VisualResource resource() const;
        TextureResource textResource() const;
        
        virtual void draw(const M3DVector4f vLightEyePos);
        
        bool getVisible() const;
        void setVisible(bool visible);
        
        Color getColor() const;
        void setColor(const Color color);
        
        GLenum getPolygonMode();
        void setPolygonMode(GLenum mode);
        
        bool getCollisionEnabled();
        void setCollisionEnabled(bool set);
        
        CUSTOM_SHADER getShader(void);
        void setShader(CUSTOM_SHADER shaderID);
        
        bool collides(VisualObject& other, PhyCol shallowness, bool bJustCheck);
        size_t getOctDepth() const;

        void setPhy(bool bPhy);
        bool getPhy() const;
        
        void setBouncing(size_t bBouncing);
        size_t getBouncing() const;
        
        void setStalling(bool bStalling);
        bool getStalling() const;
        
        void getSpeed(M3DVector3f vSpeed);
        
        void setBounce(size_t bBounce);
        size_t getBounce() const;
        
        bool isParticle();
        
        void setCollisionCallback(bool (*callback)(VisualObject& fst, VisualObject& snd));
        
        bool collide(VisualObject& snd);
        
        bool getPhyEnabled() const;
        void setPhyEnabled(bool enabled);
        
    protected:
        void _draw(const M3DVector3f vLightEyePos);
        
    private:
        VisualResource _visualResource;
        TextureResource _textureResource;
        bool _visible;
        Color _color;
        GLenum _polygonMode;
        bool _colliding;
        CUSTOM_SHADER _shaderID;
        size_t _octDepth;
//        bool _bFixed;
        bool _bPhy;
        size_t _bBouncing;
        bool _bStalling;
        size_t _bBounce;
        bool (*_collisionCallback)(VisualObject& fst, VisualObject& snd);
        bool _bPhyEnabled;
        
    protected:
        bool _bParticle;
    };
}

#endif	/* VISUALOBJECT_H */