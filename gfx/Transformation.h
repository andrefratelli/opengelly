#ifndef TRANSFORMATION_H
#define	TRANSFORMATION_H

#include <evts/LogicListener.h>
#include <evts/HandlerRegistration.h>
#include <shared.h>

/*
 * == Transformation ==
 * 
 * Base class for both world and local transformations. It only stores a reference
 * to a matrice of some Transformable object.
 */
namespace eng
{
    class Transformation :
        public LogicListener
    {
    private:
        /*
         * The matrix is a reference. See Transformable.h
         */
        M3DMatrix44f& _mMatrix;
        M3DVector3f _vAccel;
        M3DVector3f _vVelocity;
        HandlerRegistration<LogicListener, LogicEvent> _registration;
        
    protected:
        M3DMatrix44f& getMatrix() const;
        void getMatrix(M3DMatrix44f mMatrix) const;
        
    public:
        Transformation(M3DMatrix44f& mMatrix);
        Transformation(const Transformation& copy);
        ~Transformation();
        
        virtual void translate(GLfloat x, GLfloat y, GLfloat z) = 0;
        virtual void translate(const M3DVector3f vTranslation) = 0;
        
        virtual void setPosition(GLfloat x, GLfloat y, GLfloat z) = 0;
        virtual void setPosition(const M3DVector3f vPosition) = 0;
        
        virtual void rotate(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z) = 0;
        virtual void rotate(GLfloat fAngle, const M3DVector3f vAxis) = 0;
        
        virtual void setRotation(GLfloat fAngle, GLfloat x, GLfloat y, GLfloat z) = 0;
        virtual void setRotation(GLfloat fAngle, const M3DVector3f vAxis) = 0;
        
        virtual void scale(GLfloat fScale) = 0;
        virtual void setScale(GLfloat fScale) = 0;
        
        void accelerate(GLfloat x, GLfloat y, GLfloat z);
        void accelerate(const M3DVector3f vAccel);
        
        void setAcceleration(GLfloat x, GLfloat y, GLfloat z);
        void setAcceleration(const M3DVector3f vAcceleration);
        
        void speed(GLfloat x, GLfloat y, GLfloat z);
        void speed(const M3DVector3f vSpeed);
        
        void setSpeed(GLfloat x, GLfloat y, GLfloat z);
        void setSpeed(const M3DVector3f vVelocity);
        
        M3DVector3f& getAcceleration();
        M3DVector3f& getSpeed();
        
        void getAcceleration(M3DVector3f vAccel) const;
        void getSpeed(M3DVector3f vSpeed) const;
        
        void notify(LogicEvent evt);
    };
}

#endif	/* TRANSFORMATION_H */

