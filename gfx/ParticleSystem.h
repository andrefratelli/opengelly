#ifndef PARTICLESYSTEM_H
#define	PARTICLESYSTEM_H

#include <gfx/VisualObject.h>
#include <model/Buffers.h>
#include <evts/LogicListener.h>
#include <mng/WindowManager.h>
#include <evts/HandlerRegistration.h>
#include <vector>

namespace eng
{
    class ParticleSystem :
        public VisualObject
    {
    public:
        ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth);
        ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, CUSTOM_SHADER shaderID);
        ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, Color color);
        ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, Color color, CUSTOM_SHADER shaderID);
        ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, TextureResource textureResource);
        ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, TextureResource textureResource, CUSTOM_SHADER shaderID);
        
        void draw(const M3DVector4f vLightEyePos);
        
        std::vector<SafePointer<VisualObject> >& getParticles();
        
    private:
        std::vector<SafePointer<VisualObject> > _objects;
        
        void init(VisualResource resource, bool bShaded, bool bTextured, bool bColored);
    };
}

#endif	/* PARTICLESYSTEM_H */
