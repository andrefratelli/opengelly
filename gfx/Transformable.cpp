#include <gfx/Transformable.h>
#include <assert.h>
#include "mng/WindowManager.h"
#include "evts/HandlerRegistration.h"
#include <shared.h>

using namespace eng;

Transformable::Transformable(const Transformable& copy) :
    _world(copy._world),
    _local(copy._local)
{
    // Can't copy. See the header (gfx/Transformable.h)
    assert(false);
}

Transformable::Transformable() :
    _world(_mMatrix),
    _local(_mMatrix)
{
    m3dLoadIdentity44(_mMatrix);
}

WorldTransformation& Transformable::world() { return _world; }
LocalTransformation& Transformable::local() { return _local; }

M3DMatrix44f& Transformable::getMatrix() { return _mMatrix; }
void Transformable::getMatrix(M3DMatrix44f mMatrix) const { m3dCopyMatrix44(mMatrix, _mMatrix); }

void Transformable::getMatrix(M3DMatrix44f mMatrix, bool bScale)
{
    m3dCopyMatrix44(mMatrix, getMatrix());
    
    if (!bScale)
    {
        mMatrix[15] = 1.0f;
    }
}

void Transformable::transform(M3DVector3f vStore, const M3DVector3f vPoint, bool bScale)
{
    m3dTransformVector3(vStore, vPoint, getMatrix());
    
    if (bScale)
    {
        GLfloat scale = getMatrix()[15];
        
        vStore[0] /= scale;
        vStore[1] /= scale;
        vStore[2] /= scale;
    }
}
