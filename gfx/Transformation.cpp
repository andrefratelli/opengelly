#include <gfx/Transformation.h>
#include <mng/WindowManager.h>
#include <shared.h>

using namespace eng;

M3DMatrix44f& Transformation::getMatrix() const
{
    return _mMatrix;
}

void Transformation::getMatrix(M3DMatrix44f mMatrix) const
{
    m3dCopyMatrix44(mMatrix, _mMatrix);
}

Transformation::Transformation(M3DMatrix44f& mMatrix) :
    _mMatrix(mMatrix),
    _registration(WindowManager::instance()->Handler<LogicListener, LogicEvent>::addEventListener(this))
{
    m3dLoadVector3(_vAccel, 0.0f, 0.0f, 0.0f);
    m3dLoadVector3(_vVelocity, 0.0f, 0.0f, 0.0f);
}

Transformation::Transformation(const Transformation& copy) :
    _mMatrix(copy._mMatrix),
    _registration(WindowManager::instance()->Handler<LogicListener, LogicEvent>::addEventListener(this))
{
    m3dLoadVector3(_vAccel, 0.0f, 0.0f, 0.0f);
    m3dLoadVector3(_vVelocity, 0.0f, 0.0f, 0.0f);
}

Transformation::~Transformation()
{
    _registration.remove();
}

void Transformation::accelerate(GLfloat x, GLfloat y, GLfloat z)
{
    _vAccel[0] += x;
    _vAccel[1] += y;
    _vAccel[2] += z;
}

void Transformation::accelerate(const M3DVector3f vAccel)
{
    accelerate(vAccel[0], vAccel[1], vAccel[2]);
}

void Transformation::setAcceleration(GLfloat x, GLfloat y, GLfloat z)
{
    m3dLoadVector3(_vAccel, x, y, z);
}

void Transformation::setAcceleration(const M3DVector3f vAcceleration)
{
    setAcceleration(vAcceleration[0], vAcceleration[1], vAcceleration[2]);
}

void Transformation::speed(GLfloat x, GLfloat y, GLfloat z)
{
    _vVelocity[0] += x;
    _vVelocity[1] += y;
    _vVelocity[2] += z;
}

void Transformation::speed(const M3DVector3f vSpeed)
{
    speed(vSpeed[0], vSpeed[1], vSpeed[2]);
}

void Transformation::setSpeed(GLfloat x, GLfloat y, GLfloat z)
{
    _vVelocity[0] = x;
    _vVelocity[1] = y;
    _vVelocity[2] = z;
}

void Transformation::setSpeed(const M3DVector3f vVelocity)
{
    setSpeed(vVelocity[0], vVelocity[1], vVelocity[2]);
}

M3DVector3f& Transformation::getAcceleration() { return _vAccel; }
M3DVector3f& Transformation::getSpeed() { return _vVelocity; }

void Transformation::getAcceleration(M3DVector3f vAccel) const { m3dCopyVector3(vAccel, _vAccel); }
void Transformation::getSpeed(M3DVector3f vSpeed) const { m3dCopyVector3(vSpeed, _vVelocity); }

void Transformation::notify(LogicEvent evt)
{
    _vVelocity[0] += _vAccel[0];
    _vVelocity[1] += _vAccel[1];
    _vVelocity[2] += _vAccel[2];
    
    translate(_vVelocity);
}