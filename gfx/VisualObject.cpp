#include <gfx/VisualObject.h>
#include <model/BoundBox.h>
#include <model/Buffers.h>
#include <mng/Renderer.h>
#include <mng/WindowManager.h>
#include <mng/ShaderManager.h>
#include <utils/Color.h>
#include <phy/CollisionDetection.h>
#include <gfx/Camera.h>

using namespace eng;

VisualObject::VisualObject(VisualResource resource, size_t octDepth) :
    _visualResource(resource),
    _textureResource(""),
    _color(colors::White),
    _visible(true),
    _polygonMode(GL_FILL),
    _colliding(true),
    _shaderID(CUSTOM_SHADER_FLAT),
    _octDepth(octDepth),
    _bPhy(true),
    _bBouncing(0),
    _bStalling(false),
    _bBounce(1.0f),
    _bParticle(false),
    _collisionCallback(NULL),
    _bPhyEnabled(true)
{}

VisualObject::VisualObject(VisualResource resource, size_t octDepth, CUSTOM_SHADER shaderID) :
    _visualResource(resource),
    _textureResource(""),
    _color(colors::White),
    _visible(true),
    _polygonMode(GL_FILL),
    _colliding(true),
    _shaderID(shaderID),
    _octDepth(octDepth),
    _bPhy(true),
    _bBouncing(0),
    _bStalling(false),
    _bBounce(1.0f),
    _bParticle(false),
    _collisionCallback(NULL),
    _bPhyEnabled(true)
{}

VisualObject::VisualObject(VisualResource resource, size_t octDepth, Color color) :
    _visualResource(resource),
    _textureResource(""),
    _color(color),
    _visible(true),
    _polygonMode(GL_FILL),
    _colliding(true),
    _shaderID(CUSTOM_SHADER_FLAT),
    _octDepth(octDepth),
    _bPhy(true),
    _bBouncing(0),
    _bStalling(false),
    _bBounce(1.0f),
    _bParticle(false),
    _collisionCallback(NULL),
    _bPhyEnabled(true)
{}

VisualObject::VisualObject(VisualResource resource, size_t octDepth, Color color, CUSTOM_SHADER shaderID) :
    _visualResource(resource),
    _textureResource(""),
    _color(color),
    _visible(true),
    _polygonMode(GL_FILL),
    _colliding(true),
    _shaderID(shaderID),
    _octDepth(octDepth),
    _bPhy(true),
    _bBouncing(0),
    _bStalling(false),
    _bBounce(1.0f),
    _bParticle(false),
    _collisionCallback(NULL),
    _bPhyEnabled(true)
{}

VisualObject::VisualObject(VisualResource visualResource, size_t octDepth, TextureResource textureResource) :
    _visualResource(visualResource),
    _textureResource(textureResource),
    _color(colors::White),
    _visible(true),
    _polygonMode(GL_FILL),
    _colliding(true),
    _shaderID(CUSTOM_SHADER_FLAT_TEXTURED),
    _octDepth(octDepth),
    _bPhy(true),
    _bBouncing(0),
    _bStalling(false),
    _bBounce(1.0f),
    _bParticle(false),
    _collisionCallback(NULL),
    _bPhyEnabled(true)
{}

VisualObject::VisualObject(VisualResource visualResource, size_t octDepth, TextureResource textureResource, CUSTOM_SHADER shaderID) :
    _visualResource(visualResource),
    _textureResource(textureResource),
    _color(colors::White),
    _visible(true),
    _polygonMode(GL_FILL),
    _colliding(true),
    _shaderID(shaderID),
    _octDepth(octDepth),
    _bPhy(true),
    _bBouncing(0),
    _bStalling(false),
    _bBounce(1.0f),
    _bParticle(false),
    _collisionCallback(NULL),
    _bPhyEnabled(true)
{}


VisualResource VisualObject::resource() const
{
    return _visualResource;
}

TextureResource VisualObject::textResource() const
{
    return _textureResource;
}

void VisualObject::draw(const M3DVector4f vLightEyePos)
{
    glPolygonMode(GL_FRONT_AND_BACK, getPolygonMode());
    _draw(vLightEyePos);
}

bool VisualObject::getVisible() const { return _visible; }
void VisualObject::setVisible(bool visible) { _visible = visible; }

Color VisualObject::getColor() const { return _color; }
void VisualObject::setColor(const Color color) { _color = color; }

CUSTOM_SHADER VisualObject::getShader() { return _shaderID; }
void VisualObject::setShader(CUSTOM_SHADER shaderID) { _shaderID = shaderID; }

GLenum VisualObject::getPolygonMode()
{
    return _polygonMode;
}

void VisualObject::setPolygonMode(GLenum mode)
{
    _polygonMode = mode;
}

bool VisualObject::getCollisionEnabled()
{
    return _colliding;
}

void VisualObject::setCollisionEnabled(bool set)
{
    _colliding = set;
}


bool VisualObject::collides(VisualObject& other, PhyCol shallowness, bool bJustCheck)
{
    return CollisionDetection::collide(*this, other, shallowness, bJustCheck);
}

void VisualObject::_draw(const M3DVector4f vLightEyePos)
{
    M3DMatrix44f mModelView, mModelViewProjection;
    m3dMatrixMultiply44(mModelView, Camera::getActive()->getMatrix(), getMatrix());
    m3dMatrixMultiply44(mModelViewProjection, Camera::getActive()->getFrustum().getMatrix(), mModelView);
    
    if (!_textureResource.getPath().empty())
    {
        glBindTexture(GL_TEXTURE_2D, _textureResource.getTextureID());
        Renderer::instance()->shaderManager()->UseCustomShader(getShader(), mModelView, mModelViewProjection, vLightEyePos, 0);
        _visualResource.getBatch()->Draw();
    }
    else {
        Renderer::instance()->shaderManager()->UseCustomShader(getShader(), mModelView, mModelViewProjection, vLightEyePos, getColor().getRGBA());
        _visualResource.getBatch()->Draw();
    }
}

size_t VisualObject::getOctDepth() const
{
    return _octDepth;
}

void VisualObject::setPhy(bool bPhy) { _bPhy = bPhy; }
bool VisualObject::getPhy() const { return _bPhy; }

#define CPYM_NOT(d,m) m3dCopyMatrix44(d,m); d[12] = d[13] = d[14] = 0
#define T(s,v,m)                        \
    m3dTransformVector3(s,v,m);         \
    s[0] /= m[15];                      \
    s[1] /= m[15];                      \
    s[2] /= m[15];

void VisualObject::getSpeed(M3DVector3f vSpeed)
{
    M3DMatrix44f mMatrix;
    M3DVector3f vLocalSpeed;
 
    // Local speed in world coordinates
    CPYM_NOT(mMatrix, getMatrix());
    T(vLocalSpeed, local().getSpeed(), mMatrix);
    
    // Total speed
    m3dAddVectors3(vSpeed, vLocalSpeed, world().getSpeed());
}

void VisualObject::setBouncing(size_t bBouncing) { _bBouncing = bBouncing; }
size_t VisualObject::getBouncing() const { return _bBouncing; }

void VisualObject::setStalling(bool bStalling) { _bStalling = bStalling; }
bool VisualObject::getStalling() const { return _bStalling; }

void VisualObject::setBounce(size_t bBounce) { _bBounce = bBounce; }
size_t VisualObject::getBounce() const { return _bBounce; }

bool VisualObject::isParticle() { return _bParticle; }

void VisualObject::setCollisionCallback(bool (*callback)(VisualObject& fst, VisualObject& snd))
{
    _collisionCallback = callback;
}

bool VisualObject::collide(VisualObject& snd)
{
    if (_collisionCallback != NULL) return _collisionCallback(*this, snd);
    return true;
}

bool VisualObject::getPhyEnabled() const { return _bPhyEnabled; }
void VisualObject::setPhyEnabled(bool enabled) { _bPhyEnabled = enabled; }

#undef CPYM_NOT
#undef T
