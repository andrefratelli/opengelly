#ifndef TRANSFORMABLE_H
#define	TRANSFORMABLE_H

#include <utils/SafePointer.h>
#include <gfx/LocalTransformation.h>
#include <gfx/WorldTransformation.h>
#include <vector>
#include <model/Buffers.h>
#include <shared.h>

/*
 * == Transformable ==
 * 
 * This is the base class for all transformable objects. It defines both world
 * and local transformations, as well as the matrix that is shared by all of them.
 * See the comment below.
 */
namespace eng
{
    class Transformable
    {   
    public:
        Transformable();
        
        WorldTransformation& world();
        LocalTransformation& local();
        
        M3DMatrix44f& getMatrix();
        void getMatrix(M3DMatrix44f mMatrix) const;
        void getMatrix(M3DMatrix44f mMatrix, bool bScale);
        
        void transform(M3DVector3f vStore, const M3DVector3f vPoint, bool scale);
        
    private:
        M3DMatrix44f _mMatrix;
        WorldTransformation _world;
        LocalTransformation _local;
        
        /*
         * If we allow copies of this class, then the user would be led to think
         * that a copy would correspond to the same transformation matrix, which
         * it doesn't. For instance:
         * 
         * void foo(Transformable t)
         * {
         *      t.translate(1.0f, 0.0f, 0.0f);
         * }
         * 
         * Transformable original;
         * foo(original);
         * 
         * In this case, the transformation would not reflect in the original
         * Transformable, although the user would probably be led to think so.
         * 
         * Of course that the solution is using references or pointers, but a
         * simple distraction would be very hard to debug. ResourceManager
         * will return copies of the object data, but each with different
         * matrices, which is precisely the effect I'm looking for.
         * 
         * WorldTransformation and LocalTransformation (Transformation, in general)
         * will get a reference to this matrix, allowing us to perform both types
         * of transformations and keep a single static matrix.
         */
        Transformable(const Transformable& copy);
    };
}

#endif	/* TRANSFORMABLE_H */

