#include <gfx/Camera.h>
#include <shared.h>

using namespace eng;

Camera* Camera::_active = NULL;

/*
 * Cameras do not have associated any vertex, normal, or texture data, but
 * they still are transformables.
 * 
 * This is an error in the design because the buffers where added to the
 * transformable after the cameras had been written. We'll just construct
 * empty buffers for it
 */
Camera::Camera()
{
    // Always default to the newest camera
    _active = this;
}

M3DMatrix44f& Camera::getMatrix()
{
    computeCameraMatrix(_mMatrix);
    return _mMatrix;
}

void Camera::getMatrix(M3DMatrix44f mMatrix) const
{
    computeCameraMatrix(mMatrix);
}

Camera* Camera::getActive()
{
    if (_active == NULL) { _active = new Camera(); }
    return _active;
}

Frustum& Camera::getFrustum()
{
    return _frustum;
}

void Camera::computeCameraMatrix(M3DMatrix44f mResult) const
{
    M3DMatrix44f mOriginal;
    
    Transformable::getMatrix(mOriginal);
    
    M3DMatrix44f mTemp;
     M3DVector3f vRight;
    const M3DVector3f vUp = {mOriginal[4], mOriginal[5], mOriginal[6]};
    const M3DVector3f vForward = {mOriginal[8], mOriginal[9], mOriginal[10]};
    
    m3dCrossProduct3(vRight, vUp, vForward);
    
    // The matrix is transposed and the Z (vForward) axis is reversed (no translation)
    mTemp[0] = vRight[0];  mTemp[1] = vUp[0]; mTemp[2] = -vForward[0];  mTemp[3] = 0.0f;
    mTemp[4] = vRight[1];  mTemp[5] = vUp[1]; mTemp[6] = -vForward[1];  mTemp[7] = 0.0f;
    mTemp[8] = vRight[2];  mTemp[9] = vUp[2]; mTemp[10] = -vForward[2]; mTemp[11] = 0.0f;
    mTemp[12] = 0.0f;      mTemp[13] = 0.0f;  mTemp[14] = 0.0f;         mTemp[15] = 1.0f;
    
    // Translation.
    M3DMatrix44f mTranslation;
    
    m3dTranslationMatrix44(mTranslation, -mOriginal[12], -mOriginal[13], -mOriginal[14]);
    m3dMatrixMultiply44(mResult, mTemp, mTranslation);
}
