#include <gfx/ParticleSystem.h>
#include <model/Object.h>
#include <gfx/Camera.h>
#include <mng/Renderer.h>

using namespace eng;

ParticleSystem::ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth) :
    VisualObject(particles, octDepth)
{
    init(resource, false, false, false);
}

ParticleSystem::ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, CUSTOM_SHADER shaderID) :
    VisualObject(particles, octDepth, shaderID)
{
    init(resource, true, false, false);
}

ParticleSystem::ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, Color color) :
    VisualObject(particles, octDepth, color)
{
    init(resource, false, false, true);
}

ParticleSystem::ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, Color color, CUSTOM_SHADER shaderID) :
    VisualObject(particles, octDepth, color, shaderID)
{
    init(resource, true, false, true);
}

ParticleSystem::ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, TextureResource textureResource) :
    VisualObject(particles, octDepth, textureResource)
{
    init(resource, false, true, false);
}

ParticleSystem::ParticleSystem(VisualResource particles, VisualResource resource, size_t octDepth, TextureResource textureResource, CUSTOM_SHADER shaderID) :
    VisualObject(particles, octDepth, textureResource, shaderID)
{
    init(resource, true, true, false);
}

void ParticleSystem::draw(const M3DVector4f vLightEyePos)
{
//    glPolygonMode(GL_FRONT_AND_BACK, getPolygonMode());
//    
//    M3DMatrix44f mModelView, mModelViewProjection;
//    m3dMatrixMultiply44(mModelView, Camera::getActive()->getMatrix(), getMatrix());
//    m3dMatrixMultiply44(mModelViewProjection, Camera::getActive()->getFrustum().getMatrix(), mModelView);
//    
//    std::vector<SafePointer<VisualObject> >::iterator it = _objects.begin();
//    std::vector<SafePointer<VisualObject> >::iterator iend = _objects.end();
//    
//    while (it != iend)
//    {
//        (*it)->draw(vLightEyePos);
//        it++;
//    }
}

std::vector<SafePointer<VisualObject> >& ParticleSystem::getParticles()
{
    return _objects;
}
/*
void ParticleSystem::notify(LogicEvent evt)
{
    if (_go)
    {
        Buffers::buffer_vt::size_type it = 0;
        Buffers::buffer_vt& verts = resource().getObject()->buffers().vertexes();

        for ( ; it < _velocity.size() ; it++)
        {
            GLfloat resistance = 0.005f;
            
            if (it % 3 == 1) {
                _velocity[it] -= 0.02f;  // Gravity
                resistance = 0.01f;
            }

            // Air resistance
            if (_velocity[it] < 0.0f) {
                _velocity[it] += resistance;
                if (_velocity[it] > 0.0f) { _velocity[it] = 0.0f; }
            } else {
                _velocity[it] -= resistance;
                if (_velocity[it] < 0.0f) { _velocity[it] = 0.0f; }
            }
            
            // Move
            verts[it] += _velocity[it];
            
            // Collision with horizontal plane
            if (it % 3 == 1 && verts[it] < -20.0f) {
                verts[it] = -20.0f;
                _velocity[it] *= -1;
            }
        }
    }
}

void ParticleSystem::notify(KeyboardEvent evt)
{
    if (evt.down('r') || evt.down('R')) {
        Buffers::buffer_vt::size_type it = 0;
        Buffers::buffer_vt& pos = resource().getObject()->buffers().vertexes();
        Buffers::buffer_vt& norms = resource().getObject()->buffers().normals();

        for ( ; it < _velocity.size() ; it++ )
        {
            pos[it] = _original[it];
            
            _velocity[it] = norms[it];
            
            if (it % 3 == 0) 
            {
                _cynetic[it] = 1.0f;
            }
        }

        _go = true;
    }
}**/

void ParticleSystem::init(VisualResource resource, bool bShaded, bool bTextured, bool bColored)
{
    setPhy(false);
    
    Buffers::buffer_vt* vertexes = VisualObject::resource().getObject()->getBaseVertices();
    Buffers::buffer_vt::size_type it;
    Buffers::buffer_vt::size_type iend = vertexes->size();
    
    for (it=0 ; it<iend ; it += 3)
    {
        VisualObject* vobj;
        
        if (bShaded && bTextured)
        {
            vobj = new VisualObject(resource, getOctDepth(), textResource(), getShader());
        }
        else if (bShaded)
        {
            if (bColored) { vobj = new VisualObject(resource, getOctDepth(), getColor(), getShader()); }
            else { vobj = new VisualObject(resource, getOctDepth(), getColor(), getShader()); }
        }
        else if (bTextured)
        {
            vobj = new VisualObject(resource, getOctDepth(), textResource());
        }
        else
        {
            if (bColored) { vobj = new VisualObject(resource, getOctDepth()); }
            else { vobj = new VisualObject(resource, getOctDepth(), getColor()); }
        }
        
        vobj->_bParticle = true;
        
        vobj->world().translate((*vertexes)[it], (*vertexes)[it + 1], (*vertexes)[it + 2]);
        
        SafePointer<VisualObject> ptr = SafePointer<VisualObject>(vobj);
        _objects.push_back(ptr);
    }
}
