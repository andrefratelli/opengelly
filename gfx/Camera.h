#ifndef CAMERA_H
#define	CAMERA_H

#include <gfx/Transformable.h>
#include <gfx/Frustum.h>
#include <shared.h>

/*
 * == Camera ==
 * 
 * Does the necessary stuff for cameras. In addition, it's a Transformable
 * instance, which means you can do with cameras whatever you do with other
 * objects.
 * 
 * Besides the Transformable matrix, cameras add their own. This happens because
 * camera matrices are a little different, but the original is the one used for
 * transformations.
 * 
 * Unfortunately, we currently don't have a means to know if the camera has
 * been transformed (like a listener), so we have to compute the camera matrix
 * every time it's need.
 */
namespace eng
{
    class Camera :
        public Transformable
    {
    public:
        Camera();
        
        M3DMatrix44f& getMatrix();
        void getMatrix(M3DMatrix44f mMatrix) const;
        
        static Camera* getActive();
        
        Frustum& getFrustum();
        
    private:
        M3DMatrix44f _mMatrix;
        Frustum _frustum;
        static Camera* _active;
        
        void computeCameraMatrix(M3DMatrix44f mMatrix) const;
    };
}

#endif	/* CAMERA_H */

