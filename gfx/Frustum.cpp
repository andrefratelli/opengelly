#include <gfx/Frustum.h>
#include <shared.h>

using namespace eng;

Frustum::Frustum()
{
    setOrthographic(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
}

Frustum::Frustum(GLfloat fFov, GLfloat fAspect, GLfloat fNear, GLfloat fFar)
{
    setPerspective(fFov, fAspect, fNear, fFar);
}

Frustum::Frustum(GLfloat xMin, GLfloat xMax, GLfloat yMin, GLfloat yMax, GLfloat zMin, GLfloat zMax)
{
    setOrthographic(xMin, xMax, yMin, yMax, zMin, zMax);
}

Frustum::Frustum(M3DVector2f xLim, M3DVector2f yLim, M3DVector2f zLim)
{
    setOrthographic(xLim[0], xLim[1], yLim[0], yLim[1], zLim[0], zLim[1]);
}

void Frustum::setOrthographic(GLfloat xMin, GLfloat xMax, GLfloat yMin, GLfloat yMax, GLfloat zMin, GLfloat zMax)
{
    // Make projection matrix
    m3dMakeOrthographicMatrix(mProjection, xMin, xMax, yMin, yMax, zMin, zMax);
}

void Frustum::setOrthographic(M3DVector2f xLim, M3DVector2f yLim, M3DVector2f zLim)
{
    setOrthographic(xLim[0], xLim[1], yLim[0], yLim[1], zLim[0], zLim[1]);
}

void Frustum::setPerspective(GLfloat fFov, GLfloat fAspect, GLfloat fNear, GLfloat fFar)
{
    GLfloat xmin, xmax, ymin, ymax;     // Near clipping plane
    
    // Near clipping plane
    ymax = fNear * GLfloat(tan(fFov * M3D_PI / 360.0));
    ymin = -ymax;
    xmin = ymin * fAspect;
    xmax = -xmin;
    
    // Projection matrix
    m3dLoadIdentity44(mProjection);
    mProjection[0] = (2.0f * fNear) / (xmax - xmin);
    mProjection[5] = (2.0f * fNear) / (ymax - ymin);
    mProjection[8] = (xmax + xmin) / (xmax - xmin);
    mProjection[9] = (ymax + ymin) / (ymax - ymin);
    mProjection[10] = -((fFar + fNear) / (fFar - fNear));
    mProjection[11] = -1.0f;
    mProjection[14] = -((2.0f * fFar * fNear) / (fFar - fNear));
    mProjection[15] = 0.0f;
}

const M3DMatrix44f& Frustum::getMatrix()
{
    return mProjection;
}

void Frustum::getMatrix(M3DMatrix44f mMatrix)
{
    m3dCopyMatrix44(mMatrix, mProjection);
}
